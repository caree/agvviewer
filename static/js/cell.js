    //------------------------------------------------
    //cell definition
    function Cell(x, y, color, txt) {
        this.X = x
        this.Y = y
        this.Color = color
        if (txt == null) {
            this.TextToDraw = ""
        } else {
            this.TextToDraw = txt
        }
        this.graphicsShape = null
        this.graphicsText = null
    }
    Cell.prototype.updateColorText = function(color, text) {
        if (color != null) {
            this.Color = color
        }
        // this.TextToDraw= text
        if (text == null) {
            this.TextToDraw = ""
        } else {
            this.TextToDraw = text
        }
    };
    Cell.prototype.redrawCell = function() {
        if (this.graphicsShape != null) {
            this.graphicsShape.graphics.clear()
        }
        this.graphicsShape = fillCellOnGrid(this.Color, this.X, this.Y, this.graphicsShape)

        var x = originPointX + (this.X - 1) * cellWidth
        var y = originPointY + (this.Y - 1) * cellHeight
            // console.info("redrawCell => %d, %d  %d", x, y, cellWidth)

        if (this.graphicsText == null) {
            this.graphicsText = drawText(this.TextToDraw, x, y, "#000", 0, cellWidth, cellHeight, 16)
        } else {
            this.graphicsText.text = this.TextToDraw
            var spaceWidth = cellWidth
            var txtWidth = this.graphicsText.getMeasuredWidth()
            if (spaceWidth > txtWidth) {
                var padding = (spaceWidth - txtWidth) / 2
                this.graphicsText.x = x + padding
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    function ConfigPoint(x, y, blockType) {
        this.X = x
        this.Y = y
        this.BlockType = blockType
        this.Orientations = []
        // this.updateBlockType(blockType)
        this.Placer = ""
        // this.orientation = ""
    }
    ConfigPoint.prototype.setID = function(id) {
        if (id != null) {
            this.Placer = id
        }
    }
    ConfigPoint.prototype.getBlockType = function() {
        return this.BlockType
    }

    //如果四个方向全部包括，则清空方向，表示全部方向
    ConfigPoint.prototype.updateOrientation = function(cmd) {
        if(Cmd_ClearOrientation == cmd){
            this.Orientations = []
            return
        }
        if(_.contains(this.Orientations, cmd) == true){
            return
        }
        this.Orientations.push(cmd)
        if(_.size(this.Orientations) >= 4){
            this.Orientations = []
        }        
        return
        // switch (cmd) {
        //     case Cmd_ClearOrientation:
        //         this.Orientations = []
        //         // return true
        //         break
        //     case Cmd_AddOrientation_Left:
        //         if (_.contains([Cmd_AddOrientation_Left, Cmd_AddOrientation_Right]) == true) {
        //             break
        //         }
        //         this.Orientations.push(cmd)
        //         break
        //     case Cmd_AddOrientation_Right:
        //         if (_.contains([Cmd_AddOrientation_Left, Cmd_AddOrientation_Right]) == true) {
        //             break
        //         }
        //         this.Orientations.push(cmd)
        //         break
        //     case Cmd_AddOrientation_Up:
        //         if (_.contains([Cmd_AddOrientation_Up, Cmd_AddOrientation_Down]) == true) {
        //             break
        //         }
        //         this.Orientations.push(cmd)
        //         break
        //     case Cmd_AddOrientation_Down:
        //         if (_.contains([Cmd_AddOrientation_Up, Cmd_AddOrientation_Down]) == true) {
        //             break
        //         }
        //         this.Orientations.push(cmd)
        //         break
        // }
    }
    ConfigPoint.prototype.updateBlockType = function(blockType) {
        this.BlockType = blockType

        // if(_.contains(OrientationCmdSeries, blockType) == true){
        //     switch(blockType){
        //         case BlockType_Railway_Right:
        //         if(this.BlockType == BlockType_Railway_Left) return false
        //         this.Orientations.push(blockType)
        //         return true
        //         break
        //         case BlockType_Railway_Down:
        //         if(this.BlockType == BlockType_Railway_Up) return false
        //         this.Orientations.push(blockType)
        //         return true
        //         break
        //         case BlockType_Railway_Left:
        //         if(this.BlockType == BlockType_Railway_Right) return false
        //         this.Orientations.push(blockType)
        //         return true
        //         break
        //         case BlockType_Railway_Up:
        //         if(this.BlockType == BlockType_Railway_Down) return false
        //         this.Orientations.push(blockType)
        //         return true
        //         break
        //     }
        // }else{
        //     this.BlockType = blockType
        //     return true
        // }
    };
    ConfigPoint.prototype.getBlockText = function() {
        var str = ""
        var sorted = _.sortBy(this.Orientations, function(orientation) {
            return orientation
        })
        str = _.reduce(sorted, function(memo, orientation) {
            var oriTxt = "↑"
            switch (orientation) {
                case Cmd_AddOrientation_Right:
                    oriTxt = "→"
                    break
                case Cmd_AddOrientation_Down:
                    oriTxt = "↓"
                    break
                case Cmd_AddOrientation_Left:
                    oriTxt = "←"
                    break
            }
            return memo + oriTxt
        }, "")
        return str
    }
    // function Shelf(id, x, y){
    //     this.X = x
    //     this.Y = y
    //     this.ID = id
    // }
    // Shelf.prototype.setID = function(id){
    //     if(id != null){
    //         this.ID = id
    //     }
    // }