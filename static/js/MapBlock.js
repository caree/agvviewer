//------------------------------------------------
function MapBlock(x, y, color, txt) {
    this.X = x
    this.Y = y
    this.RailwayArea = new RailwayArea(x, y, txt)
    this.AGV = null
    this.Shelf = null
    this.BannedArea = null
    this.PickUpArea = null
    this.RechargingArea = null
}
MapBlock.prototype.compare = function(mapBlock) {
    if (mapBlock == null)
        return false
    return mapBlock.X == this.X && mapBlock.Y == this.Y
}
MapBlock.prototype.updateAGV = function(agv) {
    this.AGV.X = agv.X
    this.AGV.Y = agv.Y
    this.AGV.Status = agv.Status
    this.AGV.LiftStatus = agv.LiftStatus
}
MapBlock.prototype.clearAGV = function() {
    this.AGV = null
}

MapBlock.prototype.clearShelf = function() {
    this.Shelf = null
}

MapBlock.prototype.setAGV = function(agv) {
    this.AGV = agv
};


MapBlock.prototype.setShelf = function(shelf) {
    this.Shelf = shelf
};


MapBlock.prototype.setPickUpArea = function(pickUpArea) {
    // this.BlockType = 3
    this.Shelf = null
    this.AGV = null
    this.BannedArea = null
    this.PickUpArea = pickUpArea
    this.RechargingArea = null
};
MapBlock.prototype.setBannedArea = function(bannedArea) {
    // this.BlockType = 3
    this.Shelf = null
    this.AGV = null
    this.BannedArea = bannedArea
    this.PickUpArea = null
    this.RechargingArea = null
};
MapBlock.prototype.setRechargingArea = function(rechargingArea) {
    this.Shelf = null
    this.AGV = null
    this.BannedArea = null
    this.PickUpArea = null
    this.RechargingArea = rechargingArea
};

MapBlock.prototype.setBlockType = function(blockType) {
    this.BlockType = blockType
};
MapBlock.prototype.setRailwayArea = function(railwayArea) {
    this.RailwayArea = railwayArea
}
MapBlock.prototype.getDrawingContent = function() {
    if (this.Shelf != null && this.AGV != null) {
        console.log("绘制AGV与货架重合状态 (%d, %d)", this.X, this.Y)
        return {
            Color: agvUnderShelfColor,
            Text: ""
        }
    }
    if (this.Shelf != null) {
        console.log("绘制货架 (%d, %d)", this.X, this.Y)
        return {
            Color: shlefPlaceColor,
            Text: ""//不显示ID
            // Text: this.Shelf.ID
        }
    }
    if (this.AGV != null) {
        console.log("绘制AGV (%d, %d)", this.X, this.Y)
        return {
            Color: agvRunningColor,
            Text: this.AGV.ID
        }
    }

    if (this.PickUpArea != null) {
        console.log("绘制 PickUpArea (%d, %d) ", this.X, this.Y)
        return {
            Color: PickupPlaceColor,
            Text: this.PickUpArea.Text
        }
    }
    if (this.RechargingArea != null) {
        console.log("绘制 RechargingArea (%d, %d) ", this.X, this.Y)
        return {
            Color: RechargingAreaColor,
            Text: this.RechargingArea.Text
        }
    }

    if (this.BannedArea != null) {
        console.log("绘制 BannedArea (%d, %d) ", this.X, this.Y)
        return {
            Color: bannedAreaColor,
            Text: ""
        }
    }
    console.log("绘制 RailwayArea (%d, %d) %s", this.X, this.Y, this.RailwayArea.Text)
    return {
        Color: railwayColor,
        Text: this.RailwayArea.Text//其实是空
    }
}
//---------------------------------------------------------------------------
function MapBlocks() {
    this.Blocks = []
}
MapBlocks.prototype.addMapBlock = function(x, y) {
    this.Blocks.push(new MapBlock(x, y))
};
//返回两个block
MapBlocks.prototype.updateBlockShelf = function(shelf) {
    if (shelf != null) {
        //如果之前已经连接到某点，需要更新其属性，与新点建立连接，断开与之前点的连接，重新绘制新点，否则直接连接到某点
        var blockTemp = _.find(this.Blocks, function(block) {
            return block.Shelf != null && block.Shelf.X == shelf.OriginalX && block.Shelf.Y == shelf.OriginalY
        })
        console.assert(blockTemp != null, "没有找到该货架")
        if (blockTemp != null) {
            console.info("更新了Shelf %s", JSON.stringify(shelf))

            var shelfExisted = blockTemp.Shelf
            console.assert(shelfExisted != null, "系统异常")
            blockTemp.clearShelf() //将之前AGV所在的点的货架搬运了过来
            updateCellContent(blockTemp.X, blockTemp.Y, blockTemp.getDrawingContent())


            shelfExisted.X = shelf.X
            shelfExisted.Y = shelf.Y

            var blockNewPoint = _.findWhere(this.Blocks, {
                X: shelf.X,
                Y: shelf.Y
            })
            if (blockNewPoint != null) {
                blockNewPoint.setShelf(shelfExisted)
                updateCellContent(blockNewPoint.X, blockNewPoint.Y, blockNewPoint.getDrawingContent())
            }
        }
    }
}
MapBlocks.prototype.updateBlockAGV = function(agv) {
    //如果之前已经连接到某点，需要更新其属性，与新点建立连接，断开与之前点的连接，重新绘制新点，否则直接连接到某点
    var blockTemp = _.find(this.Blocks, function(block) {
        return block.AGV != null && block.AGV.ID == agv.ID
    })
    if (blockTemp != null) {
        console.info("更新了AGV %s", JSON.stringify(agv))
        var agvExisted = blockTemp.AGV

        blockTemp.clearAGV() //去除之前的连接
        updateCellContent(blockTemp.X, blockTemp.Y, blockTemp.getDrawingContent())

        agvExisted.X = agv.X
        agvExisted.Y = agv.Y
        agvExisted.Status = agv.Status
        agvExisted.LiftStatus = agv.LiftStatus

        var blockNewPoint = _.findWhere(this.Blocks, {
            X: agv.X,
            Y: agv.Y
        })
        if (blockNewPoint != null) {
            blockNewPoint.setAGV(agvExisted)
            updateCellContent(blockNewPoint.X, blockNewPoint.Y, blockNewPoint.getDrawingContent())
        }
    } else {
        console.info("第一次接收AGV信息 %s", JSON.stringify(agv))
        //返回agv指定的位置
        blockTemp = _.findWhere(this.Blocks, {
            X: agv.X,
            Y: agv.Y
        })
        if (blockTemp != null) {
            blockTemp.setAGV(agv)
            updateCellContent(blockTemp.X, blockTemp.Y, blockTemp.getDrawingContent())
        }
    }
};
//只在初始化时调用
MapBlocks.prototype.setAGV = function(agv) {
    var blockTemp = _.findWhere(this.Blocks, {
        X: agv.X,
        Y: agv.Y
    })
    if (blockTemp != null) {
        blockTemp.setAGV(agv)
        console.info("增加了AGV %s", JSON.stringify(agv))
        return true
    } else {
        return false
    }
};
//只在初始化时调用
MapBlocks.prototype.setShelf = function(shelf) {
    var blockTemp = _.findWhere(this.Blocks, {
        X: shelf.X,
        Y: shelf.Y
    })
    if (blockTemp != null) {
        blockTemp.setShelf(shelf)
        console.info("增加了Shelf %s", JSON.stringify(shelf))
        return true
    } else {
        return false
    }
};
//只在初始化时调用
MapBlocks.prototype.setBannedArea = function(bannedArea) {
    var blockTemp = _.findWhere(this.Blocks, {
        X: bannedArea.X,
        Y: bannedArea.Y
    })
    if (blockTemp != null) {
        blockTemp.setBannedArea(bannedArea)
        return true
    } else {
        return false
    }
};
MapBlocks.prototype.setPickUpArea = function(pickUpArea) {
    var blockTemp = _.findWhere(this.Blocks, {
        X: pickUpArea.X,
        Y: pickUpArea.Y
    })
    if (blockTemp != null) {
        blockTemp.setPickUpArea(pickUpArea)
        return true
    } else {
        return false
    }
};

MapBlocks.prototype.setRechargingArea = function(rechargingArea) {
    var blockTemp = _.findWhere(this.Blocks, {
        X: rechargingArea.X,
        Y: rechargingArea.Y
    })
    if (blockTemp != null) {
        blockTemp.setRechargingArea(rechargingArea)
        return true
    } else {
        return false
    }
};
MapBlocks.prototype.setRailwayArea = function(railwayArea) {
    var blockTemp = _.findWhere(this.Blocks, {
        X: railwayArea.X,
        Y: railwayArea.Y
    })
    if (blockTemp != null) {
        blockTemp.setRailwayArea(railwayArea)
        return true
    } else {
        return false
    }
};
//------------------------------------------------


function AGV(id, status, liftStatus, x, y) {
    this.ID = id;
    this.Status = status
    this.LiftStatus = liftStatus
    this.X = x
    this.Y = y
}
// AGV.prototype.clearDrawing = function() {
//     if (this.graphicShape != null) {
//         this.graphicShape.graphics.clear()
//     }
//     if (this.graphicText != null) {
//         this.graphicText = drawAGV_IDOnGrid(this.ID, -this.X, -this.Y, 0, this.graphicText)
//     }
// }
// AGV.prototype.drawAGV = function() {
//     if (this.graphicShape != null) {
//         this.graphicShape.graphics.clear()
//     }
//     switch (this.Status) {
//         case "1":
//             if (this.LiftStatus == AGV_LIFT_STATUS_UNLIFTED) {
//                 this.graphicShape = fillCellOnGrid(agvRunningColor, this.X, this.Y, this.graphicShape)
//             } else if (this.LiftStatus == AGV_LIFT_STATUS_LIFTED) {
//                 this.graphicShape = fillCellOnGrid(agvRunningLiftedColor, this.X, this.Y, this.graphicShape)
//             }
//             this.graphicText = drawAGV_IDOnGrid(this.ID, this.X, this.Y, 0, this.graphicText)
//             break
//         case "2":
//             this.graphicShape = fillCellOnGrid(agvPausedColor, this.X, this.Y, this.graphicShape)
//             this.graphicText = drawAGV_IDOnGrid(this.ID, this.X, this.Y, 0, this.graphicText)
//             break
//         case "3":
//             this.graphicShape = fillCellOnGrid(agvStoppedColor, this.X, this.Y, this.graphicShape)
//             this.graphicText = drawAGV_IDOnGrid(this.ID, this.X, this.Y, 0, this.graphicText)
//             break
//     }
// };

function Shelf(x, y, OriginalX, OriginalY) {
    // this.ID = id
    this.X = x
    this.OriginalX = OriginalX
    this.Y = y
    this.OriginalY = OriginalY
    this.graphicShape = null
}
// Shelf.prototype.drawShelf = function(specialColor) {
//     if (this.graphicShape != null) {
//         this.graphicShape.graphics.clear()
//     }
//     if (specialColor == null) {
//         this.graphicShape = fillCellOnGrid(shlefPlaceColor, this.X, this.Y, this.graphicShape)
//     } else {
//         this.graphicShape = fillCellOnGrid(specialColor, this.X, this.Y, this.graphicShape)
//     }
// };

function BannedArea(x, y, txt) {
    this.X = x
    this.Y = y
    this.Text = txt
}
// BannedArea.prototype.drawBannedArea = function() {
//     if (this.graphicShape != null) {
//         this.graphicShape.graphics.clear()
//     }
//     this.graphicShape = fillCellOnGrid(bannedAreaColor, this.X, this.Y, this.graphicShape)
// };

function PickUpArea(x, y, txt) {
    this.X = x
    this.Y = y
    this.Text = txt
}
// PickUpArea.prototype.drawArea = function() {
//     if (this.graphicShape != null) {
//         this.graphicShape.graphics.clear()
//     }
//     this.graphicShape = fillCellOnGrid(PickupPlaceColor, this.X, this.Y, this.graphicShape)
// };

function RechargingArea(x, y, txt) {
    this.X = x
    this.Y = y
    this.Text = txt
}
// RechargingArea.prototype.drawArea = function() {
//     if (this.graphicShape != null) {
//         this.graphicShape.graphics.clear()
//     }
//     this.graphicShape = fillCellOnGrid(RechargingAreaColor, this.X, this.Y, this.graphicShape)
// };

function RailwayArea(x, y, txt) {
    this.X = x
    this.Y = y
    this.Text = txt
}