package controllers

import (
	// "encoding/json"
	scheduler "agvViewer/agvScheduler"
	"fmt"
	"github.com/astaxie/beego"
	// "strconv"
	"github.com/bitly/go-simplejson"
	"strconv"
)

func init() {
	startEventListening := func() {
		scheduler.DebugOutput("开始面向用户端的AGV运行消息监听。。。"+scheduler.GetFileLocation(), 2)
		for {
			event := <-scheduler.G_chanBroadcastEvent
			broadcastWebSocket(event)
		}
	}
	go startEventListening()
}

var (
	aboutInfo string = "LIVY 无人搬运车监控系统(V1.10)"
	guid             = 1
)

func getGUID() int {
	guid = guid + 1
	return guid
}

type ResponseMsg struct {
	Code    int
	Message string
}
type MainController struct {
	beego.Controller
}

func resetConfigPoint(points scheduler.WarehouseConfigPointList) {
	scheduler.G_WarehouseConfigPointList = append(scheduler.WarehouseConfigPointList{}, points...)
}

func (this *MainController) Index() {
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "index.tpl"

}

func (this *MainController) ShowWarehouseIndex() {
	this.Data["guid"] = getGUID()
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "ShowWarehouseIndex.tpl"
}

//AGV控制测试页面
func (this *MainController) ControlIndex() {
	this.Data["AGV_STATUS_RUNNING"] = scheduler.AGV_STATUS_RUNNING
	this.Data["AGV_STATUS_RUNNING_PAUSED"] = scheduler.AGV_STATUS_RUNNING_PAUSED
	this.Data["AGV_STATUS_RUNNING_STOPPED"] = scheduler.AGV_STATUS_RUNNING_STOPPED
	this.Data["AGV_LIFT_STATUS_UNLIFTED"] = scheduler.AGV_LIFT_STATUS_UNLIFTED
	this.Data["AGV_LIFT_STATUS_LIFTED"] = scheduler.AGV_LIFT_STATUS_LIFTED
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "controlIndex.tpl"
}

func (this *MainController) SetAGVLiftStatus() {
	agvID := this.GetString("agvID")
	status := this.GetString("status")
	scheduler.DebugOutput(fmt.Sprintf("设置AGV %s 举升状态为  %s ", agvID, status)+scheduler.GetFileLocation(), 2)
	if status == scheduler.AGV_LIFT_STATUS_UNLIFTED || status == scheduler.AGV_LIFT_STATUS_LIFTED {

		this.Data["json"] = ResponseMsg{
			Code:    0,
			Message: "",
		}
		scheduler.SetAGVLiftStatus(agvID, status)
	} else {
		this.Data["json"] = ResponseMsg{
			Code:    1,
			Message: "状态信息错误",
		}
	}
	this.ServeJson()
}
func (this *MainController) SetAGVStatus() {
	this.Data["json"] = ResponseMsg{
		Code:    0,
		Message: "",
	}
	agvID := this.GetString("agvID")
	status := this.GetString("status")
	scheduler.DebugOutput(fmt.Sprintf("SetAGVStatus => %s to %s ", agvID, status)+scheduler.GetFileLocation(), 2)
	scheduler.SetAGVStatus(agvID, status)
	this.ServeJson()
}
func (this *MainController) MoveAGV2XY() {
	this.Data["json"] = ResponseMsg{
		Code:    0,
		Message: "",
	}
	agvID := this.GetString("agvID")
	x := this.GetString("X")
	y := this.GetString("Y")
	scheduler.DebugOutput(fmt.Sprintf("操作AGV  %s 移动到 (%s, %s) ", agvID, x, y)+scheduler.GetFileLocation(), 2)
	// moveTo(agvID, x, y)
	scheduler.SetAgvDestination(agvID, x, y)
	this.ServeJson()
}

type AGVInfo4DataTable struct {
	Data scheduler.AGVInfoList `json:"data"`
}

func (this *MainController) AGVListIndex() {
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "AGVListIndex.tpl"
}
func (this *MainController) AgvList() {
	this.Data["json"] = AGVInfo4DataTable{Data: scheduler.G_agvInfoList}
	this.ServeJson()
}

type ProductInfo4DataTable struct {
	Data scheduler.ProductInfoList `json:"data"`
}

func (this *MainController) ProductInfoListIndex() {
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "ProductInfoListIndex.tpl"
}
func (this *MainController) ProductInfoList() {
	this.Data["json"] = ProductInfo4DataTable{Data: scheduler.G_ProductInfoList}
	this.ServeJson()
}
func (this *MainController) SetWarehouseShelfData() {
	response := ResponseMsg{Code: 0, Message: ""}
	defer func() {
		this.Data["json"] = response
		this.ServeJson()
	}()
	data := this.GetString("data")
	scheduler.DebugOutput(data+scheduler.GetFileLocation(), 3)
	shelves := scheduler.ShelfList{}
	if dataJson, err := simplejson.NewJson([]byte(data)); err == nil {
		fmt.Println(dataJson)
		count := len(dataJson.MustArray())
		for i := 0; i < count; i++ {
			shelfTemp := dataJson.GetIndex(i)
			fmt.Println(fmt.Sprintf("%+v", shelfTemp))
			id := shelfTemp.Get("ID").MustString()
			x := shelfTemp.Get("X").MustInt()
			y := shelfTemp.Get("Y").MustInt()
			shelves = append(shelves, scheduler.NewShelf(id, x, y))
		}
		//更新货架配置信息
		if err := scheduler.UpdateShelfInfoConfig(shelves); err == nil {
			scheduler.DebugOutput("更新货架信息成功 "+scheduler.GetFileLocation(), 2)
			scheduler.G_shelves = shelves
		} else {
			scheduler.DebugOutput("更新货架信息失败: "+err.Error()+scheduler.GetFileLocation(), 1)
			response = ResponseMsg{Code: 1, Message: "更新货架信息失败"}
			return
		}
	} else {
		response = ResponseMsg{Code: 1, Message: "解析数据出错"}
	}
	return
}
func (this *MainController) SetWarehouseConfigData() {
	response := ResponseMsg{Code: 0, Message: ""}
	defer func() {
		this.Data["json"] = response
		this.ServeJson()
	}()
	data := this.GetString("data")
	scheduler.DebugOutput(data+scheduler.GetFileLocation(), 3)

	if dataJson, err := simplejson.NewJson([]byte(data)); err == nil {

		scheduler.DebugOutput(dataJson.MustString()+scheduler.GetFileLocation(), 3)
		scheduler.G_rowCount = dataJson.Get("RowCount").MustInt()
		scheduler.G_colCount = dataJson.Get("ColCount").MustInt()
		scheduler.DebugOutput(fmt.Sprintf("row: %d | col: %d", scheduler.G_rowCount, scheduler.G_colCount), 2)
		if err2 := scheduler.UpdateConfig("RowCount", strconv.Itoa(scheduler.G_rowCount)); err2 != nil {
			scheduler.DebugOutput("更新RowCount失败"+scheduler.GetFileLocation(), 1)
			response = ResponseMsg{Code: 1, Message: "更新RowCount失败"}
			return
		}
		if err2 := scheduler.UpdateConfig("ColCount", strconv.Itoa(scheduler.G_colCount)); err2 != nil {
			scheduler.DebugOutput("更新ColCount失败"+scheduler.GetFileLocation(), 1)
			response = ResponseMsg{Code: 1, Message: "更新ColCount失败"}
			return
		}

		Points := dataJson.Get("Points")
		if PointArray, err1 := Points.Array(); err1 == nil {
			count := len(PointArray)
			scheduler.DebugOutput(fmt.Sprintf("共有 %d 个配置点上传", count)+scheduler.GetFileLocation(), 3)
			// 货架配置点单独存储
			shelves := scheduler.ShelfList{}
			points := scheduler.WarehouseConfigPointList{}
			for i := 0; i < count; i++ {
				point := Points.GetIndex(i)
				// beego.Warn(point)
				flag := scheduler.WarehousePointType(point.Get("BlockType").MustInt())
				x := point.Get("X").MustInt()
				y := point.Get("Y").MustInt()
				placer := point.Get("Placer").MustString()
				orientations := point.Get("Orientations").MustString()
				// strOrientations := []string{}
				// orientationsLen := len(orientations.MustArray())
				// for i := 0; i < orientationsLen; i++ {
				// 	strOrientations = append(strOrientations, orientations.GetIndex(i).MustString())
				// }
				// fmt.Println(strOrientations)
				points = append(points, scheduler.NewWarehouseConfigPoint(x, y, flag, placer, orientations))
				if flag == scheduler.ShelfPlace {
					shelves = append(shelves, scheduler.NewShelf(placer, x, y))
				}
				// switch flag {
				// case Railway, BannedArea, PickupPlace, RechargingArea:
				// 	points = append(points, NewWarehouseConfigPoint(x, y, flag, placer, strOrientations))
				// case ShelfPlace:
				// 	scheduler.DebugOutput("需要更改客户端配置协议"+scheduler.GetFileLocation(), 1)
				// 	shelves = append(shelves, NewShelf(placer, x, y))
				// }
			}
			//更新区域配置点
			if err := scheduler.UpdateWarehousePointConfig(points); err == nil {
				scheduler.DebugOutput(points.String()+scheduler.GetFileLocation(), 3)
				scheduler.DebugOutput("更新仓库配置点成功 "+scheduler.GetFileLocation(), 2)
				scheduler.G_WarehouseConfigPointList = append(scheduler.WarehouseConfigPointList{}, points...)
			} else {
				scheduler.DebugOutput("更新仓库配置点失败: "+err.Error()+scheduler.GetFileLocation(), 1)
				response = ResponseMsg{Code: 1, Message: "更新仓库配置点失败"}
				return
			}
			//更新货架配置信息
			if err := scheduler.UpdateShelfInfoConfig(shelves); err == nil {
				scheduler.DebugOutput("更新货架信息成功 "+scheduler.GetFileLocation(), 2)
				scheduler.G_shelves = shelves
			} else {
				scheduler.DebugOutput("更新货架信息失败: "+err.Error()+scheduler.GetFileLocation(), 1)
				response = ResponseMsg{Code: 1, Message: "更新货架信息失败"}
				return
			}
		} else {
			scheduler.DebugOutput(err1.Error()+scheduler.GetFileLocation(), 1)
			response = ResponseMsg{Code: 1, Message: "系统异常"}
			return
			// goto END
		}
	} else {
		scheduler.DebugOutput(err.Error()+scheduler.GetFileLocation(), 1)
		response = ResponseMsg{Code: 1, Message: "系统异常"}
		return
	}
}

func (this *MainController) WarehouseConfigData() {
	scheduler.G_warehouseConfig.ColCount = scheduler.G_colCount
	scheduler.G_warehouseConfig.RowCount = scheduler.G_rowCount
	scheduler.G_warehouseConfig.Points = scheduler.G_WarehouseConfigPointList
	// warehouseConfig.Points = G_configPointsList

	this.Data["json"] = scheduler.G_warehouseConfig
	this.ServeJson()
}
func (this *MainController) WarehouseConfigIndex() {
	// this.Data["Website"] = "beego.me"
	// this.Data["Email"] = "astaxie@gmail.com"
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "warehouseConfigIndex.tpl"
}

type ShelfInfo4DataTable struct {
	Data scheduler.ShelfList `json:"data"`
}

func (this *MainController) ShelfList() {
	this.Data["json"] = ShelfInfo4DataTable{Data: scheduler.G_shelves}
	this.ServeJson()
}
