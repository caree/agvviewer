package controllers

import (
	scheduler "agvViewer/agvScheduler"
	"encoding/json"
	// "fmt"
	"github.com/astaxie/beego"
	"github.com/gorilla/websocket"
	"net/http"
	"startbeego/models"
	// "strings"
)

// WebSocketController handles WebSocket requests.
type WebSocketController struct {
	beego.Controller // Embed struct that has stub implementation of the interface.
}

// Join method handles WebSocket requests for WebSocketController.
func (this *WebSocketController) Join() {
	requestURI := this.Ctx.Request.RequestURI
	beego.Trace(requestURI)
	// return

	// Upgrade from http request to WebSocket.
	ws, err := websocket.Upgrade(this.Ctx.ResponseWriter, this.Ctx.Request, nil, 1024, 1024)
	if _, ok := err.(websocket.HandshakeError); ok {
		http.Error(this.Ctx.ResponseWriter, "Not a websocket handshake", 400)
		return
	} else if err != nil {
		beego.Error("Cannot setup WebSocket connection:", err)
		return
	}
	// beego.Debug(requestURI)
	// beego.Trace(ws.LocalAddr())
	// Join chat room.
	Join(requestURI, ws)
	defer Leave(requestURI)

	// Message receive loop.
	for {
		_, p, err := ws.ReadMessage()
		if err != nil {
			return
		}
		publish <- newEvent(models.EVENT_MESSAGE, requestURI, string(p))
	}
}

// broadcastWebSocket broadcasts messages to WebSocket users.
func broadcastWebSocket(event *scheduler.EventMsg) {
	// func broadcastWebSocket(event models.Event, namePrefix string) {
	scheduler.DebugOutput("broadcastWebSocket => "+event.String()+scheduler.GetFileLocation(), 3)
	data, err := json.Marshal(event)
	if err != nil {
		beego.Error("Fail to marshal event:", err)
		return
	}

	// for sub := subscribers.Front(); sub != nil; sub = sub.Next() {
	for _, sub := range subscribers {
		// Immediately send event to WebSocket users.
		var ws *websocket.Conn = nil
		ws = sub.Conn

		// if len(namePrefix) > 0 && strings.HasPrefix(sub.Name, namePrefix) {
		// 	// if len(namePrefix) > 0 && strings.HasPrefix(sub.Value.(Subscriber).Name, namePrefix) {
		// 	ws = sub.Conn
		// 	// ws = sub.Value.(Subscriber).Conn

		// } else {
		// 	ws = sub.Conn
		// 	// beego.Warn("no prefix")
		// }
		if ws != nil {
			if ws.WriteMessage(websocket.TextMessage, data) != nil {
				// User disconnected.
				unsubscribe <- sub.Name
			}
		}

	}
}
