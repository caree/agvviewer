// Copyright 2013 Beego Samples authors
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package controllers

import (
	// "container/list"
	"time"

	"github.com/astaxie/beego"
	"github.com/gorilla/websocket"
	"startbeego/models"
	// "strings"
	// "github.com/beego/samples/WebIM/models"
)

type Subscription struct {
	Archive []models.Event      // All the events from the archive.
	New     <-chan models.Event // New events coming in.
}

func newEvent(ep models.EventType, user, msg string) models.Event {
	return models.Event{ep, user, int(time.Now().Unix()), msg}
}

func Join(user string, ws *websocket.Conn) {
	subscribe <- Subscriber{Name: user, Conn: ws}
}

func Leave(user string) {
	unsubscribe <- user
}

type Subscriber struct {
	Name       string
	Conn       *websocket.Conn // Only for WebSocket users; otherwise nil.
	RegionCode string
}

func (this *Subscriber) addRegionCode(code string) {
	this.RegionCode = code
}

type SubscriberList []Subscriber

func (this *SubscriberList) remove(subscriber Subscriber) {
	for i, sub := range *this {
		if sub.Name == subscriber.Name {
			if i <= 0 {
				*this = (*this)[1:]
			} else {
				*this = append((*this)[0:i], (*this)[i+1:]...)
			}
		}
	}
}
func (this SubscriberList) getRegionCode(uri string) (string, bool) {
	for _, sub := range this {
		if sub.Name == uri {
			return sub.RegionCode, true
		}
	}
	return "", false
}
func (this *SubscriberList) addRegionCode(uri, code string) {
	for i, sub := range *this {
		if sub.Name == uri {
			(&((*this)[i])).addRegionCode(code)
		}
	}
}

func (this *SubscriberList) addNew(subscriber Subscriber) {
	(*this) = append(*this, subscriber)
}

var (
	// Channel for new join users.
	subscribe = make(chan Subscriber, 10)
	// Channel for exit users.
	unsubscribe = make(chan string, 10)
	// Send events here to publish them.
	publish = make(chan models.Event, 10)
	// Long polling waiting list.
	// waitingList = list.New()
	// subscribers = list.New()

	// waitingList SubscriberList = SubscriberList{}
	subscribers SubscriberList = SubscriberList{}
)

// This function handles all incoming chan messages.
func wsroom() {
	for {
		select {
		case sub := <-subscribe:
			if !isUserExist(subscribers, sub.Name) {
				// subscribers.PushBack(sub) // Add user to the end of list.
				subscribers.addNew(sub)
				// Publish a JOIN event.
				publish <- newEvent(models.EVENT_JOIN, sub.Name, "")
				beego.Info("New user:", sub.Name, ";WebSocket:", sub.Conn != nil)
			} else {
				beego.Info("Old user:", sub.Name, ";WebSocket:", sub.Conn != nil)
			}
		case event := <-publish:
			// Notify waiting list.
			// for ch := waitingList.Back(); ch != nil; ch = ch.Prev() {
			// 	ch.Value.(chan bool) <- true
			// 	waitingList.Remove(ch)
			// }

			// broadcastWebSocket(event, "")
			models.NewArchive(event)

			if event.Type == models.EVENT_MESSAGE {
				beego.Info("Message from", event.User, ";Content:", event.Content)
				// if strings.HasPrefix(event.Content, regionCodePrefix) {
				// 	beego.Debug("接收到区域码")
				// 	//将ws与区域编码关联在一起
				// 	subscribers.addRegionCode(event.User, event.Content)
				// 	addCodeBar(event.Content, event.Content)
				// } else {
				// 	beego.Debug("接收到图书码")
				// 	//根据区域处理新编码
				// 	if regionCode, b := subscribers.getRegionCode(event.User); b {
				// 		beego.Debug("查找到到区域码 ", regionCode)
				// 		addCodeBar(event.Content, regionCode)
				// 	}
				// }
			}
		case unsub := <-unsubscribe:
			for _, sub := range subscribers {
				// for sub := subscribers.Front(); sub != nil; sub = sub.Next() {
				if sub.Name == unsub {
					subscribers.remove(sub)
					// Clone connection.
					ws := sub.Conn
					if ws != nil {
						ws.Close()
						beego.Error("WebSocket closed:", unsub)
					}
					publish <- newEvent(models.EVENT_LEAVE, unsub, "") // Publish a LEAVE event.
					break
				}
			}
		}
	}
}

func init() {
	go wsroom()
}

func isUserExist(subscribers SubscriberList, user string) bool {
	for _, sub := range subscribers {
		// for sub := subscribers.Front(); sub != nil; sub = sub.Next() {
		if sub.Name == user {
			return true
		}
	}
	return false
}
