package agvScheduler

import (
	// "errors"
	"fmt"
	// "time"
)

type AGVTaskInfo struct {
	ID                 string
	IsDoingTask        bool
	AGV                *AGV
	Tasks              TaskList
	TaskExecuteHistory []*AgvData
	// ExecuteMoving
	// ExecuteLifting
}

func (this *AGVTaskInfo) TaskExecuteHistoryString() string {
	str := ""
	for _, dataTemp := range this.TaskExecuteHistory {
		str += " => " + dataTemp.Summarize()
	}
	return str
}
func (this *AGVTaskInfo) ToString() string {
	str := "\r\n"
	for i, task := range this.Tasks {
		str += fmt.Sprintf("      %d: %s \r\n", i+1, task.Summarize())
	}
	// DebugOutput(str+GetFileLocation(), 3)
	return fmt.Sprintf("ID: %s tasks: %s", this.ID, str)
}

//试着去执行任务，任务失败返回错误信息
func (this *AGVTaskInfo) doTask(task *AGVTaskItem) error {
	DebugOutput(this.ID+" 执行任务："+task.Summarize()+GetFileLocation(), 2)
	switch task.TaskType {
	case TASK_TYPE_MOVE_TO:
		if list, err := this.AGV.TryFindAGVPath(task.DestX, task.DestY); err != nil {
			//任务执行失败,标记为停止执行状态
			this.IsDoingTask = false
			// DebugOutput(this.ID+" 计算路径失败"+GetFileLocation(), 3)
			return err
		} else {
			if list != nil {
				// if this.ExecuteMoving != nil {
				// 	pointTemp := list[len(list)-2]
				// 	this.ExecuteMoving(this.ID, pointTemp.X, pointTemp.Y)
				// } else {
				// 	DebugOutput(this.ID+" 无法输出行走路线"+GetFileLocation(), 1)
				// 	return NewAgvError(AGV_ERROR_CODE_任务执行没有输出接口, ("无法输出行走路线"))
				// }
			} else {
				DebugOutput(this.ID+" 计算路径完成"+GetFileLocation(), 3)
			}
		}

	case TASK_TYPE_LIFT:
		// if this.ExecuteLifting != nil {
		// 	this.ExecuteLifting(this.ID, task.LiftStatus)
		// } else {
		// 	DebugOutput(this.ID+" 无法输出举升指令"+GetFileLocation(), 1)
		// 	return NewAgvError(AGV_ERROR_CODE_任务执行没有输出接口, ("无法输出举升指令"))
		// }
	}
	return nil
}
func (this *AGVTaskInfo) ResetTaskDoingStatus(b bool) {
	this.IsDoingTask = b
}

//检查任务完成情况，如果需要，执行新任务
// true 没有任务或者正确执行了任务
// false 执行任务出错
func (this *AGVTaskInfo) checkUpTask() error {
	if len(this.Tasks) <= 0 {
		this.ResetTaskDoingStatus(false)
		return nil
	}
	this.ResetTaskDoingStatus(true)
	if this.Tasks[0].TryEnd(this.AGV.X, this.AGV.Y, this.AGV.LiftStatus) { //看一下当前的任务是否已经完成
		DebugOutput(fmt.Sprintf("AGV %s 任务结束  %s", this.AGV.ID, this.Tasks[0].Summarize())+GetFileLocation(), 2)

		//已经完成一个任务，下面应该开始下一个任务，如果没有任务直接返回
		this.Tasks = this.Tasks[1:]
		return this.checkUpTask()
	} else {
		//任务没有完成，尝试去做该做的事
		if err := this.doTask(this.Tasks[0]); err != nil {
			DebugOutput(fmt.Sprintf("AGV %s 任务失败  %s,可能原因：%s", this.AGV.ID, this.Tasks[0].Summarize(), err.Error())+GetFileLocation(), 2)
			this.ResetTaskDoingStatus(false)
			return err
		}
	}
	return nil
}
func (this *AGVTaskInfo) AddTasks(taskItems TaskList) error {
	this.Tasks = this.Tasks.AddTasks(taskItems)
	return nil
}
func (this *AGVTaskInfo) AddTask(taskItem *AGVTaskItem) error {
	this.Tasks = this.Tasks.AddTask(taskItem)
	return nil
}

func (this *AGVTaskInfo) ClearTasks() {
	this.Tasks = TaskList{}
}

//无参数唤醒任务，当因为执行任务出错而停止任务时，重新开始任务执行
func (this *AGVTaskInfo) StartUpTask() error {
	// return this.checkUpTask()
	if this.IsDoingTask == true {
		return nil
	} else {
		return this.checkUpTask()
	}
}
func (this *AGVTaskInfo) AddTaskExecuteHistory(data *AgvData) {
	this.TaskExecuteHistory = append(this.TaskExecuteHistory, data) //保存历史
	if len(this.TaskExecuteHistory) > 20 {
		this.TaskExecuteHistory = this.TaskExecuteHistory[1:]
	}
}

//更新任务数据，如果任务已经停止，因为已经更新数据，可以尝试重新开始任务，相当于有参数唤醒任务
func (this *AGVTaskInfo) AcceptData(data *AgvData) error {
	if data != nil {
		switch data.DataType {
		case ACCEPTED_CMD_TYPE_HEART_BEATING:
		case ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED:
			if err := this.AGV.UpdatePosition(data.X, data.Y); err == nil {
				this.AddTaskExecuteHistory(data)
				if err := this.checkUpTask(); err == nil {
					return nil //位置信息已经更新
				} else {
					return err
				}
			} else {
				return err //位置信息没有更新
			}
		case ACCEPTED_CMD_TYPE_AGV_STATUS_CHANGED:
		case ACCEPTED_CMD_TYPE_AGV_LIFT_STATUS_CHANGED:
			if err := this.AGV.UpdateLiftStatus(data.LiftStatus); err == nil {
				this.AddTaskExecuteHistory(data)
				if err := this.checkUpTask(); err == nil {
					return nil //更新了举升状态信息
				} else {
					return err
				}
			} else {
				return err //举升状态没有更新
			}
		}
	}
	return ErrData
	// return NewAgvError(AGV_ERROR_CODE_数据异常, ("数据无法处理"))
}

func NewAGVTaskInfo(id string, x, y int) *AGVTaskInfo {
	return &AGVTaskInfo{
		ID:                 id,
		AGV:                NewAGV(id, x, y, nil, nil),
		Tasks:              TaskList{},
		TaskExecuteHistory: []*AgvData{},
	}
}

// ------------------------------------------------------------------------------------------

type AGVTaskList []*AGVTaskInfo

func (this AGVTaskList) ClearAGVTasks(id string) error {
	for _, agvTaskTemp := range this {
		if agvTaskTemp.ID == id {
			agvTaskTemp.ClearTasks()
			return nil
		}
	}
	return ErrNoSuchAGV
	// return NewAgvError(AGV_ERROR_CODE_逻辑异常, ("没有找到该AGV"))
}
func (this AGVTaskList) GetIdleTaskInfo() (*AGVTaskInfo, error) {
	for _, agvTaskTemp := range this {
		if len(agvTaskTemp.Tasks) <= 0 {
			return agvTaskTemp, nil
		}
	}
	return nil, ErrNoIdleAGV
	// return nil, NewAgvError(AGV_ERROR_CODE_逻辑异常, ("没有空闲的AGV"))
}
func (this AGVTaskList) AddAGVTaskInfo(agvTaskInfo *AGVTaskInfo) AGVTaskList {
	for _, agvTaskInfoTemp := range this {
		if agvTaskInfoTemp.ID == agvTaskInfo.ID {
			return this
		}
	}
	this = append(this, agvTaskInfo)
	DebugOutput("新AGV任务管理信息："+agvTaskInfo.ToString()+GetFileLocation(), 2)

	// if err := agvTaskInfo.AGV.InitAGVonMap(); err != nil {
	// 	DebugOutput(err.Error()+GetFileLocation(), 1)
	// }
	return this
}

//为指定AGV添加任务
func (this AGVTaskList) AddTasksToAGV(id string, taskItems TaskList) error {
	for _, agvTaskTemp := range this {
		if agvTaskTemp.ID == id {
			return agvTaskTemp.AddTasks(taskItems)
		}
	}
	// return NewAgvError(AGV_ERROR_CODE_逻辑异常, ("没有该AGV"))
	return ErrNoSuchAGV
}
func (this AGVTaskList) AddTaskToAGV(id string, taskItem *AGVTaskItem) error {
	for _, agvTaskTemp := range this {
		if agvTaskTemp.ID == id {
			return agvTaskTemp.AddTask(taskItem)
		}
	}
	return nil
}
func (this AGVTaskList) StartUpSpecialAGVTask(id string) {
	for _, agvTaskTemp := range this {
		if agvTaskTemp.ID == id {
			agvTaskTemp.StartUpTask()
		}
	}
}
func (this AGVTaskList) StartUpTask() {
	for _, agvTaskTemp := range this {
		if err := agvTaskTemp.StartUpTask(); err != nil {
			DebugOutput(err.Error()+GetFileLocation(), 2)
		}
	}
}
func (this AGVTaskList) GetAGVCarryingShelf(x, y int) (*AGVTaskInfo, error) {
	for _, agvTaskTemp := range this {
		if agvTaskTemp.AGV.Shelf == nil {
			continue
		}
		if agvTaskTemp.AGV.Shelf.X == x && agvTaskTemp.AGV.Shelf.Y == y {
			return agvTaskTemp, nil
		}
	}
	// return nil, NewAgvError(AGV_ERROR_CODE_逻辑异常, ("没有找到该货架"))
	return nil, ErrNoSuchShelf
}

//如果没有找到，返回错误信息，否则返回执行信息
func (this AGVTaskList) AcceptData(data *AgvData) (*AGVTaskInfo, error) {
	//找到对应的AGV，将数据交由其处理
	for _, agvTaskTemp := range this {
		if agvTaskTemp.ID == data.ID {
			return agvTaskTemp, agvTaskTemp.AcceptData(data)
		}
	}
	// return nil, NewAgvError(AGV_ERROR_CODE_数据异常, ("没有该AGV ID"))
	return nil, ErrNoSuchAGV
}

func (this AGVTaskList) Strings() []string {
	strs := []string{}
	for _, agvTaskTemp := range this {
		strs = append(strs, agvTaskTemp.ToString())
		strs = append(strs, agvTaskTemp.TaskExecuteHistoryString())
	}
	return strs
}
