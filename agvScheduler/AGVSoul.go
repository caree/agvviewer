package agvScheduler

import (
	// "encoding/json"
	"fmt"
	// "github.com/bitly/go-simplejson"
	// "errors"
	"time"
)

/*
	说明
	=====================

	AGVSoul是AGV在服务端的一个映射，主要负责在服务端与AGV进行沟通，保证AGV命令的接收，监测AGV的运行状态并代表AGV和服务端相关模块交互
	与其它模块通过channel交互
	---------------------

	- AGVSoul启动后，处于未初始化状态。状态的初始化center发送的数据完成，类似于心跳。
	- AGVSoul会接收AGV的数据，以此监控AGV的状态，包括AGV的失联状态、正常运行，并根据情况将状态发送给任务分发中心
	- AGVSoul负责确认agv接收到具体的执行命令，执行命令过程中，会将执行任务的情况发送给任务分发中心
	- AGVSoul接收到任务执行指令后，根据AGV状态指示AGV执行指令

*/

//执行移动命令，通常是发送一系列的点给AGV
// type ExecuteMovingPoints func(id string, list PointPointerList)
type TaskCommandSender func(agvID string, task *TaskCommand)

var (
	TICKER_INTERVAL time.Duration = 3 * time.Second
)

func NewAGVSoul(id string, sender TaskCommandSender) *AGVSoul {
	soul := AGVSoul{
		ID:                id,
		Status:            AGV_STATUS_UNCONNECTED,
		ChIn_TaskEndRing:  make(chan *TaskCommand),
		ChIn_AGVdata:      make(chan *AgvData, 100),
		TaskCommandSender: sender,
		// ExecuteMovingPoints: exeMove,
		// ExecuteLifting:      exeLift,
		// AdvicePath:          PointPointerList{},
	}
	return &soul
}

//只有当Task为空的时候才能被赋予新任务，即每次只执行一个任务
type AGVSoul struct {
	ID                string
	Status            AGV_Status        // AVG状态
	CommandSended     bool              //命令是否已经发送给AGV；一般不需要重复发送，除非AGV在完成任务前断线重连
	TaskCommand       *TaskCommand      //当前执行的命令
	TaskCommandSender TaskCommandSender //将命令发给AGV
	TimeStamp         time.Time         //监测AGV运行状态的时间标识
	// ChTicker          <-chan time.Time
	ChIn_AGVdata     chan *AgvData     //AGV运行状态信息的传入通道
	ChIn_TaskEndRing chan *TaskCommand //从center传送过来，提醒查看任务完成情况
	// ExecuteMovingPoints
	// ExecuteLifting
}

func (this *AGVSoul) String() string {
	return fmt.Sprintf("ID: %s Status: %s  ", this.ID, this.Status)
}
func (this *AGVSoul) StartRunning() {
	// this.ChTicker = time.Tick(TICKER_INTERVAL)
	DebugOutput(fmt.Sprintf("AGV %s 开始运行 ..."+GetFileLocation(), this.ID), 2)
	go func() {
		for {
			select {
			// case timeTicker := <-this.ChTicker:
			// 	if timeTicker.Sub(this.TimeStamp) > TICKER_INTERVAL {
			// 		DebugOutput("AGVSoul定时发起任务。。。"+GetFileLocation(), 3)
			// 	}
			case taskCommand := <-this.ChIn_TaskEndRing:
				// DebugOutput(fmt.Sprintf("AGVSoul(%s)接收到命令：%s", this.ID, taskCommand.String())+GetFileLocation(), 3)
				this.ReceiveCommand(taskCommand)
			case data := <-this.ChIn_AGVdata:
				this.UpdateStatus(data)
			}
		}
	}()
}

//保持AGV的状态
//初始化后默认是失联状态，有信息更新后，处于可运行状态
//AGV可以变为充电、暂停运行等状态，这些状态与AGV能否执行任务直接相关
func (this *AGVSoul) UpdateStatus(data *AgvData) {

}
func (this *AGVSoul) ReceiveCommand(taskCommand *TaskCommand) {
	if this.TaskCommand == nil || this.TaskCommand.IsTheSame(taskCommand) == false {
		DebugOutput(fmt.Sprintf("AGVSoul(%s) 设置新命令：%s", this.ID, taskCommand.String())+GetFileLocation(), 2)
		this.TaskCommand = taskCommand
		this.SetCommandSendStatus(false)
	}
	if this.CommandSended == true {
		return
	}
	if this.TaskCommandSender != nil {
		this.TaskCommandSender(this.ID, this.TaskCommand)
		this.SetCommandSendStatus(true)

	} else {
		DebugOutput("无法输出命令"+GetFileLocation(), 3)

	}
}
func (this *AGVSoul) SetCommandSendStatus(sended bool) {
	this.CommandSended = sended
}

//============================================================================================================

type AGVSoulList []*AGVSoul

func (this AGVSoulList) StartRunning() {
	for _, agvSoul := range this {
		agvSoul.StartRunning()
	}
}
func (this AGVSoulList) ToAGVSoulCore() AGVSoulCoreList {
	agvCores := AGVSoulCoreList{}
	for _, soul := range this {
		agvCores = append(agvCores, NewAGVSoulCore(soul.ID, soul.ChIn_TaskEndRing))
	}
	return agvCores
}

func (this AGVSoulList) FindAGVbyID(id string) (*AGVSoul, error) {
	for _, agvSoul := range this {
		if agvSoul.ID == id {
			return agvSoul, nil
		}
	}
	// return nil, NewLogicError("不存在该AGV")
	return nil, ErrNoSuchAGV
}
