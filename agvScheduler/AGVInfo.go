package agvScheduler

import (
// "encoding/json"
// "fmt"
// "github.com/astaxie/beego"
// "github.com/bitly/go-simplejson"
// "errors"
// "time"
)

//============================================================================================================

type AGVInfo struct {
	ID          string
	OverLoad    int
	SelfWeight  int
	Size        string
	DesignSpeed string
	AGV         *AGVSoulCore
}

func (this *AGVInfo) SetAGV(agv *AGVSoulCore) {
	this.AGV = agv
}

type AGVInfoList []*AGVInfo

func (this AGVInfoList) GetAGVInfoByID(id string) *AGVInfo {
	for _, agvInfoTemp := range this {
		if agvInfoTemp.ID == id {
			return agvInfoTemp
		}
	}
	return nil
}

func (this AGVInfoList) SetAGV(agv *AGVSoulCore) {
	for _, agvInfo := range this {
		if agvInfo.ID == agv.ID {
			agvInfo.SetAGV(agv)
		}
	}
}

// func (this AGVInfoList) UpdateStatus(id, status string) (*AGV, error) {
// 	for _, agvInfoTemp := range this {
// 		if agvInfoTemp.ID == id {
// 			if err := agvInfoTemp.AGV.UpdateStatus(status); err == nil {
// 				return agvInfoTemp.AGV, nil
// 			} else {
// 				return agvInfoTemp.AGV, err
// 			}
// 		}
// 	}
// 	return nil, NewAgvError(AGV_ERROR_CODE_数据异常, ("该AGV不存在"))
// }
