package agvScheduler

import (
// "encoding/json"
// "fmt"
// "github.com/astaxie/beego"
// "github.com/bitly/go-simplejson"
)

type TaskInfo struct {
	ID      string
	DestX   int
	DestY   int
	ShelfID string
}
