package agvScheduler

import (
	"fmt"
)

type EventMsg struct {
	Type int //
	// Content   string
	AGV *AGVTransferred
}
type AGVTransferred struct {
	ID string
	// Status     string // AVG状态
	LiftStatus string //举升状态
	X          int
	Y          int
	Shelf      *ShelfInfo
}

func NewAGVTransferred(id, liftStatus string, x, y int, shelf *ShelfInfo) *AGVTransferred {
	return &AGVTransferred{
		ID:         id,
		LiftStatus: liftStatus,
		X:          x,
		Y:          y,
		Shelf:      shelf,
	}
}

func (this *EventMsg) String() string {
	return fmt.Sprintf("Type: %d  %s", this.Type, this.AGV)
}
func NewEventMsg(eventType int, agv *AGVTransferred) *EventMsg {
	return &EventMsg{
		Type: eventType,
		AGV:  agv,
	}
}
