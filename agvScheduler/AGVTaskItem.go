package agvScheduler

import (
	// "errors"
	"fmt"
	// "time"
)

type AGVTaskItem struct {
	AGVID      string //指定执行的AGV
	TaskType   int
	LiftStatus string
	DestX      int
	DestY      int
	Ended      bool
	GUID       int64
	GroupID    string //所属的组，一组的任务通常是需要连续执行的
}

func (this *AGVTaskItem) Summarize() string {
	str := fmt.Sprintf("AGV(%s)", this.AGVID)
	switch this.TaskType {
	case TASK_TYPE_MOVE_TO:
		return str + fmt.Sprintf("移动到(%d, %d)", this.DestX, this.DestY)
	case TASK_TYPE_LIFT:
		switch this.LiftStatus {
		case AGV_LIFT_STATUS_UNLIFTED:
			return str + "收起举升"
		case AGV_LIFT_STATUS_LIFTED:
			return str + "进行举升"
		}
	}
	return str + "任务无法描述"
}
func (this *AGVTaskItem) String() string {
	return fmt.Sprintf("task type: %d LiftStatus: %s DestX: %d DestY: %d", this.TaskType, this.LiftStatus, this.DestX, this.DestY)
}
func (this *AGVTaskItem) TryEnd(x, y int, liftStatus string) bool {
	switch this.TaskType {
	case TASK_TYPE_MOVE_TO:
		if x == this.DestX && y == this.DestY {
			this.Ended = true
			return true
		}
	case TASK_TYPE_LIFT:
		if liftStatus == this.LiftStatus {
			this.Ended = true
			return true
		}
	}
	return false
}
func (this *AGVTaskItem) IsSame(task *AGVTaskItem) bool {
	return this.GUID == task.GUID
}
func NewTaskItem(id string, liftStatus string, x, y, taskType int) *AGVTaskItem {
	guid := GetGUID()
	return &AGVTaskItem{
		AGVID:      id,
		TaskType:   taskType,
		DestX:      x,
		DestY:      y,
		LiftStatus: liftStatus,
		GUID:       guid,
	}
}

//-------------------------------------------------------------------

type TaskList []*AGVTaskItem

func (this TaskList) AddTasks(taskItems TaskList) TaskList {
	return append(this, taskItems...)
}
func (this TaskList) AddTask(taskItem *AGVTaskItem) TaskList {
	return append(this, taskItem)
}
