package agvScheduler

import (
// "fmt"
)

const (
	TASK_TYPE_MOVE_TO int = 1
	TASK_TYPE_LIFT    int = 2
)
const (
	AGV_LIFT_STATUS_UNLIFTED string = "0"
	AGV_LIFT_STATUS_LIFTED   string = "1"
)
const (
	AGV_STATUS_UNCONNECTED     AGV_Status = "0"
	AGV_STATUS_RUNNING         AGV_Status = "1"
	AGV_STATUS_RUNNING_PAUSED  AGV_Status = "2"
	AGV_STATUS_RUNNING_STOPPED AGV_Status = "3"
)
const MAX_STEP_COUNT = 2 //最小值为2

//地图的默认行列数
const (
	DEFAULT_ROWCOUNT int = 20
	DEFAULT_COLCOUNT int = 20
)

//接收到的agv信息类型
const (
	ACCEPTED_CMD_TYPE_HEART_BEATING           string = "1" //心跳
	ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED    string = "2" //位置变化
	ACCEPTED_CMD_TYPE_AGV_STATUS_CHANGED      string = "3" //agv状态变化
	ACCEPTED_CMD_TYPE_AGV_LIFT_STATUS_CHANGED string = "4" //举升状态变化
)

//发送给agv的控制命令
const (
	CMD_MOVE_AGV       = "31"
	CMD_SET_AGV_STATUS = "32"
	CMD_SET_NEXT_STEP  = "33"
	CMD_SET_AGV_LIFT   = "34"
)
const (
	EVENT_JOIN             = 0 //0
	EVENT_AGV_HEARTBEATING = 1
	EVENT_POSITION_NEW     = 2
	EVENT_STATUS_NEW       = 3
	EVENT_LIFT_STATUS_NEW  = 4

	maxRead = 1024
)
