package agvScheduler

import (
	"fmt"
	// "strings"
	// "strconv"
	// "time"
)

//AGVSoul在center中的副本
type AGVSoulCore struct {
	ID                    string
	LiftStatus            string //举升状态,true，举升，false，未举升
	lastLiftStatus        string `json:"-"`
	X                     int
	lastX                 int `json:"-"`
	Y                     int
	lastY                 int                 `json:"-"`
	GUID                  int64               `json:"-"` //当前点在的guid，判断当前点是否
	Task                  *AGVTaskItem        `json:"-"` //正在执行的任务
	TaskInitialized       bool                `json:"-"`
	TaskCommand           *TaskCommand        `json:"-"`
	Center                *PathCaculateCenter `json:"-"`
	ChOut_TaskExecuteRing chan *TaskCommand   `json:"-"` //AGVSoul接收数据的管道
	Path                  PathPointList       `json:"-"`
}

func NewAGVSoulCore(id string, ring chan *TaskCommand) *AGVSoulCore {
	// func NewAGVSoulCore(id, liftStatus string, x, y int, ring chan *AgvData) *AGVSoulCore {
	return &AGVSoulCore{
		ID: id,
		// LiftStatus: liftStatus,
		// X:          x,
		// Y:          y,
		ChOut_TaskExecuteRing: ring,
	}
}

func (this *AGVSoulCore) String() string {
	return fmt.Sprintf("ID: %s LiftStatus: %s  X: %d, Y: %d ", this.ID, this.LiftStatus, this.X, this.Y)
}

//执行被赋予的任务
func (this *AGVSoulCore) ExecuteTask() {
	if this.IsTaskInitialized() == false {
		if err := this.InitTask(); err != nil {
			DebugOutput(err.Error()+GetFileLocation(), 2)
			return
		}
	}
	if this.Task == nil { //任务执行完毕，等待新任务
		return
	}
	switch this.Task.TaskType {
	case TASK_TYPE_MOVE_TO:
		if this.Path != nil && len(this.Path) > 0 {
			if this.TaskCommand == nil { //新任务添加
				path := this.Center.GetAdvicedPath(this.ID, this.Path)
				this.TaskCommand = NewTaskCommand(TASK_TYPE_MOVE_TO, path, "")
			} else {
				lastPath := this.TaskCommand.Path
				newPath := this.Center.GetAdvicedPath(this.ID, this.Path)
				if lastPath.CompareTo(newPath) == true {
					//如果路径完全相同，或者只是删除了已经走过的点，认为路径相同
					this.TaskCommand.Path = newPath
				} else {
					DebugOutput(fmt.Sprintf("AGV %s 路径不相同，重新设置命令", this.ID)+GetFileLocation(), 3)
					this.TaskCommand = NewTaskCommand(TASK_TYPE_MOVE_TO, newPath, "")
				}
			}
		}

	case TASK_TYPE_LIFT:
		this.TaskCommand = NewTaskCommand(TASK_TYPE_LIFT, nil, this.Task.LiftStatus)

	}
	this.SendTaskCommand()
}
func (this *AGVSoulCore) SendTaskCommand() {
	if this.TaskCommand != nil {
		if this.ChOut_TaskExecuteRing == nil {
			DebugOutput("无法输出执行命令"+GetFileLocation(), 2)
			return
		}
		this.ChOut_TaskExecuteRing <- this.TaskCommand
	}
}
func (this *AGVSoulCore) SetTask(task *AGVTaskItem) error {
	if this.Task != nil && this.Task.GUID == task.GUID {
		// return NewLogicError("任务重复设置")
		return ErrTaskDuplicated
	}
	this.Task = task
	this.TaskInitialized = false
	return nil
}

//是否已经完成了初始化过程，可以开始工作
func (this *AGVSoulCore) IsInitialized() bool {
	if this.X > 0 && this.Y > 0 {
		return true
	}
	return false
}
func (this *AGVSoulCore) IsTaskInitialized() bool {
	return this.TaskInitialized
}
func (this *AGVSoulCore) UpdatePosition(x, y int) bool {
	if this.X != x || this.Y != y {
		this.lastX = this.X
		this.X = x
		this.lastY = this.Y
		this.Y = y
		DebugOutput(fmt.Sprintf("AGVCore %s 位置变化: (%d, %d) => (%d, %d)", this.ID, this.lastX, this.lastY, this.X, this.Y)+GetFileLocation(), 2)
		//需要检查是否按照之前设计的路线行驶，如果否，需要重新计算路径
		if len(this.Path) > 0 && this.Path.HasPoint(this.X, this.Y) == false {
			this.TaskInitialized = false
			DebugOutput("AGV 未按照设计路径行走，重新计算路线"+GetFileLocation(), 2)
			return true
		}

		this.Center.CheckAGVCollision() //测试时检查碰撞
		// this.Center.WakeUpAGVTasks()

		if this.lastX > 0 && this.lastY > 0 {
			if err := this.Center.WareHouseMap.UnblockPoint(this.ID, this.lastX, this.lastY); err != nil {
				DebugOutput(err.Error()+GetFileLocation(), 1)
				return false
			}
		}
		if this.X > 0 && this.Y > 0 {
			if err := this.Center.WareHouseMap.BlockPoint(this.ID, x, y); err != nil {
				DebugOutput(fmt.Sprintf("AGV %s 试图锁定点(%d, %d)时失败：", this.ID, this.X, this.Y)+err.Error()+GetFileLocation(), 1)
				return false
			}
		}

		this.Path = this.Path.RemoveExpiredPoints(this.X, this.Y)
		this.Center.CheckRushIntoOthersArea(this.ID, this.X, this.Y)
		return true
	} else {
		return false
	}
}
func (this *AGVSoulCore) UpdateLiftStatus(status string) bool {
	if this.LiftStatus != status {
		this.lastLiftStatus = this.LiftStatus
		this.LiftStatus = status
		return true
	}
	return false
}

//接收到AGV的反馈数据，检验任务的完成情况，如果任务没有完成
//  1 如果是位置信息，查看是否应该发送路径点给AGV，发送的条件如果不是终点，应该使得AGV保存的路径点不低于一定数量，使得AGV可以顺畅行驶
//  2 如果是举升信息，应该再次发送命令
func (this *AGVSoulCore) TryEndTask(data *AgvData) error {
	if data == nil {
		// return NewAgvError(AGV_ERROR_CODE_系统运行异常, ("系统异常，输入数据为nil"))
		return nil
	}
	if this.Task == nil {
		return nil
	}
	if this.Task.TryEnd(data.X, data.Y, data.LiftStatus) == true {
		//发送完成任务的消息给任务中心
		DebugOutput("任务完成："+this.Task.String()+GetFileLocation(), 2)
		this.TaskCommand = nil
		this.Center.ReportTaskEnd(this.Task)
		this.Task = nil
		return nil
	} else {
		this.ExecuteTask()
	}
	return nil
}

//如果是行驶任务，需要计算并保存行驶路径
//如果是举升任务，无需初始化
func (this *AGVSoulCore) InitTask() error {
	if this.IsTaskInitialized() == true {
		return nil
	}
	if this.Task == nil {
		return nil
	}
	DebugOutput("AGVSoulCore 进行任务初始化 "+this.Task.Summarize()+"..."+GetFileLocation(), 3)
	switch this.Task.TaskType {
	case TASK_TYPE_MOVE_TO:
		if path, err := this.Center.TryFindAGVPath(this.ID, this.X, this.Y, this.Task.DestX, this.Task.DestY, this.LiftStatus); err != nil {
			return err
		} else {
			// this.Path = path.SetPathGUID() //计算成功的路径点都有唯一的ID进行标识
			this.Path = path
			this.TaskInitialized = true
			DebugOutput(fmt.Sprintf("AGV %s 任务初始化成功：", this.ID)+this.Path.FormatPath()+GetFileLocation(), 2)
			this.ExecuteTask()
		}
	case TASK_TYPE_LIFT:
		this.TaskInitialized = true
		DebugOutput("任务初始化成功"+GetFileLocation(), 3)
		this.ExecuteTask()
	}
	return nil
}

type AGVSoulCoreList []*AGVSoulCore

func (this AGVSoulCoreList) FindAGVbyID(id string) (*AGVSoulCore, error) {
	for _, agvSoul := range this {
		if agvSoul.ID == id {
			return agvSoul, nil
		}
	}
	// return nil, NewLogicError("不存在该AGV")
	return nil, ErrNoSuchAGV
}
