package agvScheduler

import (
	// "bufio"
	"fmt"
	"github.com/astaxie/beego"
	"net"
	// "strings"
	// "encoding/json"
	"regexp"
	// "runtime"
	"strconv"
	"strings"
	"time"
)

var (
	validMsg                                 = regexp.MustCompile(`\[[\w,]+\]`)
	msgTemp                           string = ""
	Time_Interval_To_Check_Connection int    = 10                //second
	chanWriterToAGV                          = make(chan string) //需要向AGV发送数据时，向此发送数据
)

func connectToAGVServer(connectionInfo ConnectionInfo) {
	if conn, err := startClient(connectionInfo.Port, connectionInfo.IP); err != nil {
		DebugOutput(fmt.Sprintf("连接服务器 %s:%d 失败 : %s", connectionInfo.IP, connectionInfo.Port, err.Error())+GetFileLocation(), 1)
	} else {
		DebugOutput("建立连接成功"+GetFileLocation(), 2)
		connectionInfo.setConn(conn)
		go startAGVListening(&connectionInfo)
		go startConnectionCloseListening(connectionInfo)
		go checkConnection(connectionInfo)
		go startWriteDataToAGVListening(connectionInfo)
	}
}
func checkConnection(connectionInfo ConnectionInfo) {
	ticker := time.Tick(10 * time.Second)
	for now := range ticker {
		DebugOutput("与服务器连接状态监测... "+now.Format("15:04:05")+GetFileLocation(), 4)
		if connectionInfo.Conn == nil {
			connectToAGVServer(connectionInfo)
		}
	}
}
func SetAgvDestination(id, x, y string) {
	DebugOutput(fmt.Sprintf("操作AGV  %s 移动到 (%s, %s) ", id, x, y)+GetFileLocation(), 2)
	ix, errx := strconv.Atoi(x)
	iy, erry := strconv.Atoi(y)
	if errx == nil && erry == nil {
		G_TaskDistributeCenter.AddTaskItem(id, NewTaskItem(id, AGV_LIFT_STATUS_LIFTED, ix, iy, TASK_TYPE_MOVE_TO))
		// taskItem := NewTaskItem(id, AGV_LIFT_STATUS_LIFTED, ix, iy, TASK_TYPE_MOVE_TO)
		// if err := g_agvTaskCenter.AddTaskToAGV(id, taskItem); err == nil {
		// 	DebugOutput(fmt.Sprintf("AGV %s add new Task ok: %s", id, taskItem.Summarize())+GetFileLocation(), 2)
		// 	g_agvTaskCenter.StartUpSpecialAGVTask(id)
		// } else {
		// 	DebugOutput(err.Error()+GetFileLocation(), 1)
		// }
	}
}
func SetAGVLiftStatus(id, status string) {

	G_TaskDistributeCenter.AddTaskItem(id, NewTaskItem(id, status, -1, -3, TASK_TYPE_LIFT))

	// taskItem := NewTaskItem("", status, 0, 0, TASK_TYPE_LIFT)
	// if err := g_agvTaskCenter.AddTaskToAGV(id, taskItem); err == nil {
	// 	DebugOutput(fmt.Sprintf("AGV %s add new Task ok: %s", id, taskItem.Summarize())+GetFileLocation(), 2)
	// 	g_agvTaskCenter.StartUpSpecialAGVTask(id)
	// } else {
	// 	DebugOutput(err.Error()+GetFileLocation(), 1)
	// }
}

func splitMsgToCmd(rawData string) string {
	// beego.Emergency(rawData + GetFileLocation())
	// DebugOutput(rawData, 3)
	rest := ""
	if i := strings.LastIndex(rawData, "]"); i >= 0 {
		msges := rawData[0 : i+1]
		rest = rawData[i+1:]
		// beego.Debug("tail cut: " + msges)
		for _, msg := range validMsg.FindAllString(msges, -1) {
			DebugOutput("命令处理："+msg+GetFileLocation(), 4)
			cmd := strings.Split(msg[1:len(msg)-1], ",")
			parseCMD(cmd)
		}
	}
	return rest
}
func parseCMD(values []string) {
	if len(values) < 6 {
		return
	}
	cmd := values[0]
	id := values[1]
	status := values[2]
	x := values[3]
	y := values[4]
	ix, errx := strconv.Atoi(x)
	iy, erry := strconv.Atoi(y)
	if errx != nil || erry != nil {
		DebugOutput("命令解析出错"+GetFileLocation(), 1)
		return
	}
	liftStatus := values[5]
	agvData := AgvData{
		ID:         id,
		DataType:   cmd,
		Status:     status,
		X:          ix,
		Y:          iy,
		LiftStatus: liftStatus,
	}
	SendAgvDataToPathCenter(&agvData)
	// switch cmd {
	// case ACCEPTED_CMD_TYPE_HEART_BEATING: //心跳 "1"
	// 	DebugOutput(fmt.Sprintf("AGV %s 活跃中 ...", id), 3)
	// 	// x := values[2]
	// 	// y := values[3]
	// 	// status := values[4]
	// 	// liftStatus := values[5]
	// 	// G_agvInfoList.UpdateStatus(id, status)

	// 	ix, errx := strconv.Atoi(x)
	// 	iy, erry := strconv.Atoi(y)
	// 	if errx == nil && erry == nil {
	// 		if err := updatePosition(id, ix, iy); err == nil {
	// 			if pointTemp := G_warehouseMap.GetPoint(ix, iy); pointTemp != nil {
	// 				if pointTemp.IsBlocked() == false {
	// 					if errBlock := pointTemp.Block(id); errBlock != nil { //占据地图上的位置
	// 						DebugOutput(errBlock.Error()+GetFileLocation(), 1)
	// 					}
	// 				}
	// 			} else {
	// 				DebugOutput("地图错误"+GetFileLocation(), 1)
	// 			}
	// 		}
	// 	}

	// 	updateLiftStatus(id, liftStatus)
	// 	g_agvTaskCenter.StartUpSpecialAGVTask(id) //以心跳为驱动力，重新启动AGV的任务执行
	// case ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED: //位置变化
	// 	x := values[2]
	// 	y := values[3]
	// 	DebugOutput(fmt.Sprintf("AGV %s 已经移动到 (%s, %s)", id, x, y)+GetFileLocation(), 2)
	// 	ix, errx := strconv.Atoi(x)
	// 	iy, erry := strconv.Atoi(y)
	// 	if errx == nil && erry == nil {
	// 		updatePosition(id, ix, iy)
	// 	}
	// case ACCEPTED_CMD_TYPE_AGV_STATUS_CHANGED: //agv状态变化
	// 	status := values[2]
	// 	beego.Debug(fmt.Sprintf("AGV(%s) 状态变为 %s", id, status) + GetFileLocation())
	// 	updateStatus(id, status)
	// case ACCEPTED_CMD_TYPE_AGV_LIFT_STATUS_CHANGED: //举升状态变化
	// 	liftStatus := values[2]
	// 	if liftStatus == AGV_LIFT_STATUS_LIFTED {
	// 		DebugOutput(fmt.Sprintf("AGV %s 完成举升", id)+GetFileLocation(), 3)
	// 		updateLiftStatus(id, liftStatus)
	// 	} else if liftStatus == AGV_LIFT_STATUS_UNLIFTED {
	// 		DebugOutput(fmt.Sprintf("AGV %s 降下举升", id)+GetFileLocation(), 3)
	// 		updateLiftStatus(id, liftStatus)
	// 	} else {
	// 		DebugOutput(fmt.Sprintf("该命令无法识别", id)+GetFileLocation(), 0)
	// 	}
	// }
}

func SendAgvDataToPathCenter(data *AgvData) {
	G_PathCaculateCenter.ChIn_AGVdata <- data
	for _, soul := range G_agvSouls {
		if data.ID == soul.ID {
			soul.ChIn_AGVdata <- data
		}
	}
}

//通知AGV举升
func leadAGVLift(id, shiftStatus string) {
	cmd := fmt.Sprintf("[%s,%s,%s]", CMD_SET_AGV_LIFT, id, shiftStatus)
	DebugOutput(fmt.Sprintf("发送AGV举升数据 => %s", cmd)+GetFileLocation(), 3)
	writeData2Connection(cmd)
}

//通知AGV行走的路径
func leadAGVNextStep(id string, x, y int) {
	cmd := fmt.Sprintf("[%s,%s,%d,%d]", CMD_SET_NEXT_STEP, id, x, y)
	DebugOutput(fmt.Sprintf("发送AGV下一步运行数据 => %s", cmd)+GetFileLocation(), 3)
	writeData2Connection(cmd)
}

//更新本地AGV运行状态，并向外广播
func updateStatus(id, status string) error {
	// agvData := AgvData{
	// 	ID:       id,
	// 	DataType: ACCEPTED_CMD_TYPE_AGV_STATUS_CHANGED,
	// 	Status:   status,
	// }
	// if agvTaskInfo, err := g_agvTaskCenter.AcceptData(&agvData); err == nil {
	// 	G_chanBroadcastEvent <- NewEventMsg(EVENT_STATUS_NEW, agvTaskInfo.AGV.CloneSimple())
	// 	return nil
	// } else {
	// 	if err.(*AgvError).ErrorCode() != AGV_ERROR_CODE_逻辑异常 {
	// 		DebugOutput(fmt.Sprintf("agv %s error: %s", id, err.Error())+GetFileLocation(), 3)
	// 		return err
	// 	}
	// }
	return nil
}

//更新本地AGV举升状态，并向外广播
func updateLiftStatus(id, liftStatus string) error {
	// agvData := AgvData{
	// 	ID:         id,
	// 	DataType:   ACCEPTED_CMD_TYPE_AGV_LIFT_STATUS_CHANGED,
	// 	LiftStatus: liftStatus,
	// }
	// if agvTaskInfo, err := g_agvTaskCenter.AcceptData(&agvData); err == nil {
	// 	return nil
	// } else {
	// 	if err.(*AgvError).ErrorCode() != AGV_ERROR_CODE_逻辑异常 {
	// 		DebugOutput(fmt.Sprintf("agv %s error: %s", id, err.Error())+GetFileLocation(), 3)
	// 		return err
	// 	}
	// }
	return nil
}

//更新本地AGV位置，并向外广播
func updatePosition(id string, ix, iy int) error {
	// agvData := AgvData{
	// 	ID:       id,
	// 	DataType: ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED, //move
	// 	X:        ix,
	// 	Y:        iy,
	// }
	// if agvTaskInfo, err := g_agvTaskCenter.AcceptData(&agvData); err == nil {
	// 	G_chanBroadcastEvent <- NewEventMsg(EVENT_POSITION_NEW, agvTaskInfo.AGV.CloneSimple())
	// 	return nil
	// } else {
	// 	if err.(*AgvError).ErrorCode() != AGV_ERROR_CODE_逻辑异常 {
	// 		DebugOutput(fmt.Sprintf("agv %s error: %s", id, err.Error())+GetFileLocation(), 3)
	// 		return err
	// 	}
	// }
	return nil
}
func writeData2Connection(data string) {
	chanWriterToAGV <- data
}

//将信息发送到服务端AGV
func startWriteDataToAGVListening(connectionInfo ConnectionInfo) {
	for {
		data := <-chanWriterToAGV
		if connectionInfo.Conn != nil {
			wrote, err := (*(connectionInfo.Conn)).Write([]byte(data))
			checkError(err, "Write: wrote "+string(wrote)+" bytes.")
		} else {
			//放到缓存区里，等待连接成功后发送
		}
	}
}

//监测关闭的连接
func startConnectionCloseListening(connectionInfo ConnectionInfo) {
	for {
		select {
		case connInfo := <-g_chanStartConnCloseListening:
			beego.Warn((connInfo).toString() + " closed" + GetFileLocation())
			connectionInfo.setConn(nil)
		}

	}
}

func startClient(serverPort int, host string) (*(net.Conn), error) {
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", host, serverPort))
	if err != nil {
		DebugOutput("dial error: "+err.Error()+GetFileLocation(), 1)
		return nil, err
	}
	return &conn, nil
}
func startAGVListening(connInfo *ConnectionInfo) {
	DebugOutput("开始监听AGV服务器发送的AGV信息 => "+connInfo.toString()+GetFileLocation(), 2)
	for {
		if connInfo.Conn == nil {
			DebugOutput("连接为空"+GetFileLocation(), 1)
			return
		}
		var ibuf []byte = make([]byte, maxRead)
		if length, err := (*(connInfo.Conn)).Read(ibuf); err == nil {
			handleMsg(length, err, ibuf)
		} else {
			goto DISCONNECT
		}
	}
DISCONNECT:
	errConn := (*(connInfo.Conn)).Close()
	g_chanStartConnCloseListening <- connInfo
	DebugOutput("连接关闭"+connInfo.toString()+GetFileLocation(), 2)
	checkError(errConn, "Close: ")
}
func handleMsg(length int, err error, msg []byte) {
	if length > 0 {
		msgTemp = splitMsgToCmd(msgTemp + string(msg))
	} else {
		beego.Warn("message length == 0" + GetFileLocation())
	}
}
func checkError(error error, info string) {
	if error != nil {
		panic("ERROR: " + info + " " + error.Error()) // terminate
	}
}
