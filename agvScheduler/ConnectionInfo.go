package agvScheduler

import (
	// "bufio"
	"fmt"
	// "github.com/astaxie/beego"
	"net"
	// "strings"
	// "regexp"
	// "strings"
	// "time"
)

type ConnectionInfo struct {
	IP   string
	Port int
	Conn *(net.Conn)
}

func (this ConnectionInfo) toString() string {
	return fmt.Sprintf("ConnectionInfo: %s:%d", this.IP, this.Port)
}
func (this *ConnectionInfo) setConn(conn *(net.Conn)) {
	this.Conn = conn
}

// type ConnectionInfoList []ConnectionInfo

// func (this ConnectionInfoList) setConnectionClose(ip string, port int) {
// 	for i, connectionInfo := range this {
// 		if connectionInfo.IP == ip && connectionInfo.Port == port {
// 			(&(this[i])).setConn(nil)
// 		}
// 	}
// }
// func (this *ConnectionInfoList) checkConnectionStatus() {
// 	for i, connectionInfo := range *this {
// 		if connectionInfo.Conn == nil {
// 			if conn, err := startClient(connectionInfo.Port, connectionInfo.IP); err != nil {
// 				DebugOutput(fmt.Sprintf("create client to %s:%d failed : %s", connectionInfo.IP, connectionInfo.Port, err.Error())+GetFileLocation(), 1)
// 			} else {
// 				DebugOutput("create client ok"+GetFileLocation(), 2)
// 				(&((*this)[i])).setConn(conn)
// 				go startClientListening(&((*this)[i]))
// 			}
// 		}
// 	}
// }
