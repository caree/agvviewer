package agvScheduler

import (
// "fmt"
// "time"
// "errors"
)

// const(
// 	const_ShelfTransportTaskTypeTo_拣选口 int = 1
// 	const_ShelfTransportTaskTypeTo_货架原位 int = 2
// }
type IShelfTransportTaskDestination interface {
	GetDestPoint() (int, int, error)
}

func NewShelfTransportTask(shelf *ShelfInfo, agvTaskInfo *AGVTaskInfo, dest IShelfTransportTaskDestination) *ShelfTransportTask {
	if dest == nil {
		return nil
	}
	return &ShelfTransportTask{
		Shelf:       shelf,
		Destination: dest,
		AGVTaskInfo: agvTaskInfo,
	}
}

type ShelfTransportTask struct {
	Shelf       *ShelfInfo
	AGVTaskInfo *AGVTaskInfo
	Destination IShelfTransportTaskDestination
	// ShelfList   ShelfList
	// AGVTaskList AGVTaskList
}

func (this *ShelfTransportTask) TranslateToAGVTasks() (TaskList, error) {
	if x, y, err := this.Destination.GetDestPoint(); err != nil {
		return nil, err
	} else {
		if this.AGVTaskInfo == nil { //货架没有在AGV上
			taskList := TaskList{
				NewTaskItem("", AGV_LIFT_STATUS_UNLIFTED, 0, 0, TASK_TYPE_LIFT),                          //如果AGV举升的话，将降下来
				NewTaskItem("", AGV_LIFT_STATUS_UNLIFTED, this.Shelf.X, this.Shelf.Y, TASK_TYPE_MOVE_TO), //AGV移动到货架底下
				NewTaskItem("", AGV_LIFT_STATUS_LIFTED, 0, 0, TASK_TYPE_LIFT),                            //AGV举升
				NewTaskItem("", AGV_LIFT_STATUS_UNLIFTED, x, y, TASK_TYPE_MOVE_TO),                       //AGV移动到目标地点
			}
			return taskList, nil
		} else { //货架在某台AGV上，直接生成任务列表，将其赋给该AGV
			taskList := TaskList{
				// NewTaskItem(AGV_LIFT_STATUS_UNLIFTED, 0, 0, TASK_TYPE_LIFT),                          //如果AGV举升的话，将降下来
				// NewTaskItem(AGV_LIFT_STATUS_UNLIFTED, this.Shelf.X, this.Shelf.Y, TASK_TYPE_MOVE_TO), //AGV移动到货架底下
				// NewTaskItem(AGV_LIFT_STATUS_LIFTED, 0, 0, TASK_TYPE_LIFT),                            //AGV举升
				NewTaskItem("", AGV_LIFT_STATUS_UNLIFTED, x, y, TASK_TYPE_MOVE_TO), //AGV移动到目标地点
			}
			return taskList, nil
		}
	}

	//先找到（应该锁定）AGV
	// if agvTaskTemp, err := this.AGVTaskList.GetIdleTaskInfo(); err != nil {
	// 	return nil, err
	// } else { //再找到货架的坐标
	// 	if shelfTemp, err := this.ShelfList.FindShelfByID(this.ShelfID); err != nil {
	// 		return nil, err
	// 	} else {
	// 		//找到目的地坐标
	// 		if x, y, err := this.Destination.GetDestPoint(); err != nil {
	// 			return nil, err
	// 		} else {
	// 			taskList := TaskList{
	// 				NewTaskItem(AGV_LIFT_STATUS_UNLIFTED, 0, 0, TASK_TYPE_LIFT),                        //如果AGV举升的话，将降下来
	// 				NewTaskItem(AGV_LIFT_STATUS_UNLIFTED, shelfTemp.X, shelfTemp.Y, TASK_TYPE_MOVE_TO), //AGV移动到货架底下
	// 				NewTaskItem(AGV_LIFT_STATUS_LIFTED, 0, 0, TASK_TYPE_LIFT),                          //AGV举升
	// 				NewTaskItem(AGV_LIFT_STATUS_UNLIFTED, x, y, TASK_TYPE_MOVE_TO),                     //AGV移动到目标地点
	// 			}
	// 			return agvTaskTemp, this.AGVTaskList.AddTasksToAGV(agvTaskTemp.ID, taskList)
	// 		}
	// 	}
	// }
	// return nil, NewAgvError(AGV_ERROR_CODE_数据异常, "数据异常")
}

//==============================================================================================

func NewTransportToOriginalPoint(shelf *ShelfInfo) *TransportToOriginalPoint {
	return &TransportToOriginalPoint{
		Shelf: shelf,
		// ID:        id,
		// ShelfList: shelfList,
	}
}

//搬运货架到原来的位置
type TransportToOriginalPoint struct {
	// ID        string //货架ID
	// ShelfList ShelfList
	Shelf *ShelfInfo
}

func (this *TransportToOriginalPoint) GetDestPoint() (int, int, error) {
	return this.Shelf.OriginalX, this.Shelf.OriginalY, nil
	// for _, shelfTemp := range this.ShelfList {
	// 	if shelfTemp.ID == this.ID {
	// 		return shelfTemp.X, shelfTemp.Y, nil
	// 	}
	// }
	// return -1, -1, NewAgvError(AGV_ERROR_CODE_没有符合条件的数据, "没有指定的货架")
}

//----------------------------------------------------------------------------------------------------

func NewTransportDestinationToPickUpPoint(pickUpPoint *Point) *TransportDestinationToPickUpPoint {
	return &TransportDestinationToPickUpPoint{
		// ID:              id,
		// PickUpPointList: pickUpPointList,
		PickUpPoint: pickUpPoint,
	}
}

//搬运货架到拣选口
type TransportDestinationToPickUpPoint struct {
	// ID              string //拣选口名称
	// PickUpPointList PointPointerList
	PickUpPoint *Point
}

func (this *TransportDestinationToPickUpPoint) GetDestPoint() (int, int, error) {
	return this.PickUpPoint.X, this.PickUpPoint.Y, nil
	// if this.PickUpPointList != nil {
	// 	if pointTemp, err := this.PickUpPointList.FindPointByTypeAndName(PickupPlace, this.ID); err == nil {
	// 		return pointTemp.X, pointTemp.Y, nil
	// 	}
	// } else {
	// 	return -1, -1, NewAgvError(0, "没有拣选口列表可供查询")
	// }
	// return -1, -1, NewAgvError(AGV_ERROR_CODE_没有符合条件的数据, "没有符合条件的点")
}
