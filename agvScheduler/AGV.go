package agvScheduler

import (
	// "bufio"
	"fmt"
	// "github.com/astaxie/beego"
	// "net"
	// "strings"
	// "encoding/json"
	// "regexp"
	// "strings"
	// "time"
	// "errors"
)

//agv各种状态的定义
type AGV_Status string

func (this AGV_Status) string() string {
	switch this {
	case "0":
		return "未连接"
	case "1":
		return "运行中"
	case "2":
		return "暂停中"
	case "3":
		return "已停止"
	}
	return ""
}

// type Shelf
type AGV struct {
	ID           string
	Status       AGV_Status // AVG状态
	LiftStatus   string     //举升状态
	X            int
	Y            int
	Shelf        *ShelfInfo
	WarehouseMap PointPointerList `json:"-"`
	Shelves      ShelfList        `json:"-"`
}

func (this *AGV) CloneSimple() *AGV {
	agv := AGV{
		ID:         this.ID,
		Status:     this.Status,
		X:          this.X,
		Y:          this.Y,
		LiftStatus: this.LiftStatus,
		Shelf:      this.Shelf,
	}
	return &agv
}
func (this *AGV) ToString() string {
	hasShelf := "无"
	if this.Shelf != nil {
		hasShelf = "有"
	}
	return fmt.Sprintf("agv ID: %s  Status: %s  X: %d  Y: %d  LiftStatus: %s 货架：%s", this.ID, this.Status, this.X, this.Y, this.LiftStatus, hasShelf)
}
func (this *AGV) UpdateLiftStatus(status string) error {
	if pointTemp := this.WarehouseMap.GetPoint(this.X, this.Y); pointTemp != nil {

		if this.LiftStatus != status {
			if status == AGV_LIFT_STATUS_UNLIFTED {
				if this.Shelf != nil {
					DebugOutput(fmt.Sprintf("AGV(%s) 放下了货架(%s)", this.ID, this.Shelf.String())+GetFileLocation(), 3)
					// pointTemp.PlacedShelf(this.Shelf)
					// this.Shelf.UpdatePosition(this.X, this.Y)
					this.Shelf = nil
				}
				DebugOutput("AGV举升状态变化: 降下 "+this.ToString()+GetFileLocation(), 3)
			} else if status == AGV_LIFT_STATUS_LIFTED {
				//如果AGV当前所在位置有货架，将其举起来
				if shelfTemp := this.Shelves.ExistShelf(this.X, this.Y); shelfTemp != nil {
					this.Shelf = shelfTemp
					// if errPlaceShelf := pointTemp.PlacedShelf(nil); errPlaceShelf != nil {
					// 	return errPlaceShelf
					// }
					DebugOutput(fmt.Sprintf("AGV(%s) 举升了货架(%s)", this.ID, this.Shelf.String())+GetFileLocation(), 3)
				} else {
					// return NewAgvError(AGV_ERROR_CODE_数据异常, "该点没有货架")
					DebugOutput(fmt.Sprintf("AGV举升，但该点(%d, %d)没有货架", this.X, this.Y), 2)
				}
				DebugOutput("AGV举升状态变化: 上升 "+this.ToString()+GetFileLocation(), 3)
			}
			this.LiftStatus = status

			return nil
		}
		// return NewAgvError(AGV_ERROR_CODE_逻辑异常, ("新举升状态与AGV当前举升状态相同"))
		return ErrAGVAlreadyGetLiftStatus
		// pointTemp.MapBlockLinked.BlockTypeTemp = ShelfPlace //AGV离开后，这里会被标记为货架
	} else {
		// return NewAgvError(AGV_ERROR_CODE_逻辑异常, ("地图上无法找到该点"))
		return ErrNoPointOnMap
	}

}
func (this *AGV) UpdateStatus(status string) error {
	if this.Status != AGV_Status(status) {
		this.Status = AGV_Status(status)
		DebugOutput(fmt.Sprintf("agv (%s) 状态变为 %s", this.ID, this.Status.string())+GetFileLocation(), 3)
		return nil
	}
	// return NewAgvError(AGV_ERROR_CODE_逻辑异常, ("AGV状态与要设定的相同"))
	return ErrAGVAlreadyGetStatus
}

//返回true说明查找路径成功，如果 PointPointerList 为 nil，说明起始点和目标点相同，否则就是路径点
//返回false说明查找路径失败
//如果 error!=nil ，说明有错误
func (this *AGV) TryFindAGVPath(destX, destY int) (PointPointerList, error) {
	if this.X == 0 || this.Y == 0 || destX == 0 || destY == 0 {
		// return nil, NewAgvError(AGV_ERROR_CODE_数据异常, ("起始点或者目标点不符合查找规则"))
		return nil, ErrPointCannotSearched
		// return false, nil, ("起始点或者目标点不符合查找规则")
	}

	if this.X == destX && this.Y == destY {
		return nil, nil //找到路径，但是没有路径点，已经在目标点上
	}

	//将地图与agv关联，计算路径点作为agv的方法
	this.WarehouseMap.ClearFindingFootPrint()
	this.WarehouseMap.UnblockPathPoint(this.ID)
	pointSrc := this.WarehouseMap.GetPoint(this.X, this.Y)
	pointSrc.SetStartFlag()
	pointDest := this.WarehouseMap.GetPoint(destX, destY)
	if pointDest.IsBlocked() {
		DebugOutput("目标点被占用"+GetFileLocation(), 2)
		// return nil, NewAgvError(AGV_ERROR_CODE_逻辑异常, ("目标点被占用"))
		return nil, ErrPointOccupied
	}
	if pointDest.IsBanned() {
		DebugOutput("目标点不可行驶"+GetFileLocation(), 2)
		// return nil, NewAgvError(AGV_ERROR_CODE_逻辑异常, ("目标点不可行驶"))
		return nil, ErrPointBanned
	}
	if pointDest.CanBeEntered(this.WarehouseMap) == false {
		DebugOutput("目标点周围没有点可以进入"+GetFileLocation(), 2)
		// return nil, NewAgvError(AGV_ERROR_CODE_逻辑异常, ("目标点周围没有点可以进入"))
		return nil, ErrPointCannotEntered
	}
	DebugOutput(fmt.Sprintf("id (%s) 查找路径 从 %s 到 %s", this.ID, pointSrc.ToString(), pointDest.ToString()), 3)
	var predictor PointPredictor
	if this.LiftStatus == AGV_LIFT_STATUS_UNLIFTED {
		predictor = func(point *Point) bool {
			return point.IsBanned() == false &&
				// point.MapBlockLinked.BlockType != RailwayBlocked &&
				point.IsBlocked() == false
			// point.IsOccupiedByAGV() == false
			// point.AgvLinked == nil
		}
	} else {
		predictor = func(point *Point) bool {
			return point.IsBanned() == false &&
				// return point.MapBlockLinked.BlockType != BannedArea &&
				point.IsBlocked() == false
			// point.MapBlockLinked.BlockType != RailwayBlocked &&
			// point.IsShelfPlaced() == false &&
			// point.IsOccupiedByAGV() == false
			// return point.MapBlockLinked.BlockType != BannedArea && point.MapBlockLinked.BlockType != RailwayBlocked && point.MapBlockLinked.BlockType != ShelfPlace
		}
	}

	if list, err := pointSrc.FindPathPoints(this.WarehouseMap, pointDest, predictor); err != nil {
		return nil, err
	} else { //if b == true
		if list != nil {
			str1 := ""
			for _, pointTemp := range list {
				str1 += fmt.Sprintf(" <= [(%d,%d),%d]", pointTemp.X, pointTemp.Y, pointTemp.MapBlockLinked.BlockType)
			}
			DebugOutput("查找到的路径："+str1+GetFileLocation(), 3)

			listPointNextStepsBlocked := list
			if length := len(list); length >= MAX_STEP_COUNT {
				listPointNextStepsBlocked = list[length-MAX_STEP_COUNT:] //最后一个是出发点
			}
			listPointNextStepsBlocked.BlockPathPoint(this.ID)
			str2 := ""
			for _, pointTemp := range listPointNextStepsBlocked {
				str2 += pointTemp.ToString() + " "
			}
			DebugOutput(fmt.Sprintf("被占用的点：%s", str2)+GetFileLocation(), 3)
			return listPointNextStepsBlocked, nil
		} else {
			DebugOutput("已经在目标点上"+GetFileLocation(), 3)
			return nil, nil
		}
	}
	// return nil, NewAgvError(AGV_ERROR_CODE_逻辑异常, ("无法查找到路径"))
	return nil, ErrNoPathFound
}

func (this *AGV) UpdatePosition(x, y int) error {
	if this.X != x || this.Y != y {
		if pointCurrent := this.WarehouseMap.GetPoint(this.X, this.Y); pointCurrent != nil {
			// pointCurrent.AgvLinked = nil
			if err := pointCurrent.UnBlock(this.ID); err != nil { //之前可能占用了该点
				return err
			}
		}

		this.X = x
		this.Y = y
		if this.Shelf != nil {
			this.Shelf.UpdatePosition(this.X, this.Y)
		}
		//将AGV与新的点绑定
		if pointNew := this.WarehouseMap.GetPoint(this.X, this.Y); pointNew != nil {
			// pointNew.AgvLinked = this
		}
		DebugOutput("位置变化: "+this.ToString()+GetFileLocation(), 3)
		return nil
	} else {
		// return NewLogicError("新位置与当前位置相同")
		return ErrAGVAlreadyOnPosition
	}
	// return NewAgvError(AGV_ERROR_CODE_与目标数据相同, ("新位置与当前位置相同"))
}
func (this *AGV) InitAGVonMap() error {
	if pointCurrent := this.WarehouseMap.GetPoint(this.X, this.Y); pointCurrent != nil {
		// pointCurrent.AgvLinked = this
		return nil
	} else {
		// return NewAgvError(AGV_ERROR_CODE_逻辑异常, ("地图坐标错误"))
		return ErrNoPointOnMap
	}
}
func NewAGV(id string, x, y int, warehouseMap PointPointerList, shelves ShelfList) *AGV {
	return &AGV{
		ID:           id,
		Status:       AGV_STATUS_UNCONNECTED,
		LiftStatus:   AGV_LIFT_STATUS_UNLIFTED,
		X:            x,
		Y:            y,
		WarehouseMap: warehouseMap,
		Shelves:      shelves,
	}
}

// ↑↓← → ↘↙↔ ↕
