package agvScheduler

import (
	"fmt"
	"strings"
	"time"
)

var (
	guid int64 = 0

	G_rowCount int = DEFAULT_ROWCOUNT
	G_colCount int = DEFAULT_COLCOUNT
)

func GetGUID() int64 {
	guid++
	return guid
}

var (
	// g_connectionInfo              ConnectionInfo
	g_chanStartConnCloseListening chan *ConnectionInfo     = make(chan *ConnectionInfo)
	G_shelves                     ShelfList                                             //= ShelfList{}
	G_WarehouseConfigPointList    WarehouseConfigPointList = WarehouseConfigPointList{} //从数据库中获得的仓库配置数据
	G_warehouseConfig             WarehouseConfig          = WarehouseConfig{
		ColCount: G_rowCount,
		RowCount: G_colCount,
		Points:   G_WarehouseConfigPointList,
		// Points:   G_configPointsList,
	}
	G_ProductInfoList ProductInfoList = ProductInfoList{}
	G_agvInfoList     AGVInfoList

	G_chanBroadcastEvent chan *EventMsg = make(chan *EventMsg)

	G_PathCaculateCenter   *PathCaculateCenter
	G_agvSouls             AGVSoulList = AGVSoulList{}
	G_TaskDistributeCenter *TaskDistributeCenter
	//************************************************************************************************
)

func init() {
	guid = time.Now().Unix()

	// return
	G_ProductInfoList = ProductInfoList{
		ProductInfo{
			ID:   "001",
			Name: "读写器A",
			Note: "RFID设备",
		},

		ProductInfo{
			ID:   "002",
			Name: "读写器B",
			Note: "RFID设备",
		},
	}
	G_agvInfoList = AGVInfoList{
		&AGVInfo{
			ID:          "01",
			OverLoad:    1000,
			SelfWeight:  890,
			Size:        "100*200*50cm",
			DesignSpeed: "20km/h",
		},

		&AGVInfo{
			ID:          "02",
			OverLoad:    1000,
			SelfWeight:  890,
			Size:        "100*200*50cm",
			DesignSpeed: "20km/h",
		},
	}
	/*
		g_shelves = ShelfList{
			NewShelf(3, 1),
			NewShelf(3, 4),
		}
		G_configPointsList = PointPointerList{
		NewPoint(2, 2, NewMapBlock("", BannedArea)),
		NewPoint(2, 3, NewMapBlock("", BannedArea)),
		NewPoint(2, 4, NewMapBlock("", BannedArea)),
		NewPoint(5, 5, NewMapBlock("拣选口2", PickupPlace)),
		NewPoint(6, 5, NewMapBlock("拣选口1", PickupPlace)),
		}
		configPoint1 := NewWarehouseConfigPoint(2, 2, BannedArea, "", Orientation_None)
		configPoint2 := NewWarehouseConfigPoint(2, 3, PickupPlace, "拣选口2", Orientation_None)
		configPoint3 := NewWarehouseConfigPoint(2, 4, RechargingArea, "充电口2", Orientation_None)
		g_WarehouseConfigPointList = append(g_WarehouseConfigPointList, configPoint1)
		g_WarehouseConfigPointList = append(g_WarehouseConfigPointList, configPoint2)
		g_WarehouseConfigPointList = append(g_WarehouseConfigPointList, configPoint3)
	*/
	G_TaskDistributeCenter = NewTaskDistributeCenter()

	var err error

	if G_rowCount, G_colCount, err = initSysConfig(); err != nil {
		DebugOutput(err.Error()+GetFileLocation(), 1)
		return
	} else {
		DebugOutput(fmt.Sprintf("默认行数 列数 分别设置为 %d %d", G_rowCount, G_colCount)+GetFileLocation(), 3)
	}
	if G_WarehouseConfigPointList, err = initWarehouseConfigPointsFromDB(); err != nil {
		DebugOutput(err.Error()+GetFileLocation(), 1)
	} else {
		DebugOutput(fmt.Sprintf("获取仓库配置点 %d 个：", len(G_WarehouseConfigPointList))+GetFileLocation(), 2)
	}
	if G_shelves, err = initShelfListFromDB(); err != nil {
		DebugOutput(err.Error()+GetFileLocation(), 1)
		return
	} else {
		DebugOutput(fmt.Sprintf("获取到货架信息 %d 个：", len(G_shelves))+GetFileLocation(), 2)
		if len(G_shelves) > 0 {
			DebugOutputStrings(G_shelves.String(), 4)
		}
	}

	warehouseMap := InitWareHouse(G_rowCount, G_colCount, G_WarehouseConfigPointList)
	agvSouls := AGVSoulList{}

	funcReceiveTaskCommand := func(agvID string, task *TaskCommand) {
		str := task.ToCommandString(agvID)
		DebugOutput(fmt.Sprintf("向模拟器 %s 输出命令：%s", agvID, task.String())+GetFileLocation(), 2)
		writeData2Connection(str)
	}

	for _, agvInfo := range G_agvInfoList {
		agvSouls = append(agvSouls, NewAGVSoul(agvInfo.ID, funcReceiveTaskCommand))
	}

	agvCores := agvSouls.ToAGVSoulCore()
	for _, core := range agvCores {
		G_agvInfoList.SetAGV(core)
	}
	G_PathCaculateCenter = NewPathCaculateCenter(warehouseMap, agvCores, G_shelves)

	G_PathCaculateCenter.SetTaskStatusReceiver(G_TaskDistributeCenter.ChIn_TaskEnd)
	G_TaskDistributeCenter.SetTaskReceiver(G_PathCaculateCenter.ChIn_TaskReceiver)

	agvSouls.StartRunning()
	G_PathCaculateCenter.StartRunning()
	G_TaskDistributeCenter.StartRunning()

	// return
	// 连接AGV
	connectToAGVServer(ConnectionInfo{
		IP:   "127.0.0.1",
		Port: 9001,
	})

}
func InitWareHouse(row, col int, pointsConfigs WarehouseConfigPointList) PointPointerList {
	temp := PointPointerList{}
	for y := 1; y <= row; y++ {
		for x := 1; x <= col; x++ {
			// flag := pointsConfigs.GetConfigFlag(x, y)
			if config := pointsConfigs.GetConfig(x, y); config == nil {
				// DebugOutput(err.Error()+getFileLocation(), 3)
				temp = append(temp, NewPoint(x, y, NewMapBlock("", Railway), []string{}))
				// continue
			} else {
				if len(config.Orientation) > 0 {
					temp = append(temp,
						NewPoint(x, y,
							NewMapBlock(config.Placer, WarehousePointType(config.BlockType)), strings.Split(config.Orientation, ",")))
				} else {
					temp = append(temp,
						NewPoint(x, y,
							NewMapBlock(config.Placer, WarehousePointType(config.BlockType)), []string{}))
				}
			}
		}
	}
	return temp
}
func printWarehouseMap(row, col int, warehouseMap PointPointerList) {
	mapstringList := map[int]string{}
	for i := 1; i <= row; i++ {
		mapstringList[i] = ""
	}
	for _, pointTemp := range warehouseMap {
		str := "nil"
		if pointTemp.IsBlocked() == true {
			str = "Block"
		}

		mapstringList[pointTemp.Y] = mapstringList[pointTemp.Y] + fmt.Sprintf("  [(%2d,%2d),%5s %5s]", pointTemp.X, pointTemp.Y, pointTemp.Orientations, str)
		// fmt.Println(fmt.Sprintf("[(%d,%d),%d]", pointTemp.X, pointTemp.Y, pointTemp.Flag))
	}
	for i := 1; i <= row; i++ {
		fmt.Println(mapstringList[i])
	}
}

// func printWarehouseMap(row, col int, warehouseMap PointPointerList) {
// 	if col > 10 {
// 		col = 10
// 	}
// 	mapstringList := map[int]string{}
// 	for i := 1; i <= row; i++ {
// 		mapstringList[i] = ""
// 	}
// 	for _, pointTemp := range warehouseMap {
// 		if pointTemp.X <= col {

// 			mapstringList[pointTemp.Y] = mapstringList[pointTemp.Y] + fmt.Sprintf("  [(%d,%d),%d]",
// 				pointTemp.X, pointTemp.Y, pointTemp.MapBlockLinked.BlockType)
// 		}
// 	}
// 	for i := 1; i <= row; i++ {
// 		fmt.Println(mapstringList[i])
// 	}
// }
