package agvScheduler

import (
	"errors"
	// "fmt"
)

var (
	// ErrLogic                   = errors.New("逻辑异常")
	// ErrSystem                  = errors.New("系统异常")
	ErrData                    = errors.New("数据异常")
	ErrTaskDuplicated          = errors.New("任务重复设置")
	ErrTaskNoAGVID             = errors.New("未指定执行任务的AGV")
	ErrAGVAlreadyGetLiftStatus = errors.New("新举升状态与AGV当前举升状态相同")
	ErrAGVAlreadyGetStatus     = errors.New("AGV状态与要设定的相同")
	ErrAGVAlreadyOnPosition    = errors.New("新位置与当前位置相同")
	ErrNoPointOnMap            = errors.New("地图上无法找到该点")
	ErrPointBlockedByOtherAGV  = errors.New("该点被其它ID锁定，不能解锁")
	ErrPointCannotSearched     = errors.New("起始点或者目标点不符合查找规则")
	ErrPointOccupied           = errors.New("目标点被占用")
	ErrPointBanned             = errors.New("目标点设计为不可行驶区域")
	ErrPointCannotEntered      = errors.New("目标点没有方向可以进入")
	ErrNoPathFound             = errors.New("无法查找到路径")
	ErrNoSuchAGV               = errors.New("不存在该AGV")
	ErrNoSuchShelf             = errors.New("没有找到该货架")
	ErrNoIdleAGV               = errors.New("没有空闲的AGV")
	ErrDbInitFailed            = errors.New("数据库初始化失败")
)

/*
type AgvError struct {
	Code int
	Err  error
}

func (this AgvError) Error() string {
	return fmt.Sprintf("%s code:%d", this.Err.Error(), this.Code)

}

func (this AgvError) IsLogicError() bool {
	return this.Code == AGV_ERROR_CODE_逻辑异常
}

func NewAgvError(code int, err string) error {
	return &AgvError{
		Code: code,
		Err:  errors.New(err),
	}
}
func NewLogicError(msg string) error {
	return NewAgvError(AGV_ERROR_CODE_逻辑异常, msg)
}

func (this *AgvError) ErrorCode() int {
	return this.Code
}
*/
const (
	AGV_ERROR_CODE_数据异常   int = 4
	AGV_ERROR_CODE_系统运行异常 int = 9  //超出逻辑是非范围，需要从相对上层解决该问题
	AGV_ERROR_CODE_逻辑异常   int = 10 //正常逻辑上可能的两个结果
	// AGV_ERROR_CODE_与目标数据相同    int = 5
	// AGV_ERROR_CODE_目标点锁定异常    int = 6
	// AGV_ERROR_CODE_没有符合条件的数据  int = 7
	// AGV_ERROR_CODE_货架没有在AGV上  int = 8
	// AGV_ERROR_CODE_没有找到路径     int = 1
	// AGV_ERROR_CODE_目的地被占用     int = 2
	// AGV_ERROR_CODE_任务执行没有输出接口 int = 3
)
