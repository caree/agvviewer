package agvScheduler

import (
	// "agvViewer/
	// _ "agvViewer/routers"
	// "path/filepath"
	// "runtime"
	"fmt"
	"testing"
	"time"
	// "github.com/astaxie/beego"
	// . "github.com/smartystreets/goconvey/convey"
)

var (
	agvTaskCenter AGVTaskList = AGVTaskList{}
)

func init() {
	// fmt.Println("TestPathFinding init ...")
}

/*
//使用指针改变数组内容
func TestArrayPointer(t *testing.T) {
	agvList := AGVList{
		NewAGV("1", 1, 1, nil, nil),
		NewAGV("2", 2, 2, nil, nil),
	}
	for _, agvTemp := range agvList {
		fmt.Println(agvTemp.ToString())
	}
	fmt.Println()
	changeList(&agvList)
	for _, agvTemp := range agvList {
		fmt.Println(agvTemp.ToString())
	}
}
func changeList(list *AGVList) {
	*list = append(*list, NewAGV("3", 3, 2, nil, nil))
}
*/
func TestAGVPathFinding(t *testing.T) {

	// tryTask001(t)
	// tryTask001_1(t)
	// tryTask002(t)
	// tryTask002_1(t)
	// tryTask003(t)
	// tryTask004(t)
	// tryTask005(t)
	// tryTask006(t)
	tryTask007(t)
}
func tryTask007(t *testing.T) {
	fmt.Println("测试 007")
	fmt.Println("### 测试两个AGV行驶单行道 ...")
	row, col := 6, 6
	configPoints := WarehouseConfigPointList{
		NewWarehouseConfigPoint(1, 1, Railway, "", Orientation_Right),
		NewWarehouseConfigPoint(2, 1, Railway, "", Orientation_Right),
		NewWarehouseConfigPoint(3, 1, Railway, "", Orientation_Right),
	}
	warehouseMap := InitWareHouse(row, col, configPoints)
	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))
	printWarehouseMap(row, col, warehouseMap)
	taskGroup := NewAGVTaskGroup("01")
	taskGroup.AddTask(NewTaskItem("01", AGV_LIFT_STATUS_LIFTED, 1, 2, TASK_TYPE_MOVE_TO))

	TaskDistributeCenter := NewTaskDistributeCenter()
	TaskDistributeCenter.AddTaskGroup(taskGroup)

	simulators := AGVSimulatorCoreList{}
	agvSouls := AGVSoulList{}
	agvID01 := "01"

	simulators = append(simulators, NewAGVSimulatorCore(agvID01, 1, 1))
	simulators.StartRunning(1 * time.Second)

	var pathCaculateCenter *PathCaculateCenter

	funcReceiveAgvData := func(data *AgvData) {
		pathCaculateCenter.ChIn_AGVdata <- data
		for _, soul := range agvSouls {
			if data.ID == soul.ID {
				soul.ChIn_AGVdata <- data
			}
		}
	}
	simulators.RegisteAGVStatusDataReceiver(funcReceiveAgvData)

	agvSouls = append(agvSouls, NewAGVSoul(agvID01, simulators.ReceiveTaskCommand))

	agvCores := agvSouls.ToAGVSoulCore()

	pathCaculateCenter = NewPathCaculateCenter(warehouseMap, agvCores, nil)

	pathCaculateCenter.SetTaskStatusReceiver(TaskDistributeCenter.ChIn_TaskEnd)
	TaskDistributeCenter.SetTaskReceiver(pathCaculateCenter.ChIn_TaskReceiver)

	agvSouls.StartRunning()
	pathCaculateCenter.StartRunning()
	TaskDistributeCenter.StartRunning()

	time.Sleep(10 * time.Second)

	fmt.Println("### 007 测试完成 ...")
}

func tryTask005(t *testing.T) {
	//只有一个货架，AGV执行相应动作，货架位置相应变化
	fmt.Println("测试 005")
	fmt.Println("### 测试agv运行到货架底，将货架举升，然后将货架运送到指定地点，地图上对货架的标识也相应改变 ...")

	row, col := 6, 6
	configPoints := WarehouseConfigPointList{}
	shelves := ShelfList{
		NewShelf("1", 2, 2),
		NewShelf("2", 2, 3),
		NewShelf("3", 2, 4),
	}
	warehouseMap := InitWareHouse(row, col, configPoints)

	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))
	printWarehouseMap(row, col, warehouseMap)
	DebugOutput("货架列表:", 1)
	DebugOutputStrings(shelves.String(), 2)

	taskGroup := NewAGVTaskGroup("01")
	taskGroup.AddTask(NewTaskItem("01", AGV_LIFT_STATUS_LIFTED, 2, 3, TASK_TYPE_MOVE_TO))
	taskGroup.AddTask(NewTaskItem("01", AGV_LIFT_STATUS_LIFTED, -1, -3, TASK_TYPE_LIFT))
	taskGroup.AddTask(NewTaskItem("01", AGV_LIFT_STATUS_LIFTED, 3, 3, TASK_TYPE_MOVE_TO))
	taskGroup.AddTask(NewTaskItem("01", AGV_LIFT_STATUS_UNLIFTED, -1, -3, TASK_TYPE_LIFT))
	taskGroup.AddTask(NewTaskItem("01", AGV_LIFT_STATUS_LIFTED, 1, 3, TASK_TYPE_MOVE_TO))

	TaskDistributeCenter := NewTaskDistributeCenter()
	TaskDistributeCenter.AddTaskGroup(taskGroup)

	simulators := AGVSimulatorCoreList{}
	agvSouls := AGVSoulList{}
	agvID01 := "01"

	simulators = append(simulators, NewAGVSimulatorCore(agvID01, 1, 3))
	simulators.StartRunning(1 * time.Second)

	var pathCaculateCenter *PathCaculateCenter

	funcReceiveAgvData := func(data *AgvData) {
		pathCaculateCenter.ChIn_AGVdata <- data
		for _, soul := range agvSouls {
			if data.ID == soul.ID {
				soul.ChIn_AGVdata <- data
			}
		}
	}
	simulators.RegisteAGVStatusDataReceiver(funcReceiveAgvData)

	agvSouls = append(agvSouls, NewAGVSoul(agvID01, simulators.ReceiveTaskCommand))

	agvCores := agvSouls.ToAGVSoulCore()

	pathCaculateCenter = NewPathCaculateCenter(warehouseMap, agvCores, shelves)

	pathCaculateCenter.SetTaskStatusReceiver(TaskDistributeCenter.ChIn_TaskEnd)
	TaskDistributeCenter.SetTaskReceiver(pathCaculateCenter.ChIn_TaskReceiver)

	agvSouls.StartRunning()
	pathCaculateCenter.StartRunning()
	TaskDistributeCenter.StartRunning()

	time.Sleep(15 * time.Second)
	printWarehouseMap(row, col, warehouseMap)
	DebugOutput("货架列表:", 1)
	DebugOutputStrings(shelves.String(), 2)

	fmt.Println("### 005 测试完成 ...")
}
func tryTask004(t *testing.T) {
	//只有货架，AGV只是执行相应动作
	fmt.Println("测试 004")
	fmt.Println("### 测试agv绕过一排货架 之后进行举升，再绕过货架回到原地...")
	row, col := 6, 6
	configPoints := WarehouseConfigPointList{}
	shelves := ShelfList{
		NewShelf("1", 2, 2),
		NewShelf("2", 2, 3),
		NewShelf("3", 2, 4),
	}
	warehouseMap := InitWareHouse(row, col, configPoints)
	// warehouseMap.AddShelfToCalCenter(shelves)

	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))

	taskGroup := NewAGVTaskGroup("01")
	taskGroup.AddTask(NewTaskItem("01", AGV_LIFT_STATUS_LIFTED, 3, 3, TASK_TYPE_MOVE_TO))
	taskGroup.AddTask(NewTaskItem("01", AGV_LIFT_STATUS_LIFTED, 1, 3, TASK_TYPE_LIFT))
	taskGroup.AddTask(NewTaskItem("01", AGV_LIFT_STATUS_LIFTED, 1, 3, TASK_TYPE_MOVE_TO))

	TaskDistributeCenter := NewTaskDistributeCenter()
	TaskDistributeCenter.AddTaskGroup(taskGroup)

	simulators := AGVSimulatorCoreList{}
	agvSouls := AGVSoulList{}
	agvID01 := "01"

	simulators = append(simulators, NewAGVSimulatorCore(agvID01, 1, 3))
	simulators.StartRunning(1 * time.Second)

	var pathCaculateCenter *PathCaculateCenter

	funcReceiveAgvData := func(data *AgvData) {
		pathCaculateCenter.ChIn_AGVdata <- data
		for _, soul := range agvSouls {
			if data.ID == soul.ID {
				soul.ChIn_AGVdata <- data
			}
		}
	}
	simulators.RegisteAGVStatusDataReceiver(funcReceiveAgvData)

	agvSouls = append(agvSouls, NewAGVSoul(agvID01, simulators.ReceiveTaskCommand))

	agvCores := agvSouls.ToAGVSoulCore()

	pathCaculateCenter = NewPathCaculateCenter(warehouseMap, agvCores, shelves)

	pathCaculateCenter.SetTaskStatusReceiver(TaskDistributeCenter.ChIn_TaskEnd)
	TaskDistributeCenter.SetTaskReceiver(pathCaculateCenter.ChIn_TaskReceiver)

	agvSouls.StartRunning()
	pathCaculateCenter.StartRunning()
	TaskDistributeCenter.StartRunning()

	time.Sleep(15 * time.Second)
	fmt.Println("### 004 测试完成 ...")
}
func tryTask003(t *testing.T) {
	fmt.Println("测试 003")
	fmt.Println("### 测试测试交叉路径行走 ...")
	row, col := 6, 6
	configPoints := WarehouseConfigPointList{}
	warehouseMap := InitWareHouse(row, col, configPoints)
	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))

	var pathCaculateCenter *PathCaculateCenter
	var simulators AGVSimulatorCoreList = AGVSimulatorCoreList{}
	agvSouls := AGVSoulList{}
	agvID01 := "01"
	agvID02 := "02"

	simulators = append(simulators, NewAGVSimulatorCore(agvID01, 3, 1))
	simulators = append(simulators, NewAGVSimulatorCore(agvID02, 1, 3))
	simulators.StartRunning(1 * time.Second)

	funcReceiveAgvData := func(data *AgvData) {
		pathCaculateCenter.ChIn_AGVdata <- data
		for _, soul := range agvSouls {
			if data.ID == soul.ID {
				soul.ChIn_AGVdata <- data
			}
		}
	}
	agvSouls = append(agvSouls, NewAGVSoul(agvID01, simulators.ReceiveTaskCommand))
	agvSouls = append(agvSouls, NewAGVSoul(agvID02, simulators.ReceiveTaskCommand))
	agvSouls.StartRunning()

	agvCores := agvSouls.ToAGVSoulCore()
	// for _, soul := range agvSouls {
	// 	agvCores = append(agvCores, NewAGVSoulCore(soul.ID, soul.ChIn_TaskEndRing))
	// }
	pathCaculateCenter = NewPathCaculateCenter(warehouseMap, agvCores, ShelfList{})
	pathCaculateCenter.StartRunning()

	simulators.RegisteAGVStatusDataReceiver(funcReceiveAgvData)

	task01 := NewTaskItem(agvID01, AGV_LIFT_STATUS_LIFTED, 3, 6, TASK_TYPE_MOVE_TO)
	task02 := NewTaskItem(agvID02, AGV_LIFT_STATUS_LIFTED, 6, 3, TASK_TYPE_MOVE_TO)
	pathCaculateCenter.SetTask(task01)
	pathCaculateCenter.SetTask(task02)
	time.Sleep(16 * time.Second)

	fmt.Println("### 003 测试完成 ...")
}

func tryTask001_1(t *testing.T) {
	fmt.Println("测试 001_1")
	fmt.Println("### 测试AGV举升 ...")
	row, col := 6, 6
	configPoints := WarehouseConfigPointList{}
	warehouseMap := InitWareHouse(row, col, configPoints)
	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))

	var pathCaculateCenter *PathCaculateCenter
	var simulator *AGVSimulatorCore
	agvSouls := AGVSoulList{}
	agvCores := AGVSoulCoreList{}
	agvID := "01"

	simulator = NewAGVSimulatorCore(agvID, 1, 1)
	simulator.StartRunning(2 * time.Second)

	funcSendCommand := func(id string, task *TaskCommand) {
		// DebugOutput("向模拟器发送命令："+task.String(), 3)
		if simulator.ChIn_TaskReceiver == nil {
			DebugOutput("无法向模拟器发送命令", 2)
		} else {
			simulator.ChIn_TaskReceiver <- task //发送命令给模拟器
		}
	}
	funcReceiveAgvData := func(data *AgvData) {
		pathCaculateCenter.ChIn_AGVdata <- data
		for _, soul := range agvSouls {
			soul.ChIn_AGVdata <- data
		}
	}
	agvSouls = append(agvSouls, NewAGVSoul(agvID, funcSendCommand))
	agvSouls.StartRunning()

	for _, soul := range agvSouls {
		agvCores = append(agvCores, NewAGVSoulCore(soul.ID, soul.ChIn_TaskEndRing))
	}
	pathCaculateCenter = NewPathCaculateCenter(warehouseMap, agvCores, ShelfList{})
	pathCaculateCenter.StartRunning()

	simulator.RegisteAGVStatusDataReceiver(funcReceiveAgvData)

	task := NewTaskItem(agvID, AGV_LIFT_STATUS_LIFTED, 2, 2, TASK_TYPE_LIFT)
	pathCaculateCenter.SetTask(task)
	time.Sleep(5 * time.Second)

	fmt.Println("### 001_1 测试完成 ...")
}
func tryTask001(t *testing.T) {
	fmt.Println("测试 001")
	fmt.Println("### 测试一个AGV行走至指定点 ...")
	row, col := 6, 6
	configPoints := WarehouseConfigPointList{}
	warehouseMap := InitWareHouse(row, col, configPoints)
	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))

	var pathCaculateCenter *PathCaculateCenter
	var simulator *AGVSimulatorCore
	agvSouls := AGVSoulList{}
	agvCores := AGVSoulCoreList{}
	agvID := "01"

	simulator = NewAGVSimulatorCore(agvID, 1, 1)
	simulator.StartRunning(2 * time.Second)

	funcSendCommand := func(id string, task *TaskCommand) {
		// DebugOutput("向模拟器发送命令："+task.String(), 3)
		if simulator.ChIn_TaskReceiver == nil {
			DebugOutput("无法向模拟器发送命令", 2)
		} else {
			simulator.ChIn_TaskReceiver <- task //发送命令给模拟器
		}
	}
	funcReceiveAgvData := func(data *AgvData) {
		pathCaculateCenter.ChIn_AGVdata <- data
		for _, soul := range agvSouls {
			soul.ChIn_AGVdata <- data
		}
	}
	agvSouls = append(agvSouls, NewAGVSoul(agvID, funcSendCommand))
	agvSouls.StartRunning()

	for _, soul := range agvSouls {
		agvCores = append(agvCores, NewAGVSoulCore(soul.ID, soul.ChIn_TaskEndRing))
	}
	pathCaculateCenter = NewPathCaculateCenter(warehouseMap, agvCores, ShelfList{})
	pathCaculateCenter.StartRunning()

	simulator.RegisteAGVStatusDataReceiver(funcReceiveAgvData)

	task := NewTaskItem(agvID, AGV_LIFT_STATUS_UNLIFTED, 2, 2, TASK_TYPE_MOVE_TO)
	pathCaculateCenter.SetTask(task)
	time.Sleep(8 * time.Second)

	fmt.Println("### 001 测试完成 ...")
}
