package agvScheduler

import (
	"fmt"
	// "strings"
	// "strconv"
	"time"
)

/*
	center掌控了仓库的地图数据、货架配置数据以及代表AGV的棋子，能够在这些数据之上进行路径的计算，货架位置的推算和任务的执行
	===============================

	功能列表：
	- center初始化完成后，处于等待任务状态
	- 处于等待任务状态时，如果接收到任务，首先会将任务初始化，即如果任务是运行任务，计算行驶路径，并将建议路径发送给AGVSoul。
	- 接收到AGV运行数据，需要完成两件事情：
		- 更新AGV棋子在center中的信息，以及根据逻辑推算货架的位置
		- 检查任务的完成情况：如果任务完成，不再提醒AGVSoul，并通知任务分发中心；否则，发送任务数据给AGVSoul，AGVSoul根据情况处理
	- 对于一个AGV任务，center将其具体化为一个带有GUID的命令，直到AGV任务完成，该命令才会结束，否则执行过程中只对该命令进行修改，不再重建
	- 任务分发：
		- 中心有任务分发时，首先从center中选择合适的AGVCore，之后将标识有该AGVCore ID的任务发送给center
		- 选择合适的AGVCore的主要依据是任务的连贯性、AGVCore的任务量、AGV的运行状态


*/

var TASK_INIT_TICKER_INTERVAL time.Duration = 2 * time.Second

//============================================================================================================

func NewPathCaculateCenter(wareHouseMap PointPointerList, agvCoreList AGVSoulCoreList, shelfList ShelfList) *PathCaculateCenter {
	// agvSoulCoreList := AGVSoulCoreList{}
	// for _, agvSoul := range agvSoulList {
	// 	agvSoulCoreList = append(agvSoulCoreList, NewAGVSoulCore(agvSoul.ID, agvSoul.LiftStatus, agvSoul.X, agvSoul.Y, agvSoul.ChIn_TaskEndRing))
	// }
	// wareHouseMap.AddShelfToCalCenter(shelfList)
	center := &PathCaculateCenter{
		WareHouseMap:      wareHouseMap,
		AGVCores:          agvCoreList,
		ShelfList:         shelfList,
		ChIn_AGVdata:      make(chan *AgvData, 100),
		ChIn_TaskReceiver: make(chan *AGVTaskItem, 10),
	}
	for _, agvCore := range agvCoreList {
		agvCore.Center = center
	}

	return center
}

type PathCaculateCenter struct {
	WareHouseMap             PointPointerList
	AGVCores                 AGVSoulCoreList
	ShelfList                ShelfList
	ChIn_AGVdata             chan *AgvData //AGV运行状态信息的传入通道
	ChIn_TaskReceiver        chan *AGVTaskItem
	ChTaskTicker             <-chan time.Time  //检查任务执行情况
	ChOut_TaskStatusReportor chan *AGVTaskItem //报告任务完成状态
}

func (this *PathCaculateCenter) SetTaskStatusReceiver(receiver chan *AGVTaskItem) {
	this.ChOut_TaskStatusReportor = receiver
}
func (this *PathCaculateCenter) ReportTaskEnd(task *AGVTaskItem) {
	if this.ChOut_TaskStatusReportor != nil {
		this.ChOut_TaskStatusReportor <- task
	}
}
func (this *PathCaculateCenter) TryFindAGVPath(agvID string, x, y, destX, destY int, liftStatus string) (PathPointList, error) {
	// DebugOutput("接收到路径计算请求："+request.String()+GetFileLocation(), 3)
	pointStart := this.WareHouseMap.GetPoint(x, y)
	if pointStart == nil {
		// return nil, NewLogicError("起点不存在")
		return nil, ErrNoPointOnMap
	}
	destPoint := this.WareHouseMap.GetPoint(destX, destY)
	if destPoint == nil {
		return nil, ErrNoPointOnMap
		// return nil, NewLogicError("终点不存在")
	}

	this.WareHouseMap.ClearFindingFootPrint()
	this.WareHouseMap.UnblockPathPoint(agvID)
	pointStart.SetStartFlag()

	if destPoint.IsBanned() {
		// DebugOutput("目标点不可行驶"+GetFileLocation(), 2)
		// return nil, NewLogicError("目标点不可行驶")
		return nil, ErrPointBanned
	}
	if destPoint.CanBeEntered(this.WareHouseMap) == false {
		// DebugOutput("目标点周围没有点可以进入"+GetFileLocation(), 2)
		// return nil, NewLogicError("目标点周围没有点可以进入")
		return nil, ErrPointCannotEntered
	}

	var predictor PointPredictor
	if liftStatus == AGV_LIFT_STATUS_UNLIFTED {
		predictor = func(point *Point) bool {
			return point.IsBanned() == false
		}
	} else {
		predictor = func(point *Point) bool {
			return point.IsBanned() == false &&
				// point.IsBlocked() == false &&
				// point.IsShelfPlaced([]IShelf(this.ShelfList)) == false
				point.IsShelfPlaced(this.ShelfList) == false
		}
	}

	if path, err := pointStart.FindPathPoints(this.WareHouseMap, destPoint, predictor); err != nil {
		// DebugOutput(fmt.Sprintf("查找路径失败: %s", err.Error())+GetFileLocation(), 2)
		return nil, err
	} else {
		return ConvertPointListToPathPointList(path), nil
	}
}

//将外部AGV的数据更新到计算中心，包括AGV的位置、举升状态，以及由此推测出的货架位置和状态
//系统将保存AGV和货架的上一次的位置和状态缓存，由此推断变化
//AGV的位置从默认的(0, 0)开始，货架从设置或者数据库数据开始
func (this *PathCaculateCenter) UpdateData(data *AgvData) error {
	//从初始化开始
	if agvCore, err := this.AGVCores.FindAGVbyID(data.ID); err != nil {
		return err
	} else {
		//规定：初始化AGV状态，必须使用AGV心跳发送的数据，保证位置和举升状态的同时更新
		if agvCore.IsInitialized() == false {
			if data.DataType != ACCEPTED_CMD_TYPE_HEART_BEATING {
				return nil
			}
			//开始初始化AGV
			agvCore.UpdatePosition(data.X, data.Y)
			agvCore.UpdateLiftStatus(data.LiftStatus)
			agvCore.TryEndTask(data)
		} else {
			//正式更新数据
			switch data.DataType {
			case ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED:
				if b := agvCore.UpdatePosition(data.X, data.Y); b == true {
					//agv发生了移动，更新关联的货架
					//如果AGV处于举升状态，并且之前的位置有货架，那么说明货架也随之移动到了现在的点
					if agvCore.LiftStatus == AGV_LIFT_STATUS_LIFTED {
						// DebugOutput(fmt.Sprintf("尝试查找点(%d, %d)的货架", agvCore.lastX, agvCore.lastY), 1)
						shelf := this.ShelfList.FindShelfByPosition(agvCore.lastX, agvCore.lastY)
						if shelf != nil {
							// DebugOutput(fmt.Sprintf("找到了点(%d, %d)位置的货架", agvCore.lastX, agvCore.lastY), 1)
							shelf.UpdatePosition(agvCore.X, agvCore.Y)
							G_chanBroadcastEvent <- NewEventMsg(EVENT_POSITION_NEW, NewAGVTransferred(agvCore.ID, agvCore.LiftStatus, agvCore.X, agvCore.Y, shelf))
						} else {
							// DebugOutput(fmt.Sprintf("未找到点(%d, %d)位置的货架", agvCore.lastX, agvCore.lastY), 1)

						}
					} else {
						G_chanBroadcastEvent <- NewEventMsg(EVENT_POSITION_NEW, NewAGVTransferred(agvCore.ID, agvCore.LiftStatus, agvCore.X, agvCore.Y, nil))

					}
					//通知AGVSoul执行任务
					agvCore.TryEndTask(data)
				}

			case ACCEPTED_CMD_TYPE_AGV_LIFT_STATUS_CHANGED:
				if b := agvCore.UpdateLiftStatus(data.LiftStatus); b == true {
					//举升状态发生了变化，与货架的关系可能发生变化

					agvCore.TryEndTask(data)
				}
			}
		}
	}
	return nil
}

//根据数据推算AGV是否进入了其余AGV锁定的区域
func (this *PathCaculateCenter) CheckRushIntoOthersArea(id string, x, y int) {
	point := this.WareHouseMap.GetPoint(x, y)
	if point != nil {
		if point.IsBlocked() && point.BlockerID != id {
			DebugOutput(fmt.Sprintf("AGV(%s) 进入了 AGV(%s) 的封锁区域", id, point.BlockerID), 1)

		}
	}
}

//根据数据推算是否AGV发生了碰撞
func (this *PathCaculateCenter) CheckAGVCollision() {
	//如果两个AGV在同一个位置，就是发生碰撞了
	count := len(this.AGVCores)
	for i := 0; i < count-1; i++ {
		for j := i + 1; j < count; j++ {
			agv1 := this.AGVCores[i]
			agv2 := this.AGVCores[j]
			if agv1.X == agv2.X && agv1.Y == agv2.Y {
				DebugOutput(fmt.Sprintf("AGV(%s) 与 AGV(%s) 发生了碰撞", agv1.ID, agv2.ID)+GetFileLocation(), 1)
			} else {
				DebugOutput(fmt.Sprintf("AGV %s(%d, %d) : AGV %s(%d, %d)", agv1.ID, agv1.X, agv1.Y, agv2.ID, agv2.X, agv2.Y)+GetFileLocation(), 4)
			}
		}
	}
}

//从计算中心获取建议路径，
//参数是AGV计算后要行走的路径，但是由于该路径上可能有AGV挡道，需要计算中心统一调度,
//计算中心查找该路径上的AGV，如果路径上没有AGV，返回建议行走的点,
//一般来说，返回的点的数量有最大值，计算中心可以根据该区域内的AGV总量动态调整该最大值以保证整体的运行效率,
//计算中心返回建议的点之前会将该点锁定，其余AGV不能通过该点,
//锁定点的解锁自动清理：
//   1 如果AGV行走过，自动解锁；
//   2 如果AGV与锁定的一段路径不相连，则说明因为AGV跳至其它点，该段路径全部解锁
func (this *PathCaculateCenter) GetAdvicedPath(id string, path PathPointList) PathPointList {
	advicedPath := path
	if len(path) > 2 {
		advicedPath = path[0:2]
	}
	for i, pathPoint := range advicedPath {
		if err := this.WareHouseMap.BlockPoint(id, pathPoint.X, pathPoint.Y); err != nil {
			DebugOutput(fmt.Sprintf("AGV %s 试图锁定点(%d, %d)时失败：", id, pathPoint.X, pathPoint.Y)+err.Error()+GetFileLocation(), 3)
			advicedPath = append(advicedPath[0:i-1], advicedPath[i+1:]...)
			break
		}
	}
	DebugOutput(fmt.Sprintf("AGV %s 锁定 %s", id, advicedPath.FormatPath())+GetFileLocation(), 3)
	return advicedPath
}

func (this *PathCaculateCenter) StartRunning() {
	this.ChTaskTicker = time.Tick(TASK_INIT_TICKER_INTERVAL)
	go func() {
		for {
			select {
			case data := <-this.ChIn_AGVdata:
				DebugOutput("接收到AGV数据:"+data.Summarize()+GetFileLocation(), 4)
				this.UpdateData(data)
			case task := <-this.ChIn_TaskReceiver:
				if err := this.InitTask(task); err != nil {
					DebugOutput("新任务初始化失败："+err.Error()+GetFileLocation(), 2)
				}
			case <-this.ChTaskTicker:
				this.WakeUpAGVTasks()
				// for _, agvCore := range this.AGVCores {
				// 	if agvCore.IsTaskInitialized() == false {
				// 		if err := agvCore.InitTask(); err != nil {
				// 			DebugOutput("任务初始化失败："+err.Error()+GetFileLocation(), 2)
				// 		}
				// 	}
				// }
			}
		}
	}()
}
func (this *PathCaculateCenter) SetTask(task *AGVTaskItem) {
	this.ChIn_TaskReceiver <- task
}

//如果是行驶任务，需要计算并保存行驶路径
//如果是举升任务，无需初始化
func (this *PathCaculateCenter) InitTask(task *AGVTaskItem) error {
	DebugOutput("Center 进行任务初始化 "+task.Summarize()+"..."+GetFileLocation(), 3)
	if len(task.AGVID) <= 0 {
		// return NewLogicError("未指定执行任务的AGV")
		return ErrTaskNoAGVID
	}
	agvCore, err := this.AGVCores.FindAGVbyID(task.AGVID)
	if err != nil {
		return err
	}
	if err := agvCore.SetTask(task); err != nil {
		return err
	}
	// agvCore.Task = task
	return agvCore.InitTask()
}
func (this *PathCaculateCenter) WakeUpAGVTasks() {
	for _, agvCore := range this.AGVCores {
		agvCore.ExecuteTask()
	}
}
