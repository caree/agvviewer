package agvScheduler

import (
	// "errors"
	"fmt"
	// "time"
)

type AgvData struct {
	ID         string
	DataType   string
	Status     string
	X          int
	Y          int
	LiftStatus string
	GUID       int64
}

func (this *AgvData) Summarize() string {
	str := "AGV " + this.ID
	switch this.DataType {
	case ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED:
		return str + fmt.Sprintf(" 移动到(%2d, %2d)", this.X, this.Y)
	case ACCEPTED_CMD_TYPE_AGV_LIFT_STATUS_CHANGED:
		switch this.LiftStatus {
		case AGV_LIFT_STATUS_UNLIFTED:
			return str + " 降下"
		case AGV_LIFT_STATUS_LIFTED:
			return str + " 举升"
		}
	case ACCEPTED_CMD_TYPE_HEART_BEATING:
		lifted := "是"
		if this.LiftStatus == AGV_LIFT_STATUS_UNLIFTED {
			lifted = "否"
		}
		return str + fmt.Sprintf(" 心跳(%2d, %2d) 举升：%s", this.X, this.Y, lifted)
	}
	return ""
}
