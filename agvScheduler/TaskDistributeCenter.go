package agvScheduler

import (
	// "errors"
	// "fmt"
	"time"
)

/*
  AGVTaskList是所有AGV的任务列表，按照AGV的ID区分，成员还有AGV本身和AGV要执行的任务列表
  TaskList 是一系列任务，任务要由AGV来执行
  AGV本身是任务执行的主体
  接收到AGV数据状态数据后，交由任务分配中心处理，中心根据协议，将数据转交给对应ID的任务处理器
  任务处理器根据新数据和当前AGV的任务进行处理，更新任务状态
*/

var TASK_DISTRIBUTE_TICKER_INTERVAL time.Duration = 2 * time.Second

//TaskDistributeCenter负责任务的分发,
//被赋予完成的任务组是没有选定AGV的，所以分发前首先需要选择合适AGV
//每个任务完成后会接到通知，center需要把后续的任务发送出去
type TaskDistributeCenter struct {
	AGVTaskGroups           AGVTaskGroupList
	ChIn_TaskEnd            chan *AGVTaskItem
	ChTicker_DistributeTask <-chan time.Time
	ChOut_TaskDistributer   chan *AGVTaskItem
}

func NewTaskDistributeCenter() *TaskDistributeCenter {
	center := TaskDistributeCenter{
		AGVTaskGroups: AGVTaskGroupList{},
		ChIn_TaskEnd:  make(chan *AGVTaskItem),
	}
	return &center
}
func (this *TaskDistributeCenter) StartRunning() {
	this.ChTicker_DistributeTask = time.Tick(TASK_DISTRIBUTE_TICKER_INTERVAL)
	go func() {
		for {
			select {
			case task := <-this.ChIn_TaskEnd:
				//任务完成状态通知
				this.EndTask(task)
			case <-this.ChTicker_DistributeTask:
				//周期性的分发任务
				this.DistributeTask()
			}
		}
	}()

}
func (this *TaskDistributeCenter) EndTask(task *AGVTaskItem) {
	for _, group := range this.AGVTaskGroups {
		if group.GroupID == task.GroupID {
			for i, taskTemp := range group.TaskList {
				if taskTemp.GUID == task.GUID {
					group.TaskList = group.TaskList[i+1:]
					break
				}
			}
			break
		}
	}
}
func (this *TaskDistributeCenter) DistributeTask() {
	for _, group := range this.AGVTaskGroups {
		if this.ChOut_TaskDistributer != nil {
			if len(group.TaskList) > 0 {
				this.ChOut_TaskDistributer <- group.TaskList[0]
			}
		} else {
			DebugOutput("没有任务分发通道"+GetFileLocation(), 1)
		}
	}
}
func (this *TaskDistributeCenter) AddTaskGroup(group *AGVTaskGroup) {
	this.AGVTaskGroups = append(this.AGVTaskGroups, group)
}
func (this *TaskDistributeCenter) SetTaskReceiver(receiver chan *AGVTaskItem) {
	this.ChOut_TaskDistributer = receiver
}
func (this *TaskDistributeCenter) AddTaskItem(groupID string, task *AGVTaskItem) {
	if group := this.AGVTaskGroups.FindGroupByID(groupID); group == nil {
		group = NewAGVTaskGroup(groupID)
		this.AddTaskGroup(group)
		group.AddTask(task)
	} else {
		group.AddTask(task)
	}
}
