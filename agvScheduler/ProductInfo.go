package agvScheduler

import (
// "encoding/json"
// "fmt"
// "github.com/astaxie/beego"
// "github.com/bitly/go-simplejson"
)

type ProductInfo struct {
	ID   string
	Name string
	Note string
}
type ProductInfoList []ProductInfo
