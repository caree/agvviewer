package agvScheduler

import (
	"fmt"
	// "github.com/astaxie/beego"
)

func SetAGVStatus(id, status string) {
	cmd := ""
	switch AGV_Status(status) {
	case AGV_STATUS_RUNNING:
		cmd = fmt.Sprintf("[%s,%s,%s]", CMD_SET_AGV_STATUS, id, AGV_STATUS_RUNNING)
	case AGV_STATUS_RUNNING_PAUSED:
		cmd = fmt.Sprintf("[%s,%s,%s]", CMD_SET_AGV_STATUS, id, AGV_STATUS_RUNNING_PAUSED)
	case AGV_STATUS_RUNNING_STOPPED:
		cmd = fmt.Sprintf("[%s,%s,%s]", CMD_SET_AGV_STATUS, id, AGV_STATUS_RUNNING_STOPPED)
	default:
		return
	}
	DebugOutput("setAGVStatus => "+cmd+GetFileLocation(), 3)
}

// func SetAgvNextSteps(id string, point *Point) {
// 	// func setAgvNextSteps(id string, list PointPointerList) {
// 	cmd := fmt.Sprintf("[%s,%s,%d,%d]", CMD_SET_NEXT_STEP, id, point.X, point.Y)
// 	DebugOutput(fmt.Sprintf("setAgvNextSteps => %s", cmd)+GetFileLocation(), 3)
// 	writeData2Connection(cmd)
// }
