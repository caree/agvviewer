package agvScheduler

import (
	// "encoding/json"
	// "errors"
	"fmt"
	// "strconv"
)

// type Orientation string
type OrientationList []string

const (
	Orientation_Left  string = "left"
	Orientation_Right string = "right"
	Orientation_Up    string = "up"
	Orientation_Down  string = "down"
	// Orientation_None  string = "all"
)

func (this OrientationList) Has(orientation string) bool {
	for _, ori := range this {
		if ori == orientation {
			return true
		}
	}
	return false
}

//============================================================================================================

type WarehousePointType int

type Point struct {
	X                      int
	Y                      int
	existsDistance         int              //已经走过的距离
	directDistanceFromDest int              //距离终点的距离，当为1时表示已经到达终点
	linkEndList            PointPointerList //终点列表，只在根节点中存在
	fatherPoint            *Point
	isStartPoint           bool
	Orientations           []string
	IsBlockedByAGV         bool      //是否被AGV锁定
	BlockerID              string    //锁定改点的AGV的ID
	MapBlockLinked         *MapBlock //关联的障碍类型，包括可以行走、禁止行走、充电区等区域
	// ShelfLinked            *ShelfInfo //关联的货架
	// AgvLinked              *AGV       //关联的AGV
	// GUID                   int64
}

func NewPoint(x, y int, mapBlock *MapBlock, orientations []string) *Point {
	return &Point{
		X:                      x,
		Y:                      y,
		MapBlockLinked:         mapBlock,
		existsDistance:         0,
		directDistanceFromDest: 0,
		linkEndList:            nil,
		fatherPoint:            nil,
		Orientations:           orientations,
		// orientations:           OrientationList{},
	}
}

type PointPredictor func(*Point) bool

func (this *Point) ToString() string {
	return fmt.Sprintf("[(%d, %d), %d %s]", this.X, this.Y, this.MapBlockLinked.BlockType, this.Orientations)
}

func (this *Point) FindPathPoints(mapTemp PointPointerList, destPoint *Point, predictor PointPredictor) (PointPointerList, error) {
	// DebugOutput("FindPathPoints : "+this.ToString()+" => "+destPoint.ToString()+GetFileLocation(), 3)

	if this.X == destPoint.X && this.Y == destPoint.Y {
		DebugOutput("已经在目标点上", 2)
		return nil, nil
	}
	this.evaluateDistance(destPoint)
	if this.directDistanceFromDest <= 1 {
		if predictor(destPoint) == false {
			DebugOutput("目标点当前不可行驶", 2)
			// return nil, NewAgvError(AGV_ERROR_CODE_逻辑异常, ("目标点当前不可行驶"))
			return nil, ErrPointOccupied
		}
	}

	if this.OrientationAllowed(Orientation_Left) {
		leftPoint := mapTemp.GetPoint(this.X-1, this.Y)
		if leftPoint != nil && leftPoint.isValidPoint() &&
			predictor(leftPoint) && !leftPoint.isChildPoint(this) {
			leftPoint.evaluateDistance(destPoint)
			leftPoint.fatherPoint = this
			leftPoint.existsDistance = this.existsDistance + 1
			this.setLinkEndList(append(this.getLinkEndList(), leftPoint))
		}
	}

	if this.OrientationAllowed(Orientation_Right) {
		rightPoint := mapTemp.GetPoint(this.X+1, this.Y)
		if rightPoint != nil && rightPoint.isValidPoint() &&
			predictor(rightPoint) && !rightPoint.isChildPoint(this) {
			rightPoint.evaluateDistance(destPoint)
			rightPoint.fatherPoint = this
			rightPoint.existsDistance = this.existsDistance + 1
			this.setLinkEndList(append(this.getLinkEndList(), rightPoint))
		}
	}
	if this.OrientationAllowed(Orientation_Down) {
		downPoint := mapTemp.GetPoint(this.X, this.Y+1)
		if downPoint != nil && downPoint.isValidPoint() &&
			predictor(downPoint) && !downPoint.isChildPoint(this) {
			downPoint.evaluateDistance(destPoint)
			downPoint.fatherPoint = this
			downPoint.existsDistance = this.existsDistance + 1
			this.setLinkEndList(append(this.getLinkEndList(), downPoint))
		}
	}
	if this.OrientationAllowed(Orientation_Up) {
		upPoint := mapTemp.GetPoint(this.X, this.Y-1)
		if upPoint != nil && upPoint.isValidPoint() &&
			predictor(upPoint) && !upPoint.isChildPoint(this) {
			upPoint.evaluateDistance(destPoint)
			upPoint.fatherPoint = this
			upPoint.existsDistance = this.existsDistance + 1
			this.setLinkEndList(append(this.getLinkEndList(), upPoint))
		}
	}
	for _, pointTemp := range this.getLinkEndList() {
		if pointTemp.directDistanceFromDest <= 0 {
			DebugOutput("查找路径成功"+GetFileLocation(), 3)
			return pointTemp.getPathPoint(PointPointerList{}), nil

			// if predictor(destPoint) == true {
			// 	DebugOutput("查找路径成功"+GetFileLocation(), 3)
			// 	return pointTemp.getPathPoint(PointPointerList{}), nil
			// } else {
			// 	DebugOutput("目标点当前不可行驶", 2)
			// 	return nil, NewAgvError(AGV_ERROR_CODE_没有找到路径, "目标点当前不可行驶")
			// }
		}
	}

	if len(this.getLinkEndList()) > 0 {
		//可以继续寻找
		list := this.getLinkEndList()
		//list.printPoints()
		nextLinkEndPoint := list[0]
		count := len(list)
		for i := 1; i < count; i++ {
			otherPointDistance := list[i].existsDistance + list[i].directDistanceFromDest
			chosedPointDistance := nextLinkEndPoint.existsDistance + nextLinkEndPoint.directDistanceFromDest
			if otherPointDistance < chosedPointDistance {
				nextLinkEndPoint = list[i]
			}
		}
		this.removeLindEndPoint(nextLinkEndPoint.X, nextLinkEndPoint.Y)
		return nextLinkEndPoint.FindPathPoints(mapTemp, destPoint, predictor)
	} else {
		DebugOutput("无法查找到路径，所有路径被封闭"+GetFileLocation(), 2)
		// return nil, NewAgvError(AGV_ERROR_CODE_逻辑异常, ("无法查找到路径，所有路径被封闭"))
		return nil, ErrNoPathFound
	}
}
func (this *Point) SetStartFlag() {
	this.isStartPoint = true
}
func (this *Point) removeLindEndPoint(x, y int) {
	list := this.getLinkEndList()
	var listNew PointPointerList
	for i, pointTemp := range list {
		if pointTemp.X == x && pointTemp.Y == y {
			listNew = append(list[:i], list[(i+1):]...)
		}
	}
	this.setLinkEndList(listNew)
}
func (this *Point) setLinkEndList(list PointPointerList) {
	if this.fatherPoint == nil {
		this.linkEndList = list
	} else {
		this.fatherPoint.setLinkEndList(list)
	}
}
func (this *Point) getLinkEndList() PointPointerList {
	if this.fatherPoint == nil {
		if this.linkEndList == nil {
			this.linkEndList = PointPointerList{}
		}
		return this.linkEndList
	} else {
		return this.fatherPoint.getLinkEndList()
	}
}
func (this *Point) getPathPoint(list PointPointerList) PointPointerList {
	// DebugOutput("getPathPoint => "+this.ToString()+GetFileLocation(), 3)
	if this.fatherPoint != nil {
		// DebugOutput("getPathPoint => fatherPoint: "+this.fatherPoint.ToString()+GetFileLocation(), 3)
		temp := append(list, this)
		return this.fatherPoint.getPathPoint(temp)
		// return append(temp, this.fatherPoint.getPathPoint()...)
	} else {
		// return list
		return append(list, this)
	}
}
func (this *Point) evaluateDistance(destPoint *Point) {
	xValue := this.X - destPoint.X
	if xValue < 0 {
		xValue = -xValue
	}
	yValue := this.Y - destPoint.Y
	if yValue < 0 {
		yValue = -yValue
	}
	this.directDistanceFromDest = xValue + yValue
}
func (this *Point) isChildPoint(child *Point) bool {
	if this.existsDistance > 0 || this.isStartPoint == true {
		return true
	}
	return false
}

// func (this *Point) PlacedShelf(shelf *ShelfInfo) error {
// 	if this.ShelfLinked != nil && shelf != nil {
// 		return NewAgvError(AGV_ERROR_CODE_目的地被占用, ("该点已经有货架"))
// 	}
// 	this.ShelfLinked = shelf
// 	return nil
// }
func (this *Point) OrientationAllowed(orientation string) bool {
	for _, orientationTemp := range this.Orientations { //还有该方向，表示可以行走
		if orientationTemp == orientation {
			DebugOutput("Orientation "+orientation+" Ok "+this.ToString()+GetFileLocation(), 4)
			return true
		}
	}
	if len(this.Orientations) <= 0 { //没有规定，表示各个方向都可以
		DebugOutput("Orientation "+orientation+" Ok "+this.ToString()+GetFileLocation(), 4)
		return true
	}
	// if this.Orientation == Orientation_None {
	// 	return true
	// }
	DebugOutput("Orientation "+orientation+" denied "+this.ToString()+GetFileLocation(), 3)
	// fmt.Println(this.Orientations)
	// fmt.Println(len(this.Orientations))
	return false
}
func (this *Point) CanBeEntered(mapTemp PointPointerList) bool {
	// DebugOutput("Test CanBeEntered: "+this.ToString()+GetFileLocation(), 3)
	funcTest := func(x, y int, orientation string) bool {
		point := mapTemp.GetPoint(x, y)
		if point != nil && point.isValidPoint() {
			// if point.Orientation == Orientation_None || point.Orientation == orientation {
			// 	return true
			// }
			return point.OrientationAllowed(orientation)
		}
		return false
	}
	if funcTest(this.X-1, this.Y, Orientation_Right) ||
		funcTest(this.X+1, this.Y, Orientation_Left) ||
		funcTest(this.X, this.Y+1, Orientation_Up) ||
		funcTest(this.X, this.Y-1, Orientation_Down) {
		return true
	}
	return false
}
func (this *Point) IsShelfPlaced(shelves ShelfList) bool {
	// return this.ShelfLinked != nil
	if shelves == nil {
		return false
	}
	for _, shelf := range shelves {
		if shelf.IsHere(this.X, this.Y) == true {
			return true
		}
	}
	return false
}

// func (this *Point) IsOccupiedByAGV() bool {
// 	return this.AgvLinked != nil
// }
func (this *Point) IsBanned() bool {
	return this.MapBlockLinked.BlockType == BannedArea
}
func (this *Point) IsBlocked() bool {
	// return this.MapBlockLinked.BlockType == RailwayBlocked
	return this.IsBlockedByAGV
}
func (this *Point) UnBlock(id string) error {
	//必须锁定者本身执行解锁
	if id != this.BlockerID {
		// if id != this.MapBlockLinked.Placer {
		// return NewAgvError(AGV_ERROR_CODE_逻辑异常, ("该点被其它ID锁定，不能解锁"))
		return ErrPointBlockedByOtherAGV
	}
	this.BlockerID = ""
	this.IsBlockedByAGV = false
	DebugOutput(fmt.Sprintf("agv %s 解除 %s 点的锁定", id, this.ToString())+GetFileLocation(), 3)
	return nil
}
func (this *Point) Block(id string) error {
	// 不能重复锁定
	if this.IsBlocked() == true {
		//区分被同一ID锁定，还是已经被其它ID锁定
		if this.BlockerID != id {
			// return NewAgvError(AGV_ERROR_CODE_逻辑异常, ("该点已经被其它ID锁定"))
			return ErrPointBlockedByOtherAGV
		}
	}
	this.IsBlockedByAGV = true
	this.BlockerID = id
	// DebugOutput(fmt.Sprintf("agv %s 锁定 %s 点", id, this.ToString())+GetFileLocation(), 3)
	return nil
}
func (this *Point) isValidPoint() bool {
	return this.X > 0 && this.Y > 0
}

//-------------------------------------------------------------------

type PointPointerList []*Point

func (this PointPointerList) printPoints() {
	fmt.Println("list => ")
	for _, pointTemp := range this {
		fmt.Println(pointTemp.ToString())
	}
}
func (this PointPointerList) GetPoint(x, y int) *Point {
	for i, point := range this {
		if point.X == x && point.Y == y {
			return ((this)[i])
		}
	}
	return nil
}
func (this PointPointerList) NotblockedByID(id string, x, y int) (bool, error) {
	point := this.GetPoint(x, y)
	if point == nil {
		// return false, NewLogicError(fmt.Sprintf("AGV %s 要被解除封锁的点(%d, %d)不存在", id, x, y))
		return false, ErrNoPointOnMap
	}
	if point.IsBlocked() && point.BlockerID != id {
		return true, nil //被其余AGV封锁
	}
	return false, nil
}
func (this PointPointerList) UnblockPoint(id string, x, y int) error {
	point := this.GetPoint(x, y)
	if point == nil {
		return ErrNoPointOnMap
		// return NewLogicError(fmt.Sprintf("AGV %s 要被解除封锁的点(%d, %d)不存在", id, x, y))
	}
	return point.UnBlock(id)
}
func (this PointPointerList) BlockPoint(id string, x, y int) error {
	point := this.GetPoint(x, y)
	if point == nil {
		// return NewLogicError(fmt.Sprintf("AGV %s 要被封锁的点(%d, %d)不存在", id, x, y))
		return ErrNoPointOnMap
	}
	return point.Block(id)
}
func (this PointPointerList) UnblockPathPoint(id string) error {
	for _, pointTemp := range this {
		if pointTemp.MapBlockLinked.Placer == id {
			if err := pointTemp.UnBlock(id); err != nil {
				return err
			}
		}
	}
	return nil
}

//封闭要行走占用的路径，不允许其它agv使用
func (this PointPointerList) BlockPathPoint(id string) error {
	for _, pointTemp := range this {
		if err := pointTemp.Block(id); err != nil {
			return err
		}
	}
	return nil
}

func (this PointPointerList) ClearFindingFootPrint() {
	for _, pointTemp := range this {
		pointTemp.fatherPoint = nil
		pointTemp.linkEndList = nil
		pointTemp.existsDistance = 0
		pointTemp.isStartPoint = false
	}
}

func (this PointPointerList) FindPointByTypeAndName(pointType WarehousePointType, name string) *Point {
	for _, pointTemp := range this {
		if pointTemp.MapBlockLinked.BlockType == pointType && pointTemp.MapBlockLinked.Placer == name {
			return pointTemp
		}
	}
	return nil
}

// func (this PointPointerList) AddShelfToCalCenter(shelves ShelfList) {
// 	for _, shelfTemp := range shelves {
// 		for _, pointTemp := range this {
// 			if shelfTemp.X == pointTemp.X && pointTemp.Y == shelfTemp.Y {
// 				// pointTemp.ShelfLinked = shelfTemp
// 				pointTemp.PlacedShelf(shelfTemp)
// 				break
// 			}
// 		}
// 	}
// }

//必须是路径点才能正常运行，终点索引为0
func (this PointPointerList) GetUntouchedPoints(index int) PointPointerList {
	return this[0:index]
}
