package agvScheduler

import (
// "bufio"
// "fmt"
// "github.com/astaxie/beego"
// "net"
// "strings"
// "encoding/json"
// "regexp"
// "strings"
// "time"
// "errors"
)

type MapBlock struct {
	Placer    string
	BlockType WarehousePointType
}

//地图上的MapBlock类型
const (
	Railway        WarehousePointType = 1 //agv运行区域
	ShelfPlace     WarehousePointType = 2 //货架区域
	BannedArea     WarehousePointType = 3 //禁行区域
	RailwayBlocked WarehousePointType = 4 //agv运行区域，但是因为已有agv运行在改点，所以暂时封闭
	PickupPlace    WarehousePointType = 5 //agv运行区域，但是因为已有agv运行在改点，所以暂时封闭
	RechargingArea WarehousePointType = 6 //充电区
)

func NewMapBlock(placer string, blockType WarehousePointType) *MapBlock {
	return &MapBlock{
		Placer:    placer,
		BlockType: blockType,
	}
}
