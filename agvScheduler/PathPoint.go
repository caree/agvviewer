package agvScheduler

import (
	"fmt"
	// "strings"
	"strconv"
	// "time"
)

func NewPathPoint(x, y int) *PathPoint {
	guid := GetGUID()
	return &PathPoint{
		X:    x,
		Y:    y,
		GUID: guid,
	}
}

type PathPoint struct {
	X    int
	Y    int
	GUID int64
}
type PathPointList []*PathPoint

func ConvertPointListToPathPointList(list PointPointerList) PathPointList {
	l := PathPointList{}
	//路径是从后往前的，但是计算完成的路径是从前往后
	for i := len(list) - 1; i >= 0; i-- {
		l = append(l, NewPathPoint(list[i].X, list[i].Y))
	}
	// for _, pointer := range list {
	// 	l = append(l, NewPathPoint(pointer.X, pointer.Y))
	// }
	return l
}

func (this PathPointList) FormatPathToTransfer() string {
	str1 := "{"
	for _, pointTemp := range this {
		if len(str1) > 1 {
			str1 += fmt.Sprintf("*(%s %s %s)", strconv.Itoa(pointTemp.X), strconv.Itoa(pointTemp.Y), strconv.FormatInt(pointTemp.GUID, 10))

		} else {
			str1 += fmt.Sprintf("(%s %s %s)", strconv.Itoa(pointTemp.X), strconv.Itoa(pointTemp.Y), strconv.FormatInt(pointTemp.GUID, 10))
		}
	}
	return str1 + "}"
}

func (this PathPointList) FormatPath() string {
	str1 := ""
	for _, pointTemp := range this {
		str1 += fmt.Sprintf(" [(%d,%d),%d] =>", pointTemp.X, pointTemp.Y, pointTemp.GUID)
		// if i > 2 {
		// 	return str1
		// }
	}
	return str1
}

//路径的比较
//路径总是在不断的缩短，所以可能新路径比之前的路径要短，但也任务相同
func (this PathPointList) CompareTo(newPath PathPointList) bool {
	// DebugOutput(this.FormatPath(), 3)
	// DebugOutput(newPath.FormatPath(), 3)
	if newPath == nil {
		return false
	}
	if len(this) <= 0 && len(newPath) <= 0 {
		return true
	}
	if len(this) <= 0 || len(newPath) <= 0 {
		return false
	}
	//如果最后一个点的GUID不同，说明两条路径不同。路径不做删除点的操作，只会增加新点，走后一个点guid大，说明是新加入的
	if this[len(this)-1].GUID < newPath[len(newPath)-1].GUID {
		return false
	}
	return true
}

func (this PathPointList) HasPoint(x, y int) bool {
	for _, pathPoint := range this {
		if pathPoint.X == x && pathPoint.Y == y {
			return true
		}
	}
	return false
}

//根据当前的位置，把之前的位置点删除
func (this PathPointList) RemoveExpiredPoints(x, y int) PathPointList {
	// DebugOutput(this.FormatPath()+GetFileLocation(), 3)
	removed := this
	for i, pathPoint := range this {
		if pathPoint.X == x && pathPoint.Y == y {
			removed = this[i:]
			break
		}
	}
	// DebugOutput(removed.FormatPath()+GetFileLocation(), 3)
	return removed
}
