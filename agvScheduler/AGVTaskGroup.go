package agvScheduler

import (
// "errors"
// "fmt"
// "time"
)

type AGVTaskGroup struct {
	GroupID string
	TaskList
}

//先进先出顺序执行
func (this *AGVTaskGroup) AddTask(task *AGVTaskItem) {
	task.GroupID = this.GroupID
	this.TaskList = append(this.TaskList, task)
}

func NewAGVTaskGroup(id string) *AGVTaskGroup {
	return &AGVTaskGroup{
		GroupID:  id,
		TaskList: TaskList{},
	}
}

//-------------------------------------------------------------------

type AGVTaskGroupList []*AGVTaskGroup

func (this AGVTaskGroupList) ExistGroup(id string) bool {
	for _, group := range this {
		if group.GroupID == id {
			return true
		}
	}
	return false
}
func (this AGVTaskGroupList) FindGroupByID(id string) *AGVTaskGroup {
	for _, group := range this {
		if group.GroupID == id {
			return group
		}
	}
	return nil
}
