package agvScheduler

import (
	"fmt"
	// "errors"
)

type ShelfInfo struct {
	ID string
	// ProductID string
	X         int //货架被搬运过程中的位置
	Y         int
	OriginalX int //仓库设计时货架的位置
	OriginalY int
}

func (this *ShelfInfo) String() string {
	return fmt.Sprintf("ID: %-2s X: %-2d Y: %-2d  Original: (%-2d, %-2d)", this.ID, this.X, this.Y, this.OriginalX, this.OriginalY)
}
func (this *ShelfInfo) IsHere(x, y int) bool {
	return this.X == x && this.Y == y
}
func (this *ShelfInfo) UpdatePosition(x, y int) {
	this.OriginalX = this.X
	this.X = x
	this.OriginalY = this.Y
	this.Y = y
	DebugOutput(fmt.Sprintf("货架位置变化：%s", this.String())+GetFileLocation(), 3)
}
func NewShelf(id string, x, y int) *ShelfInfo {
	return &ShelfInfo{
		ID:        id,
		X:         x,
		Y:         y,
		OriginalY: y,
		OriginalX: x,
	}
}

type ShelfList []*ShelfInfo

func (this ShelfList) String() []string {
	strs := []string{}
	for _, shelfTemp := range this {
		strs = append(strs, shelfTemp.String())
	}
	// return fmt.Sprintf("货架列表: %s", str)
	return strs
}
func (this ShelfList) IsAnyShelfHere(x, y int) bool {
	if len(this) <= 0 {
		return false
	}
	for _, shelf := range this {
		if shelf.IsHere(x, y) == true {
			return true
		}
	}
	return false
}
func (this ShelfList) FindShelfByPosition(x, y int) *ShelfInfo {
	// DebugOutput(fmt.Sprintf("在 %d 个货架中查找货架(%d, %d)", len(this), x, y), 3)
	for _, shelfTemp := range this {
		if shelfTemp.X == x && shelfTemp.Y == y {
			// DebugOutput(shelfTemp.String(), 3)
			return shelfTemp
		}
	}
	return nil
}

// func (this ShelfList) FindShelfByID(id string) (*ShelfInfo, error) {
// 	for _, shelfTemp := range this {
// 		if shelfTemp.ID == id {
// 			return shelfTemp, nil
// 		}
// 	}
// 	return nil, NewAgvError(AGV_ERROR_CODE_没有符合条件的数据, "没有符合条件的货架")
// }
func (this ShelfList) ExistShelf(x, y int) *ShelfInfo {
	for _, shelfTemp := range this {
		if shelfTemp.X == x && shelfTemp.Y == y {
			return shelfTemp
		}
	}
	return nil
}
func (this ShelfList) AddShelf(x, y int) ShelfList {
	return append(this, &ShelfInfo{
		// ID: id,
		X: x,
		Y: y,
	})
}
