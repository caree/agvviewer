package agvScheduler

//============================================================================================================

type WarehouseConfig struct {
	ColCount int
	RowCount int
	Points   WarehouseConfigPointList
	// Points   PointPointerList
}
