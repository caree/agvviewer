package agvScheduler

import (
	"database/sql"
	"github.com/coopernurse/gorp"
	_ "github.com/mattn/go-sqlite3"
	// "log"
	"strconv"
	// "time"
	// "errors"
	"fmt"
)

type SysConfig struct {
	ConfigName  string
	ConfigValue string
}

func NewWarehouseConfigPoint(x, y int, blockType WarehousePointType, placer string, orientations string) *WarehouseConfigPoint {
	// if ExistsOrientation(orientation) == false {
	// 	return nil, NewAgvError(AGV_ERROR_CODE_数据异常, "地图方向定义错误")
	// }
	return &WarehouseConfigPoint{
		X:           x,
		Y:           y,
		Placer:      placer,
		BlockType:   int(blockType),
		Orientation: orientations,
	}
}

type WarehouseConfigPoint struct {
	X           int
	Y           int
	Placer      string
	BlockType   int //关联的地图上的点的类型，包括禁止行走、拣选口等类型
	Orientation string
}

func (this *WarehouseConfigPoint) String() string {
	return fmt.Sprintf("X: %d Y: %d Placer: %s BlockType: %d Orientation: %s",
		this.X, this.Y, this.Placer, this.BlockType, this.Orientation)
}

type WarehouseConfigPointList []*WarehouseConfigPoint

func (this WarehouseConfigPointList) GetConfig(x, y int) *WarehouseConfigPoint {
	for _, point := range this {
		if point.X == x && point.Y == y {
			return point
		}
	}
	return nil
}
func (this WarehouseConfigPointList) GetConfigFlag(x, y int) WarehousePointType {
	for _, point := range this {
		if point.X == x && point.Y == y {
			return WarehousePointType(point.BlockType)
		}
	}
	return Railway
}
func (this WarehouseConfigPointList) String() string {
	str := "\r\n"
	for i, point := range this {
		str += fmt.Sprintf("      %d: %s \r\n", i+1, point.String())
	}
	return fmt.Sprintf("配置点列表: %s", str)
}

//==============================================================================================

var g_db *gorp.DbMap

func initSysConfig() (int, int, error) {
	if dbIntialized() == false {
		// return -1, -1, NewLogicError("数据库初始化失败")
		return -1, -1, ErrDbInitFailed
	}
	var rowCount int = DEFAULT_ROWCOUNT
	var colCount int = DEFAULT_COLCOUNT
	var err error = nil

	var configs []SysConfig
	if _, err := g_db.Select(&configs, "select * from sysconfig"); err != nil {
		DebugOutput("获取系统配置项失败"+GetFileLocation(), 1)
		return rowCount, colCount, err
	} else {
		DebugOutput("获取系统配置项："+GetFileLocation(), 2)
		if len(configs) > 0 {
			for _, config := range configs {
				switch config.ConfigName {
				case "RowCount":
					if row, err := strconv.Atoi(config.ConfigValue); err != nil {
						return rowCount, colCount, err
					} else {
						rowCount = row
					}
				case "ColCount":
					if col, err := strconv.Atoi(config.ConfigValue); err != nil {
						return rowCount, colCount, err
					} else {
						colCount = col
						DebugOutput(fmt.Sprintf("默认列数 设置为 %d"+GetFileLocation(), colCount), 3)
					}
				}
			}
		}
	}
	return rowCount, colCount, err

}
func initWarehouseConfigPointsFromDB() (WarehouseConfigPointList, error) {
	// return
	if dbIntialized() == false {
		// return nil, NewLogicError("数据库初始化失败")
		return nil, ErrDbInitFailed
	}

	var points WarehouseConfigPointList
	// var points PointPointerList
	if _, err := g_db.Select(&points, "select * from warehousepointconfig"); err != nil {
		return nil, err
		// DebugOutput("获取仓库配置点失败: "+err.Error()+GetFileLocation(), 1)
	} else {
		return points, nil
		// DebugOutput(fmt.Sprintf("获取仓库配置点 %d 个：", len(points))+GetFileLocation(), 2)

	}

}
func initShelfListFromDB() (ShelfList, error) {
	var shelves ShelfList
	if _, err := g_db.Select(&shelves, "select * from shelves"); err != nil {
		// DebugOutput("获取货架信息失败: "+err.Error()+GetFileLocation(), 1)
		return nil, err
	} else {
		return shelves, nil
		// DebugOutput(fmt.Sprintf("获取到货架信息 %d 个：", len(shelves))+GetFileLocation(), 2)
		// G_shelves = shelves
		// if len(shelves) > 0 {
		// 	DebugOutputStrings(G_shelves.String(), 2)
		// }
	}
}
func UpdateShelfInfoConfig(shelves ShelfList) error {
	_, err := g_db.Exec("delete from shelves")
	if err == nil {
		for _, shelfTemp := range shelves {
			errTemp := g_db.Insert(shelfTemp)
			if errTemp != nil {
				DebugOutput("插入货架信息失败"+GetFileLocation(), 1)
				return errTemp
			}
		}
	} else {
		DebugOutput("删除旧货架信息数据失败"+GetFileLocation(), 1)
		return err
	}
	return nil
}
func UpdateWarehousePointConfig(points WarehouseConfigPointList) error {
	_, err := g_db.Exec("delete from warehousepointconfig")
	if err == nil {
		for _, point := range points {
			// DebugOutput(point.String()+GetFileLocation(), 3)
			errTemp := g_db.Insert(point)
			if errTemp != nil {
				DebugOutput("插入仓库配置点失败"+GetFileLocation(), 1)
				return errTemp
			}
		}
	} else {
		DebugOutput("删除旧仓库配置点数据失败"+GetFileLocation(), 1)
		return err
	}
	return nil
}
func UpdateConfig(configName, value string) error {
	config := SysConfig{
		ConfigName:  configName,
		ConfigValue: value,
	}
	_, err := g_db.Update(&config)
	if err == nil {
		DebugOutput(fmt.Sprintf("%s 更新为 %s", configName, value)+GetFileLocation(), 2)
	}
	return err
}
func mapString2Var(config SysConfig) {
	var err error
	switch config.ConfigName {
	case "RowCount":
		if G_rowCount, err = strconv.Atoi(config.ConfigValue); err != nil {
			DebugOutput(err.Error()+GetFileLocation(), 1)
		} else {
			DebugOutput(fmt.Sprintf("默认行数 设置为 %d"+GetFileLocation(), G_rowCount), 3)
		}
	case "ColCount":
		if G_colCount, err = strconv.Atoi(config.ConfigValue); err != nil {
			DebugOutput(err.Error()+GetFileLocation(), 1)
		} else {
			DebugOutput(fmt.Sprintf("默认列数 设置为 %d"+GetFileLocation(), G_colCount), 3)
		}
	}
}
func dbIntialized() bool {
	if g_db != nil {
		return true
	} else {
		g_db = initDb()
		if g_db == nil {
			return false
		}
	}
	return true
}
func initDb() *gorp.DbMap {
	// connect to db using standard Go database/sql API
	// use whatever database/sql driver you wish
	db, err := sql.Open("sqlite3", "agvdb.db3")
	if err != nil {
		DebugOutput("数据打开出现错误"+GetFileLocation(), 1)
		return nil
	}

	// construct a gorp DbMap
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.SqliteDialect{}}

	// add a table, setting the table name to 'posts' and
	// specifying that the Id property is an auto incrementing PK
	dbmap.AddTableWithName(SysConfig{}, "sysconfig").SetKeys(false, "ConfigName")
	dbmap.AddTableWithName(WarehouseConfigPoint{}, "warehousepointconfig").SetUniqueTogether("X", "Y")
	dbmap.AddTableWithName(ShelfInfo{}, "shelves").SetUniqueTogether("X", "Y")
	// return nil
	// create the table. in a production system you'd generally
	// use a migration tool, or create the tables via scripts
	if err := dbmap.CreateTablesIfNotExists(); err != nil {
		DebugOutput("创建数据库表失败"+GetFileLocation(), 1)
		return nil
	}

	return dbmap
}
