package routers

import (
	"agvViewer/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{}, "get:Index")
	beego.Router("/ControlIndex", &controllers.MainController{}, "get:ControlIndex")

	beego.Router("/ShowWarehouseIndex", &controllers.MainController{}, "get:ShowWarehouseIndex")
	beego.Router("/WarehouseConfigIndex", &controllers.MainController{}, "get:WarehouseConfigIndex")
	beego.Router("/WarehouseConfigData", &controllers.MainController{}, "get:WarehouseConfigData")
	beego.Router("/SetWarehouseConfigData", &controllers.MainController{}, "post:SetWarehouseConfigData")
	beego.Router("/SetWarehouseShelfData", &controllers.MainController{}, "post:SetWarehouseShelfData")

	beego.Router("/AGVListIndex", &controllers.MainController{}, "get:AGVListIndex")
	beego.Router("/AgvList", &controllers.MainController{}, "get:AgvList")

	beego.Router("/ShelfList", &controllers.MainController{}, "get:ShelfList")

	beego.Router("/ProductInfoListIndex", &controllers.MainController{}, "get:ProductInfoListIndex")
	beego.Router("/ProductInfoList", &controllers.MainController{}, "get:ProductInfoList")

	beego.Router("/MoveAGV2XY", &controllers.MainController{}, "get:MoveAGV2XY")
	beego.Router("/SetAGVStatus", &controllers.MainController{}, "get:SetAGVStatus")
	beego.Router("/SetAGVLiftStatus", &controllers.MainController{}, "get:SetAGVLiftStatus")

	beego.Router("/ShowWarehouseIndexws", &controllers.WebSocketController{}, "get:Join")

}
