<!DOCTYPE html>
<html>
<head>
    <title>作业监控</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">
    <link href="/stylesheets/style.css" rel="stylesheet" media="screen">    
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <link rel="stylesheet" href="/bootstrap/css/font-awesome.min.css">
    <style type="text/css">
      body,html {
        margin: 0;
        padding: 0;
        height:100%;
        background: black;
      }
      #lblTip{
        margin-top: 30px; text-align: center; font-size: 30px; color: red; margin-bottom: 20px;
      }
      #dtProcess{
        text-align: center;
      }
      th{
        text-align: center;
      }
      .btn{
        width: 100px;
      }
    </style>        
    <script src="/javascripts/jquery.min.js"></script>
    <script src="/javascripts/underscore.js"></script>
    <script src="/javascripts/easeljs-0.7.1.min.js"></script>
    <script src="/javascripts/cell.js"></script>
    <script src="/javascripts/MapBlock.js"></script>
    <script>
        var wsUri                    = 'ws:'+ window.location.href.substring(window.location.protocol.length) +'ws?guid={{.guid}}';
        
        var warehouseWidth           = 810
        var warehouseHeight          = 540
        var originPointX             = 30
        var originPointY             = 30
        //legend
        var legendLeft               = 900
        var firstLengendTop          = 30
        var lengendTextGap           = 5//图和文字的垂直距离
        var lengendGap               = 70//图例之间的垂直距离
        
        var mainColor                = "rgba(100,100,100, 0.5)"
        var railwayColor             = "rgb(255,255,255)"
        var bannedAreaColor          = "rgba(100,100,100,0.2)"
        var shlefPlaceColor          = "#795548"
        // var shlefPlaceColor       = "#51a5ba"
        var PickupPlaceColor         = "#c1f535"
        var agvRunningColor          = "#fe9d35"
        var agvRunningLiftedColor    = "#rgb(0,0,255)"
        var agvUnderShelfColor       = "#d88b2b"
        var RechargingAreaColor      = "#f56539"
        var agvPausedColor           = "rgb(0,0,255)"
        var agvStoppedColor          = "yellow"
        var agvErrorColor            = "red"
        
        var BlockType_ShowInfo       = 0 //查看
        var BlockType_Railway        = 1 //agv运行区域
        var BlockType_Railway_Right  = 11 //agv运行区域
        var BlockType_Railway_Down   = 12 //agv运行区域
        var BlockType_Railway_Left   = 13 //agv运行区域
        var BlockType_Railway_Up     = 14 //agv运行区域
        var BlockType_ShelfPlace     = 2 //货架区域
        var BlockType_BannedArea     = 3 //禁行区域
        var BlockType_PickupPlace    = 5 //agv运行区域，但是因为已有agv运行在改点，所以暂时封闭
        var BlockType_RechargingArea = 6 //充电区
        
        var AGV_LIFT_STATUS_UNLIFTED = "0"
        var AGV_LIFT_STATUS_LIFTED   = "1"
        
        var cellHeight, cellWidth
        var row                      = 20, col = 20
        
        var warehouseConfig          = null
        var stage                    = null
        var MapBlockList             = null
        var cellList                 = []
		// var shapeAGV
		var x =1 ,y = 1
        function init() {
        	$.get('./WarehouseConfigData', function(data_warehouseConfig){
        		console.log((data_warehouseConfig))

                $.get('../AgvList', function(data_agvList){
                        console.log((data_agvList))

                    $.get('../ShelfList', function(data_shelfList){
                        console.log((data_shelfList))

                        warehouseConfig = data_warehouseConfig
                        row             = warehouseConfig.RowCount
                        col             = warehouseConfig.ColCount
                        //初始化地图上的点
                        cellList        = initialEmptyWarehouseCellList(row, col)
                        MapBlockList    = initialEmptyMapBlocks(row, col)

                        _.each(data_agvList.data, function(agvInfo){
                            var agvTemp = new AGV(agvInfo.AGV.ID, agvInfo.AGV.Status, agvInfo.AGV.LiftStatus, agvInfo.AGV.X, agvInfo.AGV.Y)
                            console.log("agv => %s", (agvTemp))
                            MapBlockList.setAGV(agvTemp)
                        })

                        _.map(data_shelfList.data, function(shelfTemp){
                            var shelfTemp = new Shelf(shelfTemp.X, shelfTemp.Y, shelfTemp.X, shelfTemp.Y)
                            console.log("shelf => %s", (shelfTemp))
                            MapBlockList.setShelf(shelfTemp)
                        })


                        stage = new createjs.Stage("demoCanvas");
                        //计算单元格大小

                        cellHeight = warehouseHeight/row
                        cellWidth = warehouseWidth/col

                        drawGrid(row,col)

                        //根据定义绘制特殊的点
                        if(warehouseConfig.Points != null && warehouseConfig.Points.length > 0){
                            for (var i = 0; i < warehouseConfig.Points.length; i++) {
                                var point = warehouseConfig.Points[i]
                                switch(point.BlockType){
                                    case BlockType_BannedArea:
                                    MapBlockList.setBannedArea(new BannedArea(point.X, point.Y, point.Placer))
                                    break
                                    case BlockType_PickupPlace:
                                    MapBlockList.setPickUpArea(new PickUpArea(point.X, point.Y, point.Placer))
                                    break
                                    case BlockType_RechargingArea:
                                    MapBlockList.setRechargingArea(new RechargingArea(point.X, point.Y, point.Placer))
                                    break
                                    case BlockType_Railway:
                                    MapBlockList.setRailwayArea(new RailwayArea(point.X, point.Y, ""))
                                    // switch(point.Orientation){
                                    //     case "right":
                                    //     MapBlockList.setRailwayArea(new RailwayArea(point.X, point.Y, "→"))
                                    //     break
                                    //     case "left":
                                    //     MapBlockList.setRailwayArea(new RailwayArea(point.X, point.Y, "←"))
                                    //     break
                                    //     case "up":
                                    //     MapBlockList.setRailwayArea(new RailwayArea(point.X, point.Y, "↑"))
                                    //     break
                                    //     case "down":
                                    //     MapBlockList.setRailwayArea(new RailwayArea(point.X, point.Y, "↓"))
                                    //     break
                                    // }    
                                    break 
                                }
                            };
                        }
                        //绘制图例
                        drawLegend()
                        //绘制元素，主要是AGV，货架和禁行区域
                        _.each(MapBlockList.Blocks, function(mapBlock){
                            // mapBlock.drawBlock()
                            var combine = mapBlock.getDrawingContent()
                            updateCellContent(mapBlock.X, mapBlock.Y, combine)
                        })
                        fillWarehouseCell(cellList)
                        updateStage()  

                        createWebSocket()
                        setInterval(checkConnectionStatus, 3000)                             
                    })
                })
        	})
        }
        function updateCellContent(x, y, colorTxtCombination){
            var cellTemp = _.findWhere(cellList, {X: x, Y: y})
            console.assert(cellTemp!=null, "系统错误")
            cellTemp.updateColorText(colorTxtCombination.Color, colorTxtCombination.Text)
            cellTemp.redrawCell()
        }
        function fillWarehouseCell(cellDataList){
            _.each(cellDataList, function(cell){
                cell.redrawCell()
                // cell.graphicsShape.addEventListener("click", clickCellHandlerCreator(cell.X, cell.Y));
            })            
        }
        function initialEmptyMapBlocks(row, col){
            var list = new MapBlocks()
            for (var i = 1; i <= col; i++) {
                for(var j = 1; j<= row; j++){
                    list.addMapBlock(i, j)
                    list.setRailwayArea(new RailwayArea(i, j, ""))
                }
            };
            return list
        }
        function initialEmptyWarehouseCellList(row, col){
            var cellDataList = []
            //创建坐标列表
            for (var i = 1; i <= row; i++) {
                for (var j = 1; j <= col; j++) {
                    cellDataList.push(new Cell(j, i, railwayColor))
                };
            };
            return cellDataList
        }
        function drawLegend(){
            var lengendWidth = 50
            var lengendHeight = 25
            var draw = function(lengendGapCount, color, txt, clickHandler, rotationTxt){
                var rect = drawRectangle(color, legendLeft, firstLengendTop + lengendGap * lengendGapCount, lengendWidth,lengendHeight)
                if(rotationTxt != null){
                    drawText(rotationTxt, legendLeft, firstLengendTop + lengendGap * lengendGapCount, "#000", 0, lengendWidth, lengendHeight, 16)
                }
                // rect.addEventListener("click", clickHandler);
                drawText(txt, legendLeft, firstLengendTop + lengendGap * lengendGapCount + lengendHeight + lengendTextGap, mainColor, 0, lengendWidth, -1, 14)
            }

            draw(0, agvRunningColor, "AGV")
            draw(1, shlefPlaceColor, "货架")
            draw(2, PickupPlaceColor, "拣选口")
            draw(3, RechargingAreaColor, "充电口")
            return
        }

        function refreshAGVInfo(agv){
            // return
            var agvTemp = new AGV(agv.ID, agv.Status, agv.LiftStatus, agv.X, agv.Y)
            var blocksTempAGV = MapBlockList.updateBlockAGV(agvTemp)

            var blocksTempShelf = null
            if(agv.Shelf != null){
                shelfTemp = new Shelf(agv.Shelf.X, agv.Shelf.Y, agv.Shelf.OriginalX, agv.Shelf.OriginalY)
                MapBlockList.updateBlockShelf(shelfTemp)
            }else{
                console.info("agv %s 没有货架", agv.ID)
            }

            updateStage()
        }

        function onMessage(evt) {
            console.log("onMessage => " + (evt.data))
            // console.log("onMessage => " + JSON.stringify(evt.data))
            // return
            // writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
            var cmd = JSON.parse(evt.data);
            switch(cmd.Type){
                case 1://EVENT_AGV_HEARTBEATING
                case 2://EVENT_POSITION_NEW
                case 3://EVENT_STATUS_NEW
                case 4://EVENT_LIFT_STATUS_NEW
                    refreshAGVInfo(cmd.AGV)
                    break
            }
        }

        function updateStage(){
			stage.update();
        }

        function drawAGV_IDOnGrid(txt, gridX, gridY, rotation, graphicsText){
            var graphicsTextTemp = graphicsText
            if(graphicsTextTemp == null){
                graphicsTextTemp = drawText(txt, x, y, mainColor, rotation)
            }else{
                graphicsTextTemp.text = txt
            }
            var paddingLeft = (cellWidth - graphicsTextTemp.getMeasuredWidth()) / 2
            var paddingTop = (cellHeight - graphicsTextTemp.getMeasuredHeight()) / 2
            if(paddingLeft < 0) paddingLeft = 0
            if(paddingTop < 0) paddingTop = 0
            var x = originPointX + (gridX - 1) * cellWidth + paddingLeft
            var y = originPointY + (gridY - 1) * cellHeight + paddingTop
            graphicsTextTemp.x = x
            graphicsTextTemp.y = y
            return graphicsTextTemp
        }
        function fillCellOnGrid(color, gridX, gridY, rect){
            var x = originPointX + (gridX - 1) * cellWidth + 1
            var y = originPointY + (gridY - 1) * cellHeight + 1
            return drawRectangle(color, x, y, cellWidth - 2, cellHeight - 2, rect)
        }        
        function drawGrid(row, col){
        	for (var i = 0; i <= row; i++) {
        		var y = cellHeight*i + originPointY
        		drawLine(originPointX, y, warehouseWidth + originPointX, y)
        	};
        	for (var i = 0; i <= col; i++) {
        		var x = cellWidth*i + originPointX
        		drawLine(x, originPointY, x, warehouseHeight + originPointY)
        	};
        	//坐标指示
        	for (var i = 0; i < col; i++) {
        		var x = cellWidth*i + originPointX + cellWidth/2 - 7
	        	drawText(i+1, x, originPointY -20, mainColor, 0)//x 上坐标
	        	drawText(i+1, x, originPointY + warehouseHeight, mainColor, 0)//x 下坐标
        	};
        	for (var i = 0; i < row; i++) {
        		var y = cellHeight*i + originPointY 
	        	drawText(i+1, originPointX - cellWidth + 5, y, mainColor, 0, cellWidth, cellHeight)//y 左坐标
	        	drawText(i+1, originPointX + warehouseWidth - 5 , y , mainColor, 0, cellWidth, cellHeight)//y 右坐标
        	};
        }
		function drawRectangle(color, xLeft, yTop, width, height, rect){
            if(rect == null){
    			 var rectTemp = new createjs.Shape();
                 rectTemp.graphics.beginFill(color).drawRect(xLeft, yTop, width, height);
                 stage.addChild(rectTemp);
                 return rectTemp
            }else{
    			 rect.graphics.beginFill(color).drawRect(xLeft, yTop, width, height);
                 return rect
            }
		}        
        function drawLine(fromX, fromY, toX, toY){
			var line = new createjs.Shape();
			line.graphics.moveTo(fromX,fromY).setStrokeStyle(1).beginStroke(mainColor).lineTo(toX,toY);
			stage.addChild(line);
            return line
        }        
        function drawText(txt,x,y,color,rotation, spaceWidth, spaceHeight){
            // console.log("drawText => " + txt)
            // console.log("spaceWidth = " + spaceWidth + " | spaceHeight = " + spaceHeight)
            var theText = new createjs.Text("hmmm","bold 14px Arial","");
            theText.text = txt;
            theText.x = x;
            theText.y = y;
            if(spaceWidth > 0){
                var txtWidth = theText.getMeasuredWidth()
                // console.log("txtWidth = " + txtWidth + "  |  spaceWidth = " + spaceWidth)
                if(spaceWidth > txtWidth){
                    var padding = (spaceWidth - txtWidth) / 2
                    theText.x += padding
                }
            }
            if(spaceHeight > 0){
                var txtHeight = theText.getMeasuredHeight()
                if(spaceHeight > txtHeight){
                    var padding = (spaceHeight - txtHeight) /2
                    theText.y += padding
                }
            }
	        theText.rotation = rotation
	        theText.alpha = 1;
	        theText.color = color
	        stage.addChild(theText);
            return theText
        }
        function createWebSocket() {
            websocket = new WebSocket(wsUri);
            websocket.onopen = function (evt) { onOpen(evt) };
            websocket.onclose = function (evt) { onClose(evt) };
            websocket.onmessage = function (evt) { onMessage(evt) };
            websocket.onerror = function (evt) { onError(evt) };
        }
        function onOpen(evt) {
            // writeToScreen("已连接到服务器");
            // var flag = $("#inputStartFlag").val()
            console.log("已连接到服务器")
            // doSend(flag)
        }
        function onClose(evt) {
            // writeToScreen("连接已断开");
            console.log("连接已断开")
            websocket=null
        }

        function onError(evt) {
            websocket=null
        }
        function doSend(message) {
            console.log("SENT: " + message)
            websocket.send(message);
        }        
        function checkConnectionStatus(){
          if(websocket == null){
            // createWebSocket()
            myrefresh()
          }
        }             
		function myrefresh(){
			window.location.reload();
		}

    </script>
</head>
<body onLoad="init();" style="background-color: rgb(200,200,200);text-align: center;">

    <div id = "divNav">
        <div class="container">
            <div class="row"  id = "">
                <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1">

                    <img id="imglogo" src="/images/syslogo.png">
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-md-lg-11">
                    <div style="">                        
                         <a class= "navFunc" style="right:6px;" onclick = "openAboutWindow()">关于 </a>
                     </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" id = "subNavBar" style="">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                <div id="lblTableTitleText" style=""> 
                  <span class="glyphicon glyphicon-arrow-right" style="margin-right: 3px;"></span>
                  <span style="font-size: 16px;"> 作业监控 </span></div>
            </div>
         </div>
     </div>    

    <div class="container" id= "mainContainer">
        <canvas id="demoCanvas" width="1000" height="650" style="background-color: rgb(255,255,255);"> </canvas>
    </div>

    <div class="container" style ="margin-top:30px">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" id = "tipContainer" style="margin-bottom: 0px;">
            <div> 
                <span class="icon-pushpin" style="margin-right: 3px;"></span>
                <span style=""> 查看AGV作业情况 </span>
            </div>
          </div>
        </div>
    </div>  

    <!-- Modal -->
        <script language="javascript" type="text/javascript">
            function openAboutWindow(){
              $('#modalAbout').modal();
            }
        </script>
        <div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header" style="background-color: #00695c; color: white;">
                <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">关于</h4>
              </div>
              <div class="modal-body">
                <div style="text-align: center; font-size: 19px; margin-top: 20px;margin-bottom:20px;">
                   {{.aboutInfo}}
                </div>
              </div>
              <div class="modal-footer" style="text-align: center;">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
              </div>
            </div>
          </div>
        </div>
    <!-- Modal End-->    
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script src="/dataTable/jquery.dataTables.js"></script>
       

</body>
</html>