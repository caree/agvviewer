<!DOCTYPE html>
<html>
<head>
    <title>仓库设计</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">
    <link href="/stylesheets/style.css" rel="stylesheet" media="screen">    
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <style type="text/css">
      body,html {
        margin: 0;
        padding: 0;
        height:100%;
        background: black;
      }
      #lblTip{
        margin-top: 30px; text-align: center; font-size: 30px; color: red; margin-bottom: 20px;
      }
      #dtProcess{
        text-align: center;
      }
      th{
        text-align: center;
      }
      .btn{
        width: 100px;
      }
    </style>    
    <script src="/javascripts/jquery.min.js"></script>
    <!-- // <script src="/javascripts/underscore.min.js"></script> -->
    <script src="/javascripts/underscore.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/javascripts/easeljs-0.7.1.min.js"></script>
    <script src="/javascripts/cell.js"></script>
    <script>

        var warehouseWidth               = 810
        var warehouseHeight              = 600
        var originPointX                 = 30
        var originPointY                 = 30
        //legend
        var legendLeft                   = 900
        var firstLengendTop              = 30
        var lengendTextGap               = 5//图和文字的垂直距离
        var lengendGap                   = 50//图例之间的垂直距离
        
        
        var mainColor                    = "rgba(100,100,100, 1)"
        var bannedAreaColor              = "rgba(100,100,100,0.2)"
        var shlefPlaceColor              = "#795548"
        var PickupPlaceColor             = "#c1f535"
        var railwayColor                 = "#ade7ad"
        var RechargingAreaColor          = "#f56539"
        
        
        var Cmd_ShowInfo                 = 0 //查看
        var Cmd_BlockType_Railway        = 1 //agv运行区域
        var Cmd_BlockType_ShelfPlace     = 2 //货架区域
        var Cmd_BlockType_BannedArea     = 3 //禁行区域
        var Cmd_BlockType_PickupPlace    = 5 //agv运行区域，但是因为已有agv运行在改点，所以暂时封闭
        var Cmd_BlockType_RechargingArea = 6 //充电区
        var Cmd_ClearOrientation         = 7 //消除方向
        
        var Cmd_AddOrientation_Left      = 8 //agv运行区域
        var Cmd_AddOrientation_Up        = 9 //agv运行区域
        var Cmd_AddOrientation_Down      = 10 //agv运行区域
        var Cmd_AddOrientation_Right     = 11 //agv运行区域
        
        var OrientationCmdSeries         = [Cmd_ClearOrientation, Cmd_AddOrientation_Left, Cmd_AddOrientation_Up, Cmd_AddOrientation_Down, Cmd_AddOrientation_Right]
        var ChangeBlockTypeCmdSeries     = [Cmd_BlockType_ShelfPlace, Cmd_BlockType_BannedArea, Cmd_BlockType_Railway, Cmd_BlockType_PickupPlace, Cmd_BlockType_RechargingArea]
        var BlockTypeWithIDSeries        = [Cmd_BlockType_PickupPlace, Cmd_BlockType_RechargingArea]
        var row                          = 20
        var col                          = 20
        var warehouseConfig
        var stage 
        var warehouseBaseRect
        var cellHeight, cellWidth
        var cellList                     = []
        // var shelfList                 = [];
        var currentSelectedLengend       = Cmd_ShowInfo
        var lengendColorMap              = []
        var gridLineShapeList            = []//存储表格线，重新绘制的时候从画板里删除
        var gridTextList                 = []//存储表格坐标文字，重新绘制的时候从画板里删除


        function init() {
            stage = new createjs.Stage("demoCanvas");

            $.get('./WarehouseConfigData', function(data) {
                console.log(JSON.stringify(data))
                $.get('../ShelfList', function(data_shelfList) {
                    console.log(data_shelfList)
                    warehouseConfig = data
                    _.each(data_shelfList.data, function(shelf) {
                        var point = _.findWhere(warehouseConfig.Points, {
                            X: shelf.X,
                            Y: shelf.Y
                        })
                        point.ID = shelf.ID
                        // var configPoint = new ConfigPoint(shelf.X, shelf.Y, Cmd_BlockType_ShelfPlace)
                        // configPoint.setID(shelf.ID)
                        // warehouseConfig.Points.push(configPoint)
                    })
                    row = warehouseConfig.RowCount
                    col = warehouseConfig.ColCount
                    //将已已定义的行列数赋值到输入框中
                    $('#numberRow').val(row)
                    $('#numberCol').val(col)
                    redrawWarehouse(row, col)
                })
            })
        }
        function removeExtraConfigPoint(row, col, configPoints) {
            return _.filter(configPoints, function(point) {
                return point.X <= col && point.Y <= row
            })
        }
        function addPointConfig(x, y, cmd) {
            // if(cmd == Cmd_BlockType_Railway){
            //     //实际是删除配置点
            //     warehouseConfig.Points = _.filter(warehouseConfig.Points, function(point){
            //         return !(point.X == x && point.Y == y)
            //     })
            //     return null              
            // }
            var pointTemp = _.findWhere(warehouseConfig.Points, {
                X: x,
                Y: y
            })
            if (_.contains(ChangeBlockTypeCmdSeries, cmd) == true) { //改变该点的类型
                if (pointTemp == null) {
                    console.info("添加了新配置点 (%d, %d) %d", x, y, cmd)
                    var newPoint = new ConfigPoint(x, y, cmd)
                    warehouseConfig.Points.push(newPoint)
                    return newPoint
                } else {
                    pointTemp.updateBlockType(cmd)
                    return pointTemp
                }
            } else if (_.contains(OrientationCmdSeries, cmd) == true) { //改变该点的方向性
                if (pointTemp == null) { //之前没有该点
                    console.info("没有配置点，无法消除方向")
                    // return null
                    pointTemp = new ConfigPoint(x, y, Cmd_BlockType_Railway)
                    warehouseConfig.Points.push(pointTemp)
                }
                pointTemp.updateOrientation(cmd)
                return pointTemp
            }

        }

        function redrawWarehouse(newRow, newCol){
            row = newRow
            col = newCol
            //刷新数据元素
            cellList = initialEmptyWarehouseCellList(row, col)
            warehouseConfig.Points = removeExtraConfigPoint(row, col, warehouseConfig.Points)

            getOrientations = function(orientation){
                var list = []
                if(orientation.indexOf("left") >= 0){
                    list.push(Cmd_AddOrientation_Left)
                }
                if(orientation.indexOf("right") >= 0){
                    list.push(Cmd_AddOrientation_Right)
                }
                if(orientation.indexOf("up") >= 0){
                    list.push(Cmd_AddOrientation_Up)
                }
                if(orientation.indexOf("down") >= 0){
                    list.push(Cmd_AddOrientation_Down)
                }

                return list
            }
            warehouseConfig.Points = _.map(warehouseConfig.Points, function(point){
                var c = new ConfigPoint(point.X, point.Y, point.BlockType)
                c.setID(point.Placer)
                c.Orientations = getOrientations(point.Orientation)
                return c
            })
            // _.each(warehouseConfig.Points, function(point){
            //     point.Orientations = getOrientations(point.orientation)
            // })
            cellList = updateWarehouseCellData(cellList, warehouseConfig.Points)

            console.info("row = %d col = %d cell count = %d", newRow, newCol, _.size(cellList))

            cellHeight = warehouseHeight/row
            cellWidth = warehouseWidth/col

            stage.removeAllChildren()
            gridLineShapeList = drawGrid(row, col)
            gridTextList = drawGridIndexNotation(row, col)
            fillWarehouseCell(cellList)
            drawLegend()
            updateStage()
            warehouseConfig.ColCount = col
            warehouseConfig.RowCount = row            
        }
        function removeGridFromStage(){
            _.each(gridLineShapeList, function(shape){
                shape.graphics.clear()
                stage.removeChild(shape)
            })
            _.each(gridTextList, function(text){
                stage.removeChild(text)
            })
            stage.removeChild(warehouseBaseRect)
            warehouseBaseRect = new createjs.Shape();
            warehouseBaseRect.graphics.beginFill("rgb(255,255,255)").drawRect(originPointX, originPointY, warehouseWidth, warehouseHeight);
            stage.addChild(warehouseBaseRect);            
        }
        function initialEmptyWarehouseCellList(row, col){
            var cellDataList = []
            //创建坐标列表
            for (var i = 1; i <= row; i++) {
                for (var j = 1; j <= col; j++) {
                    cellDataList.push(new Cell(j, i, railwayColor))
                };
            };
            return cellDataList
        }
        function getBlockTypeColor(blockType){
            var col = railwayColor
            switch(blockType){
                case Cmd_BlockType_Railway:
                col = railwayColor
                break
                case Cmd_BlockType_ShelfPlace:
                col = shlefPlaceColor
                break
                case Cmd_BlockType_BannedArea:
                col = bannedAreaColor
                break
                case Cmd_BlockType_PickupPlace:
                col = PickupPlaceColor
                break
                case Cmd_BlockType_RechargingArea:
                col = RechargingAreaColor
                break
            }   
            return col         
        }
        function getBlockText(blockType){
            var str = ""
            switch(blockType){
                case Cmd_BlockType_Railway_Right:
                str = "→"
                break
                case Cmd_BlockType_Railway_Down:
                str = "↓"
                break
                case Cmd_BlockType_Railway_Left:
                str = "←"
                break
                case Cmd_BlockType_Railway_Up:
                str = "↑"
                break
            }
            return str
        }
        function updateWarehouseCellData(cellDataList, configPoints){
            if(cellDataList == null || cellDataList == undefined){
                cellDataList = []
                return
            }
            var funcGetText = function(point){
                var str = ""
                switch(point.BlockType){
                    case Cmd_BlockType_Railway:
                    switch(point.Orientation){
                        case "right":
                        str = "→"
                        break
                        case "left":
                        str = "←"
                        break
                        case "up":
                        str = "↑"
                        break
                        case "down":
                        str = "↓"
                        break
                    }
                    break
                    // default:
                    // str = point.Placer
                    // break
                }
                return str                
            }
            //根据定义绘制特殊的点
            if(configPoints != null && configPoints.length > 0){
                _.each(configPoints, function(point){
                    var pointTempInList = _.findWhere(cellDataList, {X: point.X, Y: point.Y})
                    if(pointTempInList != null){
                        pointTempInList.updateColorText(getBlockTypeColor(point.BlockType), point.getBlockText())
                        // pointTempInList.updateColorText(getBlockTypeColor(point.BlockType), funcGetText(point))
                    }
                })
            }
            return cellDataList
        }
        function fillWarehouseCell(cellDataList){
            _.each(cellDataList, function(cell){
                cell.redrawCell()
                cell.graphicsShape.addEventListener("click", clickCellHandlerCreator(cell.X, cell.Y));
            })            
        }
        function drawLegend(){
            var lengendWidth = 50
            var lengendHeight = 35
            var draw = function(lengendGapCount, color, txt, clickHandler, rotationTxt){
                var rect = drawRectangle(color, legendLeft, firstLengendTop + lengendGap * lengendGapCount, lengendWidth,lengendHeight)
                if(rotationTxt != null){
                    drawText(rotationTxt, legendLeft, firstLengendTop + lengendGap * lengendGapCount, "#000", 0, lengendWidth, lengendHeight, 16)
                }
                rect.addEventListener("click", clickHandler);
                drawText(txt, legendLeft + lengendWidth + 8, firstLengendTop + lengendGap * lengendGapCount , mainColor, 0, -1, lengendHeight, 14)
                // drawText(txt, legendLeft, firstLengendTop + lengendGap * lengendGapCount + lengendHeight + lengendTextGap, mainColor, 0, lengendWidth, -1, 14)
            }
            draw(0, mainColor, "选择查看", clickLegendHandlerCreator(Cmd_ShowInfo))
            draw(1, shlefPlaceColor, "货架", clickLegendHandlerCreator(Cmd_BlockType_ShelfPlace))
            draw(2, PickupPlaceColor, "拣选口", clickLegendHandlerCreator(Cmd_BlockType_PickupPlace))
            draw(3, RechargingAreaColor, "充电区", clickLegendHandlerCreator(Cmd_BlockType_RechargingArea))
            draw(4, bannedAreaColor, "禁区", clickLegendHandlerCreator(Cmd_BlockType_BannedArea))
            draw(5, railwayColor, "运行区", clickLegendHandlerCreator(Cmd_BlockType_Railway))
            draw(6, railwayColor, "右单向运行区", clickLegendHandlerCreator(Cmd_AddOrientation_Right), "→")
            draw(7, railwayColor, "下单向运行区", clickLegendHandlerCreator(Cmd_AddOrientation_Down), "↓")
            draw(8, railwayColor, "左单向运行区", clickLegendHandlerCreator(Cmd_AddOrientation_Left), "←")
            draw(9, railwayColor, "上单向运行区", clickLegendHandlerCreator(Cmd_AddOrientation_Up), "↑")
            draw(10, railwayColor, "消除方向", clickLegendHandlerCreator(Cmd_ClearOrientation), "×")
            return
        }
        function clickCellHandlerCreator(x, y){
            return function(){
                console.log("cell clicked (%d, %d)", x, y)
                console.info("cell(%d, %d) changed to %d",x, y, currentSelectedLengend)
                var pointTempInList = _.findWhere(cellList, {X: x, Y: y})
                console.assert(pointTempInList!= null, "程序异常")
                switch(currentSelectedLengend){
                    case Cmd_ShowInfo:
                        selectCellInfo(x, y)
                    break
                    default:
                        var pointConfig = addPointConfig(x, y, currentSelectedLengend)
                        var txt = ""
                        if(pointConfig != null){
                            txt = pointConfig.getBlockText()
                        }
                        pointTempInList.updateColorText(getBlockTypeColor(pointConfig.getBlockType()), txt)
                        pointTempInList.redrawCell()

                        updateStage()  
                    break
                }


                // if(pointTempInList != null){
                //     if(currentSelectedLengend == Cmd_ShowInfo){//查看配置点信息状态
                //         selectCellInfo(x, y)
                //     }else{//添加配置点
                //         var pointConfig = addPointConfig(x, y, currentSelectedLengend)
                //         var txt = ""
                //         if(pointConfig != null){
                //             txt = pointConfig.getBlockText()
                //         }
                //         pointTempInList.updateColorText(getBlockTypeColor(currentSelectedLengend), txt)
                //         // pointTempInList.updateColorText(getBlockTypeColor(currentSelectedLengend), getBlockText(currentSelectedLengend))
                //         pointTempInList.redrawCell()

                //         updateStage()                        
                //     }
                // }                
            }
        }
        function selectCellInfo(x, y){
            //显示该单元格信息
            var configPoint = _.findWhere(warehouseConfig.Points, {X: x, Y: y})
            if(configPoint == null){//如果不是配置点，则有可能是货架
                console.info("该点（%d, %d）没有定义特殊点", x, y)
                setSpanXY(x, y, "", false)
            }else{
                console.log(configPoint)
                if(_.contains(BlockTypeWithIDSeries, configPoint.BlockType) == true){
                    setSpanXY(configPoint.X, configPoint.Y, configPoint.Placer, true)
                    if(configPoint.Placer.length <= 0){
                        console.info("配置点(%d, %d) 编号为空", configPoint.X, configPoint.Y)
                    }else{
                        console.info("配置点(%d, %d) 编号为 %s", configPoint.X, configPoint.Y, configPoint.Placer)
                    }
                }else{
                    setSpanXY(configPoint.X, configPoint.Y, configPoint.Placer, false)
                }
            }            
        }
        function clickLegendHandlerCreator(lengend){
            var handler = function(){
                console.log("lengend clicked " + lengend)
                currentSelectedLengend = lengend
            }
            return handler
        }
        function updateStage(){
			stage.update();
        }
        function fillCellOnGrid(color, gridX, gridY, rect){
			var x = originPointX + (gridX - 1) * cellWidth + 1
			var y = originPointY + (gridY - 1) * cellHeight + 1
			return drawRectangle(color, x, y, cellWidth -2 , cellHeight -2, rect)
        }
        function drawGrid(row, col){
            var list = []
        	for (var i = 0; i <= row; i++) {
        		var y = cellHeight*i + originPointY
        		list.push(drawLine(originPointX, y, warehouseWidth + originPointX, y))
        	};
        	for (var i = 0; i <= col; i++) {
        		var x = cellWidth*i + originPointX
        		list.push(drawLine(x, originPointY, x, warehouseHeight + originPointY))
        	};
            return list
        }
        function drawGridIndexNotation(row, col){
            list = []
            //坐标指示
            for (var i = 0; i < col; i++) {
                var x = cellWidth*i + originPointX + cellWidth/2 - 7
                list.push(drawText(i+1, x, originPointY -20, mainColor, 0, 0, 0, 14))//x 上坐标
                list.push(drawText(i+1, x, originPointY + warehouseHeight, mainColor, 0, 0, 0, 14))//x 下坐标
            };
            for (var i = 0; i < row; i++) {
                var y = cellHeight*i + originPointY 
                list.push(drawText(i+1, 0, y, mainColor, 0, originPointX, cellHeight, 14))//y 左坐标
                list.push(drawText(i+1, originPointX + warehouseWidth , y , mainColor, 0, originPointX, cellHeight, 14))//y 右坐标
            };            
            return list
        }
		function drawRectangle(color, xLeft, yTop, width, height, rect){
            if(rect == null){
    			 var rectTemp = new createjs.Shape();
                 rectTemp.graphics.beginFill(color).drawRect(xLeft, yTop, width, height);
                 stage.addChild(rectTemp);
                 return rectTemp
            }else{
    			 rect.graphics.beginFill(color).drawRect(xLeft, yTop, width, height);
                 return rect
            }
		}        
        function drawLine(fromX, fromY, toX, toY){
			var line = new createjs.Shape();
			line.graphics.moveTo(fromX,fromY).setStrokeStyle(1).beginStroke(mainColor).lineTo(toX,toY);
			stage.addChild(line);
            return line
        }        
        function drawText(txt, x, y,color,rotation, spaceWidth, spaceHeight, fontSize){
            // console.log("drawText => " + txt)
            // console.log("spaceWidth = " + spaceWidth + " | spaceHeight = " + spaceHeight)
            var font = "bold "+ fontSize +"px Arial"
            var theText = new createjs.Text("hmmm","bold 14px Arial","");
            theText.text = txt;
            theText.x = x;
            theText.y = y;
            theText.font = font
            if(spaceWidth > 0){
                var txtWidth = theText.getMeasuredWidth()
                // console.log("txtWidth = " + txtWidth + "  |  spaceWidth = " + spaceWidth)
                if(spaceWidth > txtWidth){
                    var padding = (spaceWidth - txtWidth) / 2
                    theText.x += padding
                }
            }
            if(spaceHeight > 0){
                var txtHeight = theText.getMeasuredHeight()
                if(spaceHeight > txtHeight){
                    var padding = (spaceHeight - txtHeight) /2
                    theText.y += padding
                }
            }
	        theText.rotation = rotation
	        theText.alpha = 1;
	        theText.color = color
	        stage.addChild(theText);
            return theText
        }         
		function myrefresh(){
			window.location.reload();
		}
        function changeRowCol(){
            // redrawWarehouse(10,10)
            // return
            var newRow = parseInt($('#numberRow').val())
            var newCol = parseInt($('#numberCol').val())
            // console.log(_.isNumber(newRow))            
            console.log("new row = %d col = %d", (newRow), newCol)
            if(newRow < row || newCol < col){
                if(confirm("仓库变小将删除超出部分的数据，确定吗？")){
                    redrawWarehouse(newRow,newCol)
                }else{
                    console.log("取消调整")
                }
            }else{
                redrawWarehouse(newRow,newCol)
            }
        }
        function submitConfig(){
            console.info("提交配置信息")
            var funcMapOrientation = function(orientationList){
                var list = []
                console.info(orientationList)
                list = _.reduce(orientationList, function(memo, orientation){
                    var oriTxt = "up"
                    switch(orientation){
                        case Cmd_AddOrientation_Right:
                        oriTxt = "right"
                        break
                        case Cmd_AddOrientation_Down:
                        oriTxt = "down"
                        break
                        case Cmd_AddOrientation_Left:
                        oriTxt = "left"
                        break
                    }
                    memo.push(oriTxt)
                    return memo
                }, list) 
                return list
            }
            var configs = _.map(warehouseConfig.Points, function(point){

                return {X: point.X, Y: point.Y, BlockType: point.BlockType, Placer: point.Placer, Orientations: funcMapOrientation(point.Orientations).join(",")}
                // var blockType = point.BlockType
                // if(blockType == Cmd_BlockType_ShelfPlace){
                //     return null
                // }else{
                //     if(_.contains(BlockTypeRailwaySeries, blockType) == true){
                //         blockType = Cmd_BlockType_Railway
                //     }
                //     return {X: point.X, Y: point.Y, BlockType: blockType, Placer: point.Placer, Orientation: point.Orientation}
                // }
            })
            configs = _.without(configs, null)
            var data = JSON.stringify({Points: configs, RowCount: warehouseConfig.RowCount, ColCount: warehouseConfig.ColCount})
            console.info(data)
            // return
            $.post("../SetWarehouseConfigData", {data: (data)}, function(data){
            // $.post("../SetWarehouseConfigData", {data: JSON.stringify(data)}, function(data){
                console.log(JSON.stringify(data))
                // if(resFunction != null){
                //     resFunction(data)
                // }
            })  


            // funcRes = function(data) {
            //     if(data.Code == 0){
            //         func2 = function(data2){
            //             if(data2.Code == 0){
            //                 alert("更新配置信息成功")
            //             }else{
            //                 alert(data.Message)
            //             }
            //         }
            //         submitRegionConfig(func2)
            //     }else{
            //         alert(data.Message)
            //     }
            // }
            // submitShelfConfig(funcRes)
        }
        function submitShelfConfig(resFunction){
            console.info("提交货架配置信息")
            var shelfList = _.map(warehouseConfig.Points, function(point){
                var blockType = point.BlockType
                if(blockType != Cmd_BlockType_ShelfPlace){
                    return null
                }else{
                    return {X: point.X, Y: point.Y, BlockType: blockType, ID: point.Placer}
                }
            })
            shelfList = _.without(shelfList, null)
            console.info(shelfList)
            return
            $.post("../SetWarehouseShelfData", {data: JSON.stringify(shelfList)}, function(data){
                console.log(JSON.stringify(data))
                if(resFunction != null){
                    resFunction(data)
                }
            })              
        }
        // function submitMapCell
        function submitRegionConfig(resFunction){
            console.info("提交区域配置信息")
            var configs = _.map(warehouseConfig.Points, function(point){
                var blockType = point.BlockType
                if(blockType == Cmd_BlockType_ShelfPlace){
                    return null
                }else{
                    if(_.contains(BlockTypeRailwaySeries, blockType) == true){
                        blockType = Cmd_BlockType_Railway
                    }
                    return {X: point.X, Y: point.Y, BlockType: blockType, Placer: point.Placer, Orientation: point.Orientation}
                }
            })
            configs = _.without(configs, null)
            var data = JSON.stringify({Points: configs, RowCount: warehouseConfig.RowCount, ColCount: warehouseConfig.ColCount})
            console.info(data)
            return
            $.post("../SetWarehouseConfigData", {data: (data)}, function(data){
            // $.post("../SetWarehouseConfigData", {data: JSON.stringify(data)}, function(data){
                console.log(JSON.stringify(data))
                if(resFunction != null){
                    resFunction(data)
                }
            })            
        }
        function setSpanXY(x, y, id, enabled){
            $('#pointID').val(id)
            $('#spanX').text(x)
            $('#spanY').text(y)
            if(enabled == true){
                $('#btnEditPointID').removeAttr('disabled')
            }else{
                $('#btnEditPointID').attr('disabled', "")
            }
        }
        function editPointID(){
            var x = parseInt($('#spanX').text())
            var y = parseInt($('#spanY').text())
            var id = $('#pointID').val()
            console.info("设置ID：(%d, %d) = %s", x, y, id)
            var point = _.findWhere(warehouseConfig.Points, {X: x, Y: y})
            console.assert(point != null, "程序异常")
            point.setID(id)
            var pointTempInList = _.findWhere(cellList, {X: x, Y: y})
            console.assert(pointTempInList != null, "程序异常")
            pointTempInList.updateColorText(null, id)
            pointTempInList.redrawCell()
            updateStage()
        }
    </script>
</head>
<body onLoad="init();" style="background-color: rgb(238,238,238);height: 1280px;">

    <div id = "divNav">
        <div class="container">
            <div class="row"  id = "">
                <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1">

                    <img id="imglogo" src="/images/syslogo.png">
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-md-lg-11">
                    <div style="">                        
                         <a class= "navFunc" style="right:6px;" onclick = "openAboutWindow()">关于 </a>
                     </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" id = "subNavBar" style="">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                <div id="lblTableTitleText" style=""> 
                  <span class="glyphicon glyphicon-arrow-right" style="margin-right: 3px;"></span>
                  <span style="font-size: 16px;"> 仓库环境设计 </span></div>
            </div>
         </div>
     </div>    

    <div class="container" id= "mainContainer">

            <canvas id="demoCanvas" width="1100" height="700" style="background-color: rgb(255,255,255);"> </canvas>
        <!-- <div style="background-color: rgb(255,255,255);text-align: center;"> </div> -->

        <div class="container" style="background-color: rgb(255,255,255); width: 1140px; height: 140px;margin-left: 0px; margin-right: 0px;border-top: solid 1px rgba(0,0,0,0.1);">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="font-size: 20px; margin-bottom: 9px; margin-top: 24px;">
                <span>调整仓库大小</span>
            </div>
              <div class="col-xs-4 col-sm-4 col-md-4 col-md-lg-4" style="font-size: 15px; margin-bottom: 9px; margin-top: 14px;">
                  <span>行数：</span><input type="number" id="numberRow" value="2">
              </div>
            
              <div class="col-xs-4 col-sm-4 col-md-4 col-md-lg-4" style="font-size: 15px; margin-bottom: 9px; margin-top: 14px;">
                  <span>列数：</span><input type="number" id="numberCol" value="2">
              </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-md-lg-4" style="font-size: 15px; margin-bottom: 9px; margin-top: 14px;text-align: right;">
                <button type="button" class="btn btn-sm btn-primary" onclick="changeRowCol()" style="margin-right: 105px;">确定</button>
            </div>  
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="font-size: 15px; margin-bottom: 9px; margin-top: 34px;text-align:center;border-top: solid 2px rgba(0,0,0,0.2); padding-top: 40px; padding-bottom: 20px;">
            <button type="button" class="btn btn-lg btn-success" onclick="submitConfig()" style="width: 80%;">提交设计信息</button>
        </div>  

    </div>


<!--     <div class="container" style="background-color: rgb(255,255,255);width:1000px; margin-top: -4px; margin-bottom: 5px; height: 140px;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="font-size: 20px; margin-bottom: 0px; margin-top: 15px;">
            <span>配置点信息：</span>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-md-lg-4" style="font-size: 15px; margin-bottom: 9px; margin-top: 14px;">
              <span>编号：</span><input type="input" id="pointID" value="">
        </div>        
        <div class="col-xs-4 col-sm-4 col-md-4 col-md-lg-4" style="font-size: 15px; margin-bottom: 9px; margin-top: 14px;color: rgba(0,0,0,0.6);">
              <span>坐标：</span><span id="spanX">0</span><span>，</span><span id="spanY">0</span>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-md-lg-4" style="font-size: 15px; margin-bottom: 9px; margin-top: 14px;text-align: right;">
            <button id="btnEditPointID" type="button" class="btn btn-sm btn-primary" disabled onclick="editPointID()">修改编号</button>
        </div>  
    </div> -->


    <div class="container" style="background-color: rgb(255,255,255);width:1170px;">
    <div class="row">
<!--         <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1" style="font-size: 15px; margin-bottom: 9px; margin-top: 34px;text-align:center;"> </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-md-lg-4" style="font-size: 15px; margin-bottom: 9px; margin-top: 34px;text-align:center;">
            <button type="button" class="btn btn-md btn-success" onclick="submitShelfConfig()" style="margin-bottom:30px;">只提交货架信息</button>
            
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-md-lg-2" style="font-size: 15px; margin-bottom: 9px; margin-top: 34px;text-align:center;"> </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-md-lg-4" style="font-size: 15px; margin-bottom: 9px; margin-top: 34px;text-align:center;">
            <button type="button" class="btn btn-md btn-success" onclick="submitRegionConfig()" style="margin-bottom:30px;">只提交区域配置信息</button>
        </div>
        <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1" style="font-size: 15px; margin-bottom: 9px; margin-top: 14px;text-align:center;"> </div> -->
  
      </div>

    </div>    
    <div class="container" style ="margin-top:30px">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" id = "tipContainer" style="margin-bottom: 0px;">
            <div> 
                <span class="icon-pushpin" style="margin-right: 3px;"></span>
                <span style=""> 设计完成后，提交设计信息 </span>
            </div>
          </div>
        </div>
    </div>  

    <!-- Modal -->
        <script language="javascript" type="text/javascript">
            function openAboutWindow(){
              $('#modalAbout').modal();
            }
        </script>
        <div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header" style="background-color: #00695c; color: white;">
                <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">关于</h4>
              </div>
              <div class="modal-body">
                <div style="text-align: center; font-size: 19px; margin-top: 20px;margin-bottom:20px;">
                   {{.aboutInfo}}
                </div>
              </div>
              <div class="modal-footer" style="text-align: center;">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
              </div>
            </div>
          </div>
        </div>
    <!-- Modal End-->    
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script src="/dataTable/jquery.dataTables.js"></script>
       

</body>
</html>