<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">    
    <title>AGV列表</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="icon" type="image/png" href="/images/logo3.png"> -->
    <!-- <link rel="icon" type="image/png" href="/images/logo_pure.png"> -->
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <link rel="stylesheet" href="/bootstrap/css/font-awesome.min.css">
    <link href="/stylesheets/style.css" rel="stylesheet" media="screen">    
    <link href="/dataTable/jquery.dataTables.css" rel="stylesheet" media="screen">    
    <style type="text/css">
      td.highlight {
          background-color: rgba(0,256,0, 0.1) !important;
      }
      tr.highlight{
          background-color: rgba(0,256,0, 0.1) !important;
      }
      #lblTip{
        margin-top: 30px; text-align: center; font-size: 30px; color: red; margin-bottom: 20px;
      }
      #dtProcess{
        text-align: center;
      }
      th{
        text-align: center;
      }
      .btn{
        width: 100px;
      }
 
    </style>
    <script type="text/javascript" src = "/javascripts/jquery.js"></script>
    <script type="text/javascript" src = "/javascripts/underscore.js"></script>
    <script type="text/javascript">
        var table;

        $(document).ready(function() {

            table = $('#dtProcess').DataTable( {
                "paging":   false,
                "ordering": false,
                "info":     false,
                "searching":  false,
                "ajax": "../AgvList",
                "columns": [
                    { "data": "ID" ,"width": "20%"},
                    { "data": "OverLoad" ,"width": "20%"},
                    { "data": "SelfWeight" ,"width": "20%"},
                    { "data": "Size" ,"width": "20%"},
                    { "data": "DesignSpeed" ,"width": "20%"},
                    // { "data": "" },
                ]
            } );

            $('#dtProcess tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
            
            table.on('draw', function(){
                return;
                var rowsCount = table.rows()[0].length;
                for(var i = 0; i< rowsCount; i++){
                    var data = table.row().nodes().data()[i];
                    console.dir(data);
                    if(data.complted == true){
                        $(table.row(i).node()).addClass('highlight');
                    }
                }
            })


            // $("#checkAutoPlan").change(checkboxChange);

            // createWebSocket();
            // updateSaying();
            // setInterval(updateSaying, 30 * 1000);

        });

    </script>
  </head>
  <body>
    <div id = "divNav">
        <div class="container">
            <div class="row"  id = "">
                <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1">

                    <img id="imglogo" src="/images/syslogo.png">
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-md-lg-11">
                    <div style="">

<!--                           <a class= "navFunc" style="right:525px;">教程 </a>
                          <a class= "navFunc" style="right:470px;"  href="/orderConfigIndex" target="_blank">订单 </a>
                          <a class= "navFunc " style="right:415px;" href="/processConfigIndex" target="_blank">流程 </a>
                          <a class= "navFunc " style="right:360px;color: rgb(100,100,100);" href="/planConfigIndex" target="_blank">计划 </a>
                          <a class= "navFunc" style="right:70px;">导出 </a>
 -->                          
                         <a class= "navFunc" style="right:6px;" onclick = "openAboutWindow()">关于 </a>
                     </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row" id = "subNavBar" style="">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                <div id="lblTableTitleText" style=""> 
                  <span class="glyphicon glyphicon-arrow-right" style="margin-right: 3px;"></span>
                  <span style="font-size: 16px;"> AGV列表 </span></div>
            </div>
         </div>
     </div>    
    <div class="container" id= "mainContainer">
      <div class="row"  id = "row_container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 10px; margin-bottom: 10px; border-bottom: solid rgba(0,0,0,0.1) 3px; padding-bottom: 10px;text-align: right;">
                <button type="button"  style="margin-left:0px;width:80px;" onclick = "">添加</button>
                <button type="button"  style="margin-left:10px;width:80px;" onclick = "">删除</button>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
          <table id="dtProcess" class="display" cellspacing="0" width="100%" >
              <thead>
                  <tr>
                      <th>编号</th>
                      <th>额定载重量</th>
                      <th>自重</th>
                      <th>车体尺寸</th>
                      <th>额定速度</th>
                  </tr>
              </thead>
          </table>
        </div>
      </div>
      <div class="row" style="margin-top: 150px;margin-bottom: 10px;text-align:center;">
            <!-- <button class="btnAction" onclick= "start()" style="">开始</button> -->
      </div>
    </div>
    <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" id = "tipContainer" style="">
            <div> 
                <span class="icon-pushpin" style="margin-right: 3px;"></span>
                <span style=""> 列表中列出了当前仓库的AGV信息 </span>
            </div>
          </div>
        </div>
    </div>  
<!-- Modal -->
    <script language="javascript" type="text/javascript">
        function openAboutWindow(){
          $('#modalAbout').modal();
        }
    </script>
    <div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="background-color: #00695c; color: white;">
            <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">关于</h4>
          </div>
          <div class="modal-body">
            <div style="text-align: center; font-size: 19px; margin-top: 20px;margin-bottom:20px;">
               {{.aboutInfo}}
            </div>
          </div>
          <div class="modal-footer" style="text-align: center;">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div>
    </div>
<!-- Modal End-->    
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/dataTable/jquery.dataTables.js"></script>
   
  </body>
</html>
