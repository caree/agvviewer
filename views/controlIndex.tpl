<!DOCTYPE html>
<html>
<head>
    <title>AGV控制</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">
    <link href="/stylesheets/style.css" rel="stylesheet" media="screen">    
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <link rel="stylesheet" href="/bootstrap/css/font-awesome.min.css">
    <style type="text/css">
      body,html {
        margin: 0;
        padding: 0;
        height:100%;
        background: black;
      }
      #lblTip{
        margin-top: 30px; text-align: center; font-size: 30px; color: red; margin-bottom: 20px;
      }
      #dtProcess{
        text-align: center;
      }
      th{
        text-align: center;
      }
      .btn{
        width: 100px;
      }
    </style>  
    <script src="/javascripts/jquery.min.js"></script>
    <script src="/bootstrap/js/bootstrap.js"></script>
    <script src="/javascripts/underscore.js"></script>
    <script src="/javascripts/easeljs-0.7.1.min.js"></script>
    <script>
		var row = 20, col = 20
        var warehouseConfig
        var agvList
        var row,col
        function init() {
            $.get('../WarehouseConfigData', function(data){
                console.log(JSON.stringify(data))
                warehouseConfig = data
                row = warehouseConfig.RowCount
                col = warehouseConfig.ColCount
            })
        	$.get('../AgvList', function(data){
        		console.log(JSON.stringify(data))
                agvList = data.data
                _.each(agvList, function(agv){
                    // $("#selectAgvList").appendTo(agv.ID)
                    $("<option value='"+ agv.ID +"'>"+ agv.ID +"</option>").appendTo($("#selectAgvList"));
                })
        		// col = warehouseConfig.ColCount
            })
     		
        }
        function sendXY(){
            var agvID = $("#selectAgvList").val();
            var x = $("#inputX").val()
            var y = $("#inputY").val()
            var url = '../MoveAGV2XY?X=' + x + '&Y=' + y + '&agvID=' + agvID
            console.log("request url => " + url)
            $.get(url, function(data){
                console.log(JSON.stringify(data))
            })

        }
        function setAGVLiftStatus(status){
            var agvID = $("#selectAgvList").val();
            console.log("set status => " + status)
            var url = '../SetAGVLiftStatus?agvID=' + agvID + "&status=" + status
            console.log("request url => " + url)
            $.get(url, function(data){
                console.log(JSON.stringify(data))
            })            
        }
        function setAGVStatus(status){
            var agvID = $("#selectAgvList").val();
            console.log("set status => " + status)
            var url = '../SetAGVStatus?agvID=' + agvID + "&status=" + status
            console.log("request url => " + url)
            $.get(url, function(data){
                console.log(JSON.stringify(data))
            })
        }

		function myrefresh(){
			window.location.reload();
		}

    </script>
</head>
<body onLoad="init();" style="background-color: rgb(200,200,200);text-align: center;">


    <div id = "divNav">
        <div class="container">
            <div class="row"  id = "">
                <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1">

                    <img id="imglogo" src="/images/syslogo.png">
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-md-lg-11">
                    <div style="">                        
                         <a class= "navFunc" style="right:6px;" onclick = "openAboutWindow()">关于 </a>
                     </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" id = "subNavBar" style="">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                <div id="lblTableTitleText" style=""> 
                  <span class="glyphicon glyphicon-arrow-right" style="margin-right: 3px;"></span>
                  <span style="font-size: 16px;"> AGV控制 </span></div>
            </div>
         </div>
     </div>    


    <div class="container" id= "mainContainer">

        <div class="row">
            <!-- ----------------------------------------- -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 20px;text-align:left;margin-bottom: 0px;">
                <h4>AGV选择</h4>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-md-lg-6" style="margin-top: 5px;text-align:left;margin-bottom: 10px;">
                <div>选择要控制的AGV</div>
                <select id="selectAgvList" name="selectTest" style="width:100%;font-size: 24px;">
                </select>
            </div>
           
            <!-- ----------------------------------------- -->


            <!-- ----------------------------------------- -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 20px;text-align:left;margin-bottom: 0px;">
                <h4>AGV移动</h4>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-md-lg-6" style="margin-top: 5px;text-align:left;margin-bottom: 10px;">
                <div>在输入框里填写AGV移动到的X坐标</div>
                <input type="text" id="inputX" style="width:100%;"  value="10" />
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-md-lg-6" style="margin-top: 5px;text-align:left;margin-bottom: 10px;">
                <div>在输入框里填写AGV移动到的Y坐标</div>
                <input type="text" id="inputY" style="width:100%;"  value="10" />
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 20px;text-align:left;margin-bottom: 10px;">
                <button type="button"  style="margin-left:0px;width:150px;" onclick = "sendXY()">移动</button>
            </div>            
            <!-- ----------------------------------------- -->
            
            <!-- ----------------------------------------- -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 20px;text-align:left;margin-bottom: 0px;">
                <h4>AGV举升控制</h4>
            </div>

            <div class="col-xs-2 col-sm-2 col-md-2 col-md-lg-2" style="margin-top: 20px;text-align:left;margin-bottom: 10px;">
                <button type="button"  style="margin-left:0px;width:150px;" onclick = "setAGVLiftStatus('{{.AGV_LIFT_STATUS_LIFTED}}')">举升</button>
            </div>      
                  
            <div class="col-xs-2 col-sm-2 col-md-2 col-md-lg-2" style="margin-top: 20px;text-align:left;margin-bottom: 10px;">
                <button type="button"  style="margin-left:0px;width:150px;" onclick = "setAGVLiftStatus('{{.AGV_LIFT_STATUS_UNLIFTED}}')">降下</button>
            </div>          

            <!-- ----------------------------------------- -->

            <!-- ----------------------------------------- -->
<!--             <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 20px;text-align:left;margin-bottom: 0px;">
                <h4>AGV状态控制</h4>
            </div>

            <div class="col-xs-2 col-sm-2 col-md-2 col-md-lg-2" style="margin-top: 20px;text-align:left;margin-bottom: 10px;">
                <button type="button"  style="margin-left:0px;width:150px;" onclick = "setAGVStatus('{{.AGV_STATUS_RUNNING_PAUSED}}')">暂停</button>
            </div>      
                  
            <div class="col-xs-2 col-sm-2 col-md-2 col-md-lg-2" style="margin-top: 20px;text-align:left;margin-bottom: 10px;">
                <button type="button"  style="margin-left:0px;width:150px;" onclick = "setAGVStatus('{{.AGV_STATUS_RUNNING}}')">运行</button>
            </div>      

            <div class="col-xs-2 col-sm-2 col-md-2 col-md-lg-2" style="margin-top: 20px;text-align:left;margin-bottom: 10px;">
                <button type="button"  style="margin-left:0px;width:150px;" onclick = "setAGVStatus('{{.AGV_STATUS_RUNNING_STOPPED}}')">停止</button>
            </div>   -->    

            <!-- ----------------------------------------- -->
            <div class="col-xs-2 col-sm-2 col-md-2 col-md-lg-2" style="margin-top: 20px;text-align:left;margin-bottom: 10px;">
                <div style ="height: 60px;"> </div>
            </div>
        </div>


    </div>
    <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" id = "tipContainer" style="">
            <div> 
                <span class="icon-pushpin" style="margin-right: 3px;"></span>
                <span style=""> 选择AGV，通过坐标控制其移动，还可以控制AGV的举升动作 </span>
            </div>
          </div>
        </div>
    </div>   
<!-- Modal -->
    <script language="javascript" type="text/javascript">
        function openAboutWindow(){
          $('#modalAbout').modal();
        }
    </script>
    <div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="background-color: #00695c; color: white;">
            <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">关于</h4>
          </div>
          <div class="modal-body">
            <div style="text-align: center; font-size: 19px; margin-top: 20px;margin-bottom:20px;">
               {{.aboutInfo}}
            </div>
          </div>
          <div class="modal-footer" style="text-align: center;">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div>
    </div>
<!-- Modal End-->    
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/dataTable/jquery.dataTables.js"></script>
   


</body>
</html>