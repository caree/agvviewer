package test

import (
	controllers "agvViewer/controllers"
	_ "agvViewer/routers"
	"net/http"
	"net/http/httptest"
	// "path/filepath"
	// "runtime"
	"fmt"
	"testing"

	"github.com/astaxie/beego"
	. "github.com/smartystreets/goconvey/convey"
)

type pathTaskInfo struct {
	index       int
	id          string
	srcX        int
	srcY        int
	destX       int
	destY       int
	isEnded     bool
	currentTask bool
	// pointList   controllers.PointPointerList
	pathPrint controllers.PointPointerList
}

func (this *pathTaskInfo) setPathPointList(list controllers.PointPointerList) {
	// this.pointList = list
	this.srcX = list[len(list)-2].X
	this.srcY = list[len(list)-2].Y
}

func (this *pathTaskInfo) addPathPrint() {
	this.pathPrint = append(this.pathPrint, &controllers.Point{X: this.srcX, Y: this.srcY})
}
func (this *pathTaskInfo) endTask() {
	this.isEnded = true
}
func (this *pathTaskInfo) isCurrentTask() bool {
	return this.currentTask
}
func (this *pathTaskInfo) setCurrentTask(b bool) {
	this.currentTask = b
}

type pathTaskList []*pathTaskInfo

func (this pathTaskList) setNextTask(currentTaskIndex int) bool {
	loopCount := 0
	for i := currentTaskIndex + 1; ; i++ {
		if loopCount > len(this) { //全部循环了一遍
			break
		} else {
			loopCount++
		}
		if i >= len(this) { //超出索引范围
			i = 0
		}

		if pointTemp := this[i]; pointTemp.isEnded == false {
			pointTemp.setCurrentTask(true)
			return false
		}
	}
	return true
}
func (this pathTaskList) printTaskPath() {
	for _, task := range this {
		str := "所经过路径： id  " + task.id
		for _, pointTemp := range task.pathPrint {
			str += " => " + pointTemp.ToString()
		}
		fmt.Println(str)
	}
}
func (this pathTaskList) getCurrentTask() *pathTaskInfo {
	for _, task := range this {
		if task.isCurrentTask() {
			return task
		}
	}
	return nil
}

var loopCount int = 0

func resetMaxloop() {
	loopCount = 0
}
func maxloopTest() bool {
	if loopCount > 10 {
		return true
	} else {
		return false
	}
}

func init() {
	// _, file, _, _ := runtime.Caller(1)
	// apppath, _ := filepath.Abs(filepath.Dir(filepath.Join(file, ".."+string(filepath.Separator))))
	// beego.TestBeegoInit(apppath)
}

func TestPathFinding(t *testing.T) {
	// tryCrossPath(t)
	// tryHeadPath(t)
}
func tryHeadPath(t *testing.T) {
	fmt.Println("tryHeadPath ...")
	controllers.G_warehouseMap = controllers.InitWarehouseMap(6, 5, controllers.WarehouseConfigPointList{})
	// controllers.G_warehouseMap = controllers.InitWarehouseMap(6, 5, controllers.G_configPointsList)
	fmt.Println(fmt.Sprintf("There are %d points on map", len(controllers.G_warehouseMap)))
	// beego.Warn(controllers.G_warehouseMap)
	for _, pointTemp := range controllers.G_warehouseMap {
		fmt.Println(fmt.Sprintf("[(%d,%d),%d]", pointTemp.X, pointTemp.Y, pointTemp.MapBlockLinked.BlockType))
	}
	tasks := pathTaskList{
		&pathTaskInfo{0, "1", 1, 3, 5, 3, false, true, controllers.PointPointerList{}},
		&pathTaskInfo{1, "2", 5, 3, 1, 3, false, false, controllers.PointPointerList{}},
	}
	executePathTaskList(tasks, t)
}

//测试交叉路径
func tryCrossPath(t *testing.T) {
	fmt.Println("tryCrossPath ...")
	controllers.G_warehouseMap = controllers.InitWarehouseMap(6, 5, controllers.WarehouseConfigPointList{})
	fmt.Println(fmt.Sprintf("There are %d points on map", len(controllers.G_warehouseMap)))
	// beego.Warn(controllers.G_warehouseMap)
	for _, pointTemp := range controllers.G_warehouseMap {
		fmt.Println(fmt.Sprintf("[(%d,%d),%d]", pointTemp.X, pointTemp.Y, pointTemp.MapBlockLinked.BlockType))
	}
	tasks := pathTaskList{
		&pathTaskInfo{0, "1", 3, 1, 3, 5, false, true, controllers.PointPointerList{}},
		&pathTaskInfo{1, "2", 1, 3, 5, 3, false, false, controllers.PointPointerList{}},
	}
	executePathTaskList(tasks, t)
}
func executePathTaskList(tasks pathTaskList, t *testing.T) {
	resetMaxloop()
	for {
		if maxloopTest() {
			break
		}
		currentTask := tasks.getCurrentTask()
		currentTask.addPathPrint()
		if b, list := tryFindPath(currentTask.id, currentTask.srcX, currentTask.srcY, currentTask.destX, currentTask.destY); b == true {
			currentTask.setCurrentTask(false)
			if list != nil {
				currentTask.setPathPointList(list)
			} else {
				fmt.Println(currentTask.id + " 计算路径完成")
				currentTask.endTask()
			}
			if allTaskEnd := tasks.setNextTask(currentTask.index); allTaskEnd == true {
				break
			}
		} else {
			currentTask.endTask()
			fmt.Println(currentTask.id + " 计算路径失败")
			t.Fail()
		}
	}
	fmt.Println("全部路径计算完成")
	tasks.printTaskPath()
}

func tryFindPath(id string, srcX, srcY, destX, destY int) (bool, controllers.PointPointerList) {
	// func tryFindPath(id string, srcX, srcY, destX, destY int) (finded bool, nextPoint *controllers.Point) {
	controllers.G_warehouseMap.ClearFindingFootPrint()

	controllers.G_warehouseMap.UnblockPathPoint(id)
	pointSrc1 := controllers.G_warehouseMap.GetPoint(srcX, srcY)
	pointSrc1.SetStartFlag()
	pointDest1 := controllers.G_warehouseMap.GetPoint(destX, destY)
	fmt.Println("")
	fmt.Println(fmt.Sprintf("id (%s) 查找路径 从 %s 到 %s", id, pointSrc1.ToString(), pointDest1.ToString()))
	predictor := func(point *controllers.Point) bool {
		return point.MapBlockLinked.BlockType != controllers.BannedArea && point.MapBlockLinked.BlockType != controllers.RailwayBlocked
	}
	if list, err := pointSrc1.FindPathPoints(controllers.G_warehouseMap, pointDest1, predictor); err != nil {
		if len(list) > 0 {
			fmt.Println("查找到的路径：")
			for _, pointTemp := range list {
				fmt.Println(fmt.Sprintf("[(%d,%d),%d]", pointTemp.X, pointTemp.Y, pointTemp.MapBlockLinked.BlockType))
			}
			listPointNextStepsBlocked := list
			if length := len(list); length >= controllers.MAX_STEP_COUNT {
				listPointNextStepsBlocked = list[length-controllers.MAX_STEP_COUNT:] //最后一个是出发点
			}
			// else {
			// 	listPointNextStepsBlocked = list
			// }
			listPointNextStepsBlocked.BlockPathPoint(id)
			str := ""
			for _, pointTemp := range listPointNextStepsBlocked {
				str += pointTemp.ToString() + " "
			}
			fmt.Println(fmt.Sprintf("被占用的点：%s", str))
			// pointNext := listPointNextStepsBlocked[len(listPointNextStepsBlocked)-2]
			// return tryFindPath(id, pointNext.X, pointNext.Y, destX, destY)
			return true, list
		} else {
			fmt.Println("已经在目标点上")
			return true, nil
		}
	} else {
		fmt.Println("寻找路径失败")
		return false, nil
	}
	// controllers.G_warehouseMap.ClearFindingFootPrint()
	return false, nil
}

// TestMain is a sample to run an endpoint test
func tryTestMain(t *testing.T) {
	return
	r, _ := http.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)

	beego.Trace("testing", "TestMain", "Code[%d]\n%s", w.Code, w.Body.String())

	Convey("Subject: Test Station Endpoint\n", t, func() {
		Convey("Status Code Should Be 200", func() {
			So(w.Code, ShouldEqual, 200)
		})
		Convey("The Result Should Not Be Empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
	})
}
