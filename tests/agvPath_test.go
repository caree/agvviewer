package test

import (
	controllers "agvViewer/controllers"
	_ "agvViewer/routers"
	// "path/filepath"
	// "runtime"
	"fmt"
	"testing"
	"time"
	// "github.com/astaxie/beego"
	// . "github.com/smartystreets/goconvey/convey"
)

var (
	agvTaskCenter controllers.AGVTaskList = controllers.AGVTaskList{}
)

func init() {
	// fmt.Println("TestPathFinding init ...")
}

/*
//使用指针改变数组内容
func TestArrayPointer(t *testing.T) {
	agvList := controllers.AGVList{
		controllers.NewAGV("1", 1, 1, nil, nil),
		controllers.NewAGV("2", 2, 2, nil, nil),
	}
	for _, agvTemp := range agvList {
		fmt.Println(agvTemp.ToString())
	}
	fmt.Println()
	changeList(&agvList)
	for _, agvTemp := range agvList {
		fmt.Println(agvTemp.ToString())
	}
}
func changeList(list *controllers.AGVList) {
	*list = append(*list, controllers.NewAGV("3", 3, 2, nil, nil))
}
*/
func TestAGVPathFinding(t *testing.T) {

	// tryTask001(t)
	// tryTask002(t)
	// tryTask002_1(t)
	// tryTask003(t)
	// tryTask004(t)
	// tryTask005(t)
	// tryTask006(t)
	tryTask007(t)
}
func tryTask007(t *testing.T) {
	fmt.Println("测试 007")
	fmt.Println("### 测试两个AGV行驶单行道 ...")
	row, col := 6, 6
	configPoints := controllers.WarehouseConfigPointList{
		controllers.NewWarehouseConfigPoint(1, 1, controllers.Railway, "", controllers.Orientation_Right),
		controllers.NewWarehouseConfigPoint(2, 1, controllers.Railway, "", controllers.Orientation_Right),
		controllers.NewWarehouseConfigPoint(3, 1, controllers.Railway, "", controllers.Orientation_Right),
	}
	warehouseMap := controllers.InitWarehouseMap(row, col, configPoints)
	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))

	var agvTaskCenter controllers.AGVTaskList = controllers.AGVTaskList{}
	chanTaskCenterReceiver := make(chan controllers.AgvData)

	funcMove := func(id string, x, y int) {
		fmt.Println(fmt.Sprintf("agv %s will move to (%d, %d)", id, x, y))
		agvData := controllers.AgvData{
			ID:       id,
			DataType: controllers.ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED, //move
			X:        x,
			Y:        y,
		}
		agvTaskCenter.AcceptData(&agvData)
	}

	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:            "1",
		AGV:           controllers.NewAGV("1", 1, 1, warehouseMap, controllers.ShelfList{}),
		Tasks:         controllers.TaskList{},
		ExecuteMoving: funcMove,
		// ExecuteLifting: funcLift,
	})

	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    1,
		DestY:    2,
	})

	printWarehouseMap(row, col, warehouseMap)

	evironmentInitlizor := func(receiver chan controllers.AgvData) {
		for {
			agvData := <-receiver
			time.Sleep(1 * time.Second)
			go agvTaskCenter.AcceptData(&agvData)
		}
	}
	go evironmentInitlizor(chanTaskCenterReceiver)
	for i := 0; i < 5; i++ {
		time.Sleep(1 * time.Second)
		agvTaskCenter.StartUpSpecialAGVTask("1")
	}
	printWarehouseMap(row, col, warehouseMap)
	strs := agvTaskCenter.Strings()
	for _, str := range strs {
		fmt.Println(str)
	}
	fmt.Println("### 007 测试完成 ...")
}
func tryTask006(t *testing.T) {
	//只有一个货架，AGV执行相应动作，货架位置相应变化
	fmt.Println("测试 006")
	fmt.Println("### 测试agv运行到货架底，然后运行回原位置，然后再运行至货架底 ...")

	row, col := 6, 6
	configPoints := controllers.WarehouseConfigPointList{
		controllers.NewWarehouseConfigPoint(2, 2, controllers.BannedArea, "", ""),
		controllers.NewWarehouseConfigPoint(2, 3, controllers.BannedArea, "", ""),
		controllers.NewWarehouseConfigPoint(2, 4, controllers.BannedArea, "", ""),
	}
	shelves := controllers.ShelfList{
		controllers.NewShelf("3", 3, 4),
		controllers.NewShelf("4", 3, 1),
	}
	warehouseMap := controllers.InitWarehouseMap(row, col, configPoints)
	warehouseMap.AddShelfToWarehouseMap(shelves)
	// warehouseMap = controllers.InitWarehouseMap(row, col, controllers.G_configPointsList)
	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))

	chanTaskCenterReceiver := make(chan controllers.AgvData)

	funcMove := func(id string, x, y int) {
		fmt.Println(fmt.Sprintf("agv %s will move to (%d, %d)", id, x, y))
		agvData := controllers.AgvData{
			ID:       id,
			DataType: controllers.ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED, //move
			X:        x,
			Y:        y,
		}
		chanTaskCenterReceiver <- agvData
	}
	funcLift := func(id, liftStatus string) {
		if liftStatus == "1" {
			fmt.Println(fmt.Sprintf("agv %s 将要举升货架", id))
		} else if liftStatus == "0" {
			fmt.Println(fmt.Sprintf("agv %s 将要放下货架", id))
		}
		agvData := controllers.AgvData{
			ID:         "1",
			DataType:   controllers.ACCEPTED_CMD_TYPE_AGV_LIFT_STATUS_CHANGED,
			LiftStatus: liftStatus,
		}
		chanTaskCenterReceiver <- agvData
	}

	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:             "1",
		AGV:            controllers.NewAGV("1", 5, 1, warehouseMap, shelves),
		Tasks:          controllers.TaskList{},
		ExecuteMoving:  funcMove,
		ExecuteLifting: funcLift,
	})

	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    3,
		DestY:    1,
	})
	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    5,
		DestY:    1,
	})
	evironmentInitlizor := func(receiver chan controllers.AgvData) {
		for {
			agvData := <-receiver
			time.Sleep(1 * time.Second)
			go agvTaskCenter.AcceptData(&agvData)
		}
	}
	printWarehouseMap(row, col, warehouseMap)
	go evironmentInitlizor(chanTaskCenterReceiver)
	for i := 0; i < 10; i++ {
		time.Sleep(1 * time.Second)
		agvTaskCenter.StartUpSpecialAGVTask("1")
	}
	printWarehouseMap(row, col, warehouseMap)

	fmt.Println("### 006 测试完成 ...")
}
func tryTask005(t *testing.T) {
	//只有一个货架，AGV执行相应动作，货架位置相应变化
	fmt.Println("测试 005")
	fmt.Println("### 测试agv运行到货架底，将货架举升，然后将货架运送到指定地点，地图上对货架的标识也相应改变 ...")

	row, col := 6, 6
	configPoints := controllers.WarehouseConfigPointList{}
	shelves := controllers.ShelfList{
		controllers.NewShelf("1", 2, 2),
		controllers.NewShelf("2", 2, 3),
		controllers.NewShelf("3", 2, 4),
		controllers.NewShelf("4", 3, 3),
	}
	warehouseMap := controllers.InitWarehouseMap(row, col, configPoints)
	warehouseMap.AddShelfToWarehouseMap(shelves)
	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))

	chanTaskCenterReceiver := make(chan controllers.AgvData)

	funcMove := func(id string, x, y int) {
		fmt.Println(fmt.Sprintf("agv %s will move to (%d, %d)", id, x, y))
		agvData := controllers.AgvData{
			ID:       id,
			DataType: controllers.ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED, //move
			X:        x,
			Y:        y,
		}
		chanTaskCenterReceiver <- agvData
	}
	funcLift := func(id, liftStatus string) {
		if liftStatus == "1" {
			fmt.Println(fmt.Sprintf("agv %s 将要举升货架", id))
		} else if liftStatus == "0" {
			fmt.Println(fmt.Sprintf("agv %s 将要放下货架", id))
		}
		agvData := controllers.AgvData{
			ID:         "1",
			DataType:   controllers.ACCEPTED_CMD_TYPE_AGV_LIFT_STATUS_CHANGED,
			LiftStatus: liftStatus,
		}
		chanTaskCenterReceiver <- agvData
	}

	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:             "1",
		AGV:            controllers.NewAGV("1", 1, 1, warehouseMap, shelves),
		Tasks:          controllers.TaskList{},
		ExecuteMoving:  funcMove,
		ExecuteLifting: funcLift,
	})

	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    3,
		DestY:    3,
	})
	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType:   controllers.TASK_TYPE_LIFT,
		LiftStatus: controllers.AGV_LIFT_STATUS_LIFTED,
	})
	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    1,
		DestY:    3,
	})
	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType:   controllers.TASK_TYPE_LIFT,
		LiftStatus: controllers.AGV_LIFT_STATUS_UNLIFTED,
	})
	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    1,
		DestY:    1,
	})
	evironmentInitlizor := func(receiver chan controllers.AgvData) {
		for {
			agvData := <-receiver
			time.Sleep(1 * time.Second)
			go agvTaskCenter.AcceptData(&agvData)
		}
	}
	printWarehouseMap(row, col, warehouseMap)

	go evironmentInitlizor(chanTaskCenterReceiver)
	for i := 0; i < 20; i++ {
		time.Sleep(1 * time.Second)
		agvTaskCenter.StartUpSpecialAGVTask("1")
	}
	printWarehouseMap(row, col, warehouseMap)

	fmt.Println("### 005 测试完成 ...")
}
func tryTask004(t *testing.T) {
	//只有货架，AGV只是执行相应动作
	fmt.Println("测试 004")
	fmt.Println("### 测试agv穿过一排货架，之后变为举升状态，之后绕过一排货架 ...")
	row, col := 6, 6
	configPoints := controllers.WarehouseConfigPointList{}
	shelves := controllers.ShelfList{
		controllers.NewShelf("1", 2, 2),
		controllers.NewShelf("2", 2, 3),
		controllers.NewShelf("3", 2, 4),
	}
	warehouseMap := controllers.InitWarehouseMap(row, col, configPoints)
	warehouseMap.AddShelfToWarehouseMap(shelves)

	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))

	chanTaskCenterReceiver := make(chan controllers.AgvData)
	funcMove := func(id string, x, y int) {
		fmt.Println(fmt.Sprintf("agv %s will move to (%d, %d)", id, x, y))
		agvData := controllers.AgvData{
			ID:       id,
			DataType: controllers.ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED, //move
			X:        x,
			Y:        y,
		}
		chanTaskCenterReceiver <- agvData
	}
	funcLift := func(id, liftStatus string) {
		if liftStatus == "1" {
			fmt.Println(fmt.Sprintf("agv %s 将要举升货架", id))
		} else if liftStatus == "0" {
			fmt.Println(fmt.Sprintf("agv %s 将要放下货架", id))
		}
		agvData := controllers.AgvData{
			ID:         "1",
			DataType:   controllers.ACCEPTED_CMD_TYPE_AGV_LIFT_STATUS_CHANGED,
			LiftStatus: liftStatus,
		}
		chanTaskCenterReceiver <- agvData
		// agvTaskCenter.AcceptData(&agvData)
	}

	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:             "1",
		AGV:            controllers.NewAGV("1", 1, 3, warehouseMap, shelves),
		Tasks:          controllers.TaskList{},
		ExecuteMoving:  funcMove,
		ExecuteLifting: funcLift,
	})

	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    3,
		DestY:    3,
	})
	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType:   controllers.TASK_TYPE_LIFT,
		LiftStatus: controllers.AGV_LIFT_STATUS_LIFTED,
	})
	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    1,
		DestY:    3,
	})

	printWarehouseMap(row, col, warehouseMap)

	evironmentInitlizor := func(receiver chan controllers.AgvData) {
		for {
			agvData := <-receiver
			time.Sleep(1 * time.Second)
			go agvTaskCenter.AcceptData(&agvData)
		}
	}
	go evironmentInitlizor(chanTaskCenterReceiver)
	for i := 0; i < 5; i++ {
		time.Sleep(1 * time.Second)
		agvTaskCenter.StartUpSpecialAGVTask("1")
	}

	printWarehouseMap(row, col, warehouseMap)

	fmt.Println("### 004 测试完成 ...")
}
func tryTask003(t *testing.T) {
	fmt.Println("测试 003")
	fmt.Println("### 测试agv1行走后，被其占用点需要取消占用，使得agv2能够运行至该点 ...")
	row, col := 6, 6
	configPoints := controllers.WarehouseConfigPointList{}
	warehouseMap := controllers.InitWarehouseMap(row, col, configPoints)
	// warehouseMap = controllers.InitWarehouseMap(row, col, controllers.G_configPointsList)
	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))
	chanTaskCenterReceiver := make(chan controllers.AgvData)
	funcMove := func(id string, x, y int) {
		fmt.Println(fmt.Sprintf("agv %s will move to (%d, %d)", id, x, y))
		agvData := controllers.AgvData{
			ID:       id,
			DataType: controllers.ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED, //move
			X:        x,
			Y:        y,
		}
		chanTaskCenterReceiver <- agvData
	}

	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:            "1",
		AGV:           controllers.NewAGV("1", 1, 1, warehouseMap, controllers.ShelfList{}),
		Tasks:         controllers.TaskList{},
		ExecuteMoving: funcMove,
	})
	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:            "2",
		AGV:           controllers.NewAGV("2", 4, 1, warehouseMap, controllers.ShelfList{}),
		Tasks:         controllers.TaskList{},
		ExecuteMoving: funcMove,
	})

	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    1,
		DestY:    3,
	})
	agvTaskCenter.AddTaskToAGV("2", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    1,
		DestY:    1,
	})

	printWarehouseMap(row, col, warehouseMap)

	// agvTaskCenter.AddTaskToAGV("2", &controllers.AGVTaskItem{
	// 	TaskType: controllers.TASK_TYPE_MOVE_TO,
	// 	DestX:    1,
	// 	DestY:    5,
	// })

	evironmentInitlizor := func(receiver chan controllers.AgvData) {
		for {
			agvData := <-receiver
			time.Sleep(1 * time.Second)
			go agvTaskCenter.AcceptData(&agvData)
		}
	}
	go evironmentInitlizor(chanTaskCenterReceiver)
	for i := 0; i < 10; i++ {
		time.Sleep(1 * time.Second)
		agvTaskCenter.StartUpSpecialAGVTask("1")
		agvTaskCenter.StartUpSpecialAGVTask("2")
	}
	printWarehouseMap(row, col, warehouseMap)

	fmt.Println("### 003 测试完成 ...")
}
func tryTask002_1(t *testing.T) {
	fmt.Println("测试 002-1")
	fmt.Println("### 测试行走至被AGV占用点时任务失败的情况 ...")
	row, col := 6, 5
	configPoints := controllers.WarehouseConfigPointList{}
	warehouseMap := controllers.InitWarehouseMap(row, col, configPoints)
	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))

	chanTaskCenterReceiver := make(chan controllers.AgvData)

	funcMove := func(id string, x, y int) {
		fmt.Println(fmt.Sprintf("agv %s will move to (%d, %d)", id, x, y))
		agvData := controllers.AgvData{
			ID:       id,
			DataType: controllers.ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED, //move
			X:        x,
			Y:        y,
		}
		fmt.Println("func move ...")
		chanTaskCenterReceiver <- agvData
	}

	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:            "1",
		AGV:           controllers.NewAGV("1", 1, 3, warehouseMap, controllers.ShelfList{}),
		Tasks:         controllers.TaskList{},
		ExecuteMoving: funcMove,
	})
	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:            "2",
		AGV:           controllers.NewAGV("2", 5, 3, warehouseMap, controllers.ShelfList{}),
		Tasks:         controllers.TaskList{},
		ExecuteMoving: funcMove,
	})

	// agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
	// 	TaskType: controllers.TASK_TYPE_MOVE_TO,
	// 	DestX:    2,
	// 	DestY:    3,
	// })
	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    5,
		DestY:    3,
	})
	printWarehouseMap(row, col, warehouseMap)

	evironmentInitlizor := func(receiver chan controllers.AgvData) {
		for {
			agvData := <-receiver
			fmt.Println("received ...")
			time.Sleep(1 * time.Second)
			go agvTaskCenter.AcceptData(&agvData)
			fmt.Println("received over...")
		}
	}
	go evironmentInitlizor(chanTaskCenterReceiver)
	for i := 0; i < 5; i++ {
		time.Sleep(1 * time.Second)
		agvTaskCenter.StartUpSpecialAGVTask("1")
	}
	// 或者
	// agvTaskCenter.StartUpTask()
	printWarehouseMap(row, col, warehouseMap)
	fmt.Println("### 002-1 测试完成 ...")
}
func tryTask002(t *testing.T) {
	fmt.Println("测试 002")
	fmt.Println("### 测试行走至被占用点时任务失败的情况 ...")
	row, col := 6, 5
	configPoints := controllers.WarehouseConfigPointList{}
	warehouseMap := controllers.InitWarehouseMap(row, col, configPoints)
	// warehouseMap = controllers.InitWarehouseMap(row, col, controllers.G_configPointsList)
	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))

	chanTaskCenterReceiver := make(chan controllers.AgvData)

	funcMove := func(id string, x, y int) {
		fmt.Println(fmt.Sprintf("agv %s will move to (%d, %d)", id, x, y))
		agvData := controllers.AgvData{
			ID:       id,
			DataType: controllers.ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED, //move
			X:        x,
			Y:        y,
		}
		fmt.Println("func move ...")
		chanTaskCenterReceiver <- agvData
	}

	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:            "1",
		AGV:           controllers.NewAGV("1", 1, 3, warehouseMap, controllers.ShelfList{}),
		Tasks:         controllers.TaskList{},
		ExecuteMoving: funcMove,
	})
	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:            "2",
		AGV:           controllers.NewAGV("2", 5, 3, warehouseMap, controllers.ShelfList{}),
		Tasks:         controllers.TaskList{},
		ExecuteMoving: funcMove,
	})

	if pointTemp := warehouseMap.GetPoint(3, 4); pointTemp != nil {
		pointTemp.Block("2") //占据地图上的位置
	} else {
		controllers.DebugOutput("地图错误", 2)
	}
	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    2,
		DestY:    3,
	})
	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    3,
		DestY:    4,
	})
	printWarehouseMap(row, col, warehouseMap)

	evironmentInitlizor := func(receiver chan controllers.AgvData) {
		for {
			agvData := <-receiver
			fmt.Println("received ...")
			// time.Sleep(1 * time.Second)
			go agvTaskCenter.AcceptData(&agvData)
			fmt.Println("received over...")
		}
	}
	go evironmentInitlizor(chanTaskCenterReceiver)
	for i := 0; i < 5; i++ {
		time.Sleep(1 * time.Second)
		agvTaskCenter.StartUpSpecialAGVTask("1")
	}
	// 或者
	// agvTaskCenter.StartUpTask()
	fmt.Println("### 002 测试完成 ...")
}
func tryTask001(t *testing.T) {
	fmt.Println("测试 001")
	fmt.Println("### 测试两个AGV对向路径行走 ...")
	row, col := 6, 6
	configPoints := controllers.WarehouseConfigPointList{}
	warehouseMap := controllers.InitWarehouseMap(row, col, configPoints)
	// warehouseMap = controllers.InitWarehouseMap(row, col, controllers.G_configPointsList)
	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))

	var agvTaskCenter controllers.AGVTaskList = controllers.AGVTaskList{}
	chanTaskCenterReceiver := make(chan controllers.AgvData)

	funcMove := func(id string, x, y int) {
		fmt.Println(fmt.Sprintf("agv %s will move to (%d, %d)", id, x, y))
		agvData := controllers.AgvData{
			ID:       id,
			DataType: controllers.ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED, //move
			X:        x,
			Y:        y,
		}
		agvTaskCenter.AcceptData(&agvData)
	}

	funcLift := func(id, liftStatus string) {
		if liftStatus == "1" {
			fmt.Println(fmt.Sprintf("agv %s 将要举升货架", id))
		} else if liftStatus == "0" {
			fmt.Println(fmt.Sprintf("agv %s 将要放下货架", id))
		}
		agvData := controllers.AgvData{
			ID:         "1",
			DataType:   controllers.ACCEPTED_CMD_TYPE_AGV_LIFT_STATUS_CHANGED,
			LiftStatus: liftStatus,
		}
		agvTaskCenter.AcceptData(&agvData)
	}
	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:             "1",
		AGV:            controllers.NewAGV("1", 2, 3, warehouseMap, controllers.ShelfList{}),
		Tasks:          controllers.TaskList{},
		ExecuteMoving:  funcMove,
		ExecuteLifting: funcLift,
	})
	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:             "2",
		AGV:            controllers.NewAGV("2", 5, 3, warehouseMap, controllers.ShelfList{}),
		Tasks:          controllers.TaskList{},
		ExecuteMoving:  funcMove,
		ExecuteLifting: funcLift,
	})

	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    6,
		DestY:    3,
	})

	agvTaskCenter.AddTaskToAGV("2", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    1,
		DestY:    3,
	})
	printWarehouseMap(row, col, warehouseMap)

	evironmentInitlizor := func(receiver chan controllers.AgvData) {
		for {
			agvData := <-receiver
			time.Sleep(1 * time.Second)
			go agvTaskCenter.AcceptData(&agvData)
		}
	}
	go evironmentInitlizor(chanTaskCenterReceiver)
	for i := 0; i < 5; i++ {
		time.Sleep(1 * time.Second)
		agvTaskCenter.StartUpSpecialAGVTask("1")
		agvTaskCenter.StartUpSpecialAGVTask("2")
	}
	printWarehouseMap(row, col, warehouseMap)
	strs := agvTaskCenter.Strings()
	for _, str := range strs {
		fmt.Println(str)
	}
	fmt.Println("### 001 测试完成 ...")
}

//测试交叉路径
func tryAGVCrossPath(t *testing.T) {
	fmt.Println("")
	fmt.Println("### 测试交叉路径行走 ...")
	row, col := 6, 5
	configPoints := controllers.WarehouseConfigPointList{}

	warehouseMap := controllers.InitWarehouseMap(row, col, configPoints)
	fmt.Println(fmt.Sprintf("地图分为 %d 行  %d 列，共有 %d 个点", row, col, len(warehouseMap)))
	printWarehouseMap(row, col, warehouseMap)

}

func checkPathOnBlockedPoint(blockedPoints controllers.PointPointerList, path controllers.PointPointerList) bool {
	for _, pointTemp := range path {
		for _, pointBlocked := range blockedPoints {
			if pointTemp.X == pointBlocked.X && pointTemp.Y == pointBlocked.Y {
				return false
			}
		}
	}
	return true
}
func printWarehouseMap(row, col int, warehouseMap controllers.PointPointerList) {
	mapstringList := map[int]string{}
	for i := 1; i <= row; i++ {
		mapstringList[i] = ""
	}
	for _, pointTemp := range warehouseMap {
		str := "nil"
		if pointTemp.AgvLinked != nil {
			str = "AGV"
		} else if pointTemp.ShelfLinked != nil {
			str = "Shelf"
		} else if pointTemp.IsBlocked() == true {
			str = "Block"
		}

		mapstringList[pointTemp.Y] = mapstringList[pointTemp.Y] + fmt.Sprintf("  [(%2d,%2d),%5s %3s]", pointTemp.X, pointTemp.Y, pointTemp.Orientations, str)
		// fmt.Println(fmt.Sprintf("[(%d,%d),%d]", pointTemp.X, pointTemp.Y, pointTemp.Flag))
	}
	for i := 1; i <= row; i++ {
		fmt.Println(mapstringList[i])
	}
}
