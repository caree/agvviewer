package test

import (
	controllers "agvViewer/controllers"
	"fmt"
	"testing"
	// "time"
	// "errors"
)

func TestAGVTaskProducing(t *testing.T) {
	// tryTask024(t)
	// tryTask023(t)
	// tryTask022(t)
	// tryTask021(t)
	// tryTask020(t)
}
func tryTask020(t *testing.T) {
	fmt.Println("测试 020")
	fmt.Println("### 将给定编号货架（在AGV上）运送到原位置 ...")

	var agvTaskCenter controllers.AGVTaskList = controllers.AGVTaskList{}
	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(controllers.NewAGVTaskInfo("1", 1, 1))
	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(controllers.NewAGVTaskInfo("2", 2, 1))

	shelves := controllers.ShelfList{
		controllers.NewShelf("1", 3, 1),
		controllers.NewShelf("2", 3, 4),
	}
	if agvTaskTemp, err := agvTaskCenter.GetIdleTaskInfo(); err != nil {
		fmt.Println(err.Error())
		t.FailNow()
	} else {
		shelfInfo, err1 := shelves.FindShelfByPosition(3, 4)
		if err1 != nil {
			t.FailNow()
		} else {
			fmt.Println(fmt.Sprintf("货架 %s 信息：%s", "2", shelfInfo.String()))
		}
		ShelfTransportTaskDestination2OriginalPoint := controllers.NewTransportToOriginalPoint(shelfInfo)
		shelfTransportTask := controllers.NewShelfTransportTask(shelfInfo, agvTaskTemp, ShelfTransportTaskDestination2OriginalPoint)
		if tasks, err4 := shelfTransportTask.TranslateToAGVTasks(); err4 == nil {
			agvTaskTemp.AddTasks(tasks)
			fmt.Println(agvTaskTemp.ToString())
		} else {
			t.FailNow()
		}

	}
	fmt.Println("### 020 测试完成 ...")
}
func tryTask021(t *testing.T) {
	fmt.Println("测试 021")
	fmt.Println("### 查找符合条件（没有任务）AGV，命令其将给定货架编号运送到目标点（如拣选口） ...")

	var agvTaskCenter controllers.AGVTaskList = controllers.AGVTaskList{}
	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(controllers.NewAGVTaskInfo("1", 1, 1))
	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(controllers.NewAGVTaskInfo("2", 2, 1))

	shelves := controllers.ShelfList{
		controllers.NewShelf("1", 3, 1),
		controllers.NewShelf("2", 3, 4),
	}
	configPointsList := controllers.PointPointerList{
		controllers.NewPoint(2, 2, controllers.NewMapBlock("", controllers.BannedArea), []string{}),
		controllers.NewPoint(5, 5, controllers.NewMapBlock("拣选口2", controllers.PickupPlace), []string{}),
		controllers.NewPoint(6, 5, controllers.NewMapBlock("拣选口1", controllers.PickupPlace), []string{}),
	}

	// shelfID := "2"
	x, y := 3, 4
	pickUpPlaceName := "拣选口2"
	pickUpPoint, err1 := configPointsList.FindPointByTypeAndName(controllers.PickupPlace, pickUpPlaceName)
	if err1 != nil {
		fmt.Println("查找不到指定的拣选口")
		t.FailNow()
	} else {
		fmt.Println(fmt.Sprintf("%s 信息：%s", pickUpPlaceName, pickUpPoint.ToString()))
	}
	ShelfTransportTaskDestination2PickUpPoint := controllers.NewTransportDestinationToPickUpPoint(pickUpPoint)

	shelfInfo, err2 := shelves.FindShelfByPosition(x, y)
	if err2 != nil {
		fmt.Println("查找不到要移动的货架的信息")
		t.FailNow()
	} else {
		fmt.Println(fmt.Sprintf("货架 信息：%s", shelfInfo.String()))
	}

	agvTaskInfo, err3 := agvTaskCenter.GetAGVCarryingShelf(x, y)
	if err3 == nil {
		//货架在某台AGV上
		fmt.Println(fmt.Sprintf("货架在AGV %s 上", agvTaskInfo.ID))
	} else {
		//货架没有在AGV上
		fmt.Println("货架没有在AGV上")
	}
	shelfTransportTask := controllers.NewShelfTransportTask(shelfInfo, agvTaskInfo, ShelfTransportTaskDestination2PickUpPoint)
	if tasks, err4 := shelfTransportTask.TranslateToAGVTasks(); err4 == nil {
		if agvTaskInfo == nil {
			if agvTaskTemp, err := agvTaskCenter.GetIdleTaskInfo(); err != nil {
				fmt.Println(err.Error())
				t.FailNow()
			} else {
				agvTaskInfo = agvTaskTemp
			}
		}
		agvTaskInfo.AddTasks(tasks)
		fmt.Println(agvTaskInfo.ToString())
	} else {
		t.FailNow()
	}

	//##将货架 运送到 【拣选口】等命名坐标点 或者 【原来位置】 或者 【指定位置】
	//1 货架在某台AGV上，直接生成任务列表，将其赋给该AGV
	//2 货架没有在AGV上，
	//  A 将货架搬运回原来位置，原来位置在货架属性里保存
	//  B 将货架搬运到指定位置，直接指定坐标

	// shelfTask := controllers.NewShelfTransportTask("2", agvTaskCenter, shelves,
	// 	controllers.NewTransportToPickUpPoint("拣选口2", configPointsList))
	// if agvTaskInfo, err := shelfTask.TranslateToAGVTasks(); err != nil {
	// 	fmt.Println(err.Error())
	// 	t.FailNow()
	// } else {
	// 	fmt.Println(agvTaskInfo.ToString())
	// }
	fmt.Println("### 021 测试完成 ...")
}
func tryTask022(t *testing.T) {
	fmt.Println("测试 022")
	fmt.Println("### 查找符合条件（没有任务）AGV ...")
	var agvTaskCenter controllers.AGVTaskList = controllers.AGVTaskList{}
	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:    "1",
		AGV:   controllers.NewAGV("1", 1, 1, nil, nil),
		Tasks: controllers.TaskList{},
	})

	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:    "2",
		AGV:   controllers.NewAGV("2", 2, 1, nil, nil),
		Tasks: controllers.TaskList{},
	})

	agvTaskCenter = agvTaskCenter.AddAGVTaskInfo(&controllers.AGVTaskInfo{
		ID:    "3",
		AGV:   controllers.NewAGV("3", 3, 1, nil, nil),
		Tasks: controllers.TaskList{},
	})

	agvTaskCenter.AddTaskToAGV("1", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    3,
		DestY:    1,
	})
	agvTaskCenter.AddTaskToAGV("2", &controllers.AGVTaskItem{
		TaskType: controllers.TASK_TYPE_MOVE_TO,
		DestX:    5,
		DestY:    1,
	})

	if agvTaskTemp, err := agvTaskCenter.GetIdleTaskInfo(); err != nil {
		fmt.Println(err.Error())
		t.Log("第一次查找")
		t.FailNow()
	} else {
		if agvTaskTemp.ID != "3" {
			t.FailNow()
		}
	}
	if err := agvTaskCenter.ClearAGVTasks("1"); err != nil {
		t.FailNow()
	}
	if agvTaskTemp, err := agvTaskCenter.GetIdleTaskInfo(); err != nil {
		fmt.Println(err.Error())
		t.Log("第二次查找")
		t.FailNow()
	} else {
		if agvTaskTemp.ID != "1" {
			t.FailNow()
		}
	}

	fmt.Println("### 022 测试完成 ...")
}
func tryTask023(t *testing.T) {
	fmt.Println("测试 023")
	fmt.Println("### 查找给定编号的货架坐标 ...")
	shelves := controllers.ShelfList{
		controllers.NewShelf("1", 3, 1),
		controllers.NewShelf("2", 3, 4),
	}
	if shelfTemp, err := shelves.FindShelfByPosition(3, 4); err != nil {
		fmt.Println(err.Error())
		t.FailNow()
	} else {
		if shelfTemp.X != 3 || shelfTemp.Y != 4 {
			fmt.Println("查找结果不符")
			t.FailNow()
		}
	}
	fmt.Println("### 023 测试完成 ...")
}
func tryTask024(t *testing.T) {
	fmt.Println("测试 024")
	fmt.Println("### 查找目标点（如拣选口）坐标 ...")

	configPointsList := controllers.PointPointerList{
		controllers.NewPoint(2, 2, controllers.NewMapBlock("", controllers.BannedArea), []string{}),
		controllers.NewPoint(5, 5, controllers.NewMapBlock("拣选口2", controllers.PickupPlace), []string{}),
		controllers.NewPoint(6, 5, controllers.NewMapBlock("拣选口1", controllers.PickupPlace), []string{}),
	}
	if point, err := configPointsList.FindPointByTypeAndName(controllers.PickupPlace, "拣选口2"); err != nil {
		fmt.Println(err.Error())
		t.FailNow()
	} else {
		fmt.Println(point.ToString())
		if point.X != 5 || point.Y != 5 {
			t.FailNow()
		}
	}

	fmt.Println("### 024 测试完成 ...")
}
